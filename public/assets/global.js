function Global(){
    return {
        requestAjax: async function(methodRequest, urlRequest, headersRequest, dataRequest) {
            let response = await $.ajax({
                method: methodRequest,
                url: urlRequest,
                headers: headersRequest,
                data: dataRequest,
            })

            return response
        },

        requestAjaxSync: function(methodRequest, urlRequest, headersRequest, dataRequest) {
            let response = $.ajax({
                method: methodRequest,
                url: urlRequest,
                headers: headersRequest,
                data: dataRequest,
                async: false
            })

            return response
        },

        convertToRupiah: function(angka) {
            var rupiah = '';
            var angkarev = angka.toString().split('').reverse().join('');
            for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
            return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
        }
    }
}