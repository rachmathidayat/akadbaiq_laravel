$(function() {
	//--------------------- global api-------------------------------//
	//--------------------- global api -------------------------------//

	var getapi = 'http://103.253.107.139:8080'

	//--------------------- end global api-------------------------------//
	//--------------------- end global api -------------------------------//
	
	//--------------------- page -------------------------------//
	//--------------------- page -------------------------------//

	//page profile
	if ($('.profile-page').length > 0) {
		var getProfileApi = function() {
			$.ajax({
				method: 'GET',
				url: getapi +'/api/profile/me',
				headers: {
					Authorization: localStorage.aq_acc_token, 
					'Accept-Version': '1.0'
				},
				success:function(response){
					if(response.success) {
						$('.name-profile').html(response.data.userAccount.userProfile.fullName)
						$('.phone-profile').html(response.data.userAccount.userProfile.phoneNumber)
						$('.email-profile').html(response.data.userAccount.username)
					} else {
						alert(response.data.message)
					}
				}
			});
		}
		// sidebar
		function noscroll(){
			$('html').addClass('hide-overflow')
		}
		function scroll(){
			$('html').removeClass('hide-overflow')
		}
		
		$('.burger').click(function(){
			$('.side-bar').addClass('is-active')
			noscroll()
		})
		$(".modal-background, .close-modal").click(function(){
			$(this).parents('.modal').removeClass('is-active')
			scroll()
		})
	}

	// page edit profile
	if ($('.editProfile-page').length > 0) {
		var getProfileApi = function() {
			$.ajax({
				method: 'GET',
				url: getapi + '/api/profile/me',
				headers: {
					Authorization: localStorage.aq_acc_token, 
					'Accept-Version': '1.0'
				},
				success:function(response){
					if(response.success) {
						$('.name-profile').html(response.data.userAccount.userProfile.fullName)
						$('.name-profile').val(response.data.userAccount.userProfile.fullName)
						$('.email-profile').html(response.data.userAccount.username)
					} else {
						alert(response.data.message)
					}
				}
			});
		}
	}

	//page index


	
	//--------------------- end page -------------------------------//
	//--------------------- end page -------------------------------//

	//--------------------- global js -------------------------------//
	//--------------------- global js -------------------------------//

	// menu ternak
	$('.peternak').on('click', function(){
		$('.menu-ternak').toggle();
		$('.menu-akun').css('display', 'none')
	})
	// menu akun
	$('.akun').on('click', function(){
		$('.menu-akun').toggle();
		$('.menu-ternak').css('display', 'none')
	})
	//if login
	if(localStorage.aq_login_status=="login"){
		$(".box-login").hide();
		$(".account_box").css("display","flex");
		$(".register_btn").html("Ajukan Kemitraan");
		$(".menu-ternak").find(".register_btn").attr("href","pengajuan-mitra1.html");
		$(".box-buy").css("display","block")
		getProfileApi();
	} else {
		window.location.href="index.html"
	}
	//logout
	$(".logout_btn").click(function(){
		localStorage.aq_login_status="";
		localStorage.aq_mitra_status="";
		location.reload(true);
	})
	//link contacus
	function contact(){
		window.location.assign("contactus.html")
	}
	//link pilih hewan 
	function cariternak(){
		window.location.assign("pilih-hewan.html")
	}
	//link bantuan pilih hewan 
	function bantuan(){
		window.location.assign("bantuan.html")
	}

	//--------------------- end global js -------------------------------//
	//--------------------- end global js -------------------------------//
});

	