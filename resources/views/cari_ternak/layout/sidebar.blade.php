<div class="column for-left is-narrow pb0 is-hidden-mobile">
    <div class="left-menu is-back-grey">
        <div class="brand-icon has-text-centered">
            <a href="{{ url('/') }}"><img src="{{ asset('assets/img/logo.png') }}" alt="Logo" width="70%"></a>
        </div>
        <div class="title-filter is-back-blue">Filter</div>
        <div class="box-filter">
            <div class="field">
                <label class="label has-text-left">Pilih Hewan</label>
                <div class="control">
                    <div class="select is-fullwidth">
                        <select id="filter_by_commodityType" onchange="changeHewan(this.value)">
                            <option value="" disabled selected>Pilih Hewan</option>
                            <option value="Sapi">Sapi</option>
                            <option value="Kambing">Kambing</option>
                            <option value="Kambing">Domba</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="field">
                <label class="label has-text-left">Pilih Jenis Hewan</label>
                <div class="control">
                    <div class="select is-fullwidth">
                        <select id="filter_by_any">
                            <option value="" disabled selected>Pilih Jenis Hewan</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="field mt1">
                <label class="label has-text-left">Pilih Grade Hewan</label>
                <div class="control">
                    <div class="select is-fullwidth">
                        <select id="filter_by_grade">
                            <option value="" disabled selected>Pilih Grade Hewan</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="field mt1">
                <label class="label has-text-left">Pilih Kab / Kota</label>
                <div class="control">
                    <div class="select is-fullwidth">
                        <select id="filter_by_districts">
                            <option value="" disabled selected>Pilih Kab / Kota</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="field mt1">
                <label class="label has-text-left">Harga Minimum</label>
                <div class="control">
                    <input class="input" type="text" id="filter_by_priceMin" placeholder="1.000">
                </div>
            </div>
            <div class="field mt1">
                <label class="label has-text-left">Harga Maksimum</label>
                <div class="control">
                    <input class="input" type="number" id="filter_by_priceMax" placeholder="1.000.000">
                </div>
            </div>
            <div class="field mt1">
                <button onclick="filterData()">Terapkan</button>
            </div>
        </div>
    </div>
</div>

<!-- minim sapi 10jt maks 100jt -->
<!-- minim kambing dan domba 1jt maks 10tk -->