<div class="modal side-bar">
    <div class="modal-background"></div>
    <div class="modal-card filter">
        <header class="modal-card-head">
            <p class="modal-card-title has-text-left">Filter</p>
            <a class="close-modal">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" fill="#fff"/></svg>
            </a>
        </header>
        <section class="modal-card-body is-paddingless list-bar">
            <div class="container pad-1">
                <div class="field">
                    <label class="label has-text-left">Jenis Komoditas</label>
                    <div class="control">
                        <div class="select is-fullwidth">
                            <select>
                                <optgroup label="Sapi">
                                    <option value="Sapi Jawa">Sapi Jawa</option>
                                    <option value="Sapi Limosi">Sapi Limosin</option>
                                    <option value="Sapi Bali">Sapi Bali</option>
                                </optgroup>
                                <optgroup label="Kambing">
                                    <option value="mercedes">Mercedes</option>
                                    <option value="audi">Audi</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="field mt1">
                    <label class="label has-text-left">Kota</label>
                    <div class="control">
                        <div class="select is-fullwidth">
                            <select>
                                <option value="Jakarta">Jakarta</option>
                                <option value="Bogor">Bogor</option>
                                <option value="Depok">Depok</option>
                                <option value="Tangerang">Tangerang</option>
                                <option value="Bekasi">Bekasi</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="field mt1">
                    <label class="label has-text-left">Grade</label>
                    <div class="control">
                        <div class="select is-fullwidth">
                            <select>
                                <option value="Grade A">Grade A</option>
                                <option value="Grade B">Grade B</option>
                                <option value="Grade C">Grade C</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="field mt1">
                    <label class="label has-text-left">Harga Minimum</label>
                    <div class="control">
                        <input class="input" type="number" placeholder="1.000">
                    </div>
                </div>
                <div class="field mt1">
                    <label class="label has-text-left">Harga Maksimum</label>
                    <div class="control">
                        <input class="input" type="number" placeholder="1.000.000">
                    </div>
                </div>
                <div class="field">
                    <a href="#" class="button btn-akadQ is-fullwidth mt1">FILTER</a>
                </div>
            </div>
        </section>
    </div>
</div>