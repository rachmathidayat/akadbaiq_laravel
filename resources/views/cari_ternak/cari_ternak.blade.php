@extends('cari_ternak.layout.index')

@section('content')
    <div class="level">
        <div class="level-left">
            <div class="level-item">
                <div class="content">
                    <!-- <p class="subtitle is-size-6 fw300 is-text-blue">Hasil pencarian hewan Anda saat ini belum tersedia</p> -->
                </div>
            </div>
        </div>
        <div class="level-right">
            <div class="level-item">
                <div class="field">
                    <label class="label"></label>
                    <div class="control has-text-centered">
                        <div class="select">
                            <select class="w-250">
                                <option value="all">Terbaru</option>
                                <option value="sapi">Termahal</option>
                                <option value="kambing">Termurah</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="columns col-netral is-multiline mt2 list-catalogue">
    
    </div>
@endsection

@section('script')
<script>
    const GlobalJS = new Global()

    $(document).ready(function() {
        getListCatalog()
        getDistrict()
        getGrade()
    })

    function getListCatalog() {
        var methodRequest = "GET"
        var urlRequest = "{{ env("HOST_API") }}/api/catalogue"
        var headerRequest = {
            "Accept-Version":"1.0",
        }
        var dataRequest = ""

        GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
            console.log(response.data)
            catalogMap(response.data)
        })
    }

    function catalogMap(data) {
        var list_catalogue = $('.list-catalogue')
        list_catalogue.html('')

        if(data.length > 0){
            data.map((item, index) => {
                // console.log(item.animal.uuid)
                var dateUpdated = item.dateUpdated

                getAddress(item.ranch.uuid).then(address => {
                    
                    getDetailAnimal(item.animal.uuid).then(res => {
                        // console.log(res)
                        list_catalogue.append(`
                            <div class="column is-4">
                                <input type="hidden" id="animaluuid${index}" value="${item.animal.uuid}"/>
                                <textarea id="imagesTmp`+res.data.uuid+`" style="display: none">`+JSON.stringify(res.data.images)+`</textarea>
                                <div class="box-hewan relative">
                                    <div class="img-hewan">
                                        <img src="${item.animal.images.length > 0 ? item.animal.images[0].url : ''}">
                                    </div>
                                    <div class="columns col-netral mb0">
                                        <div class="column">
                                            <div class="content desc-ternak">
                                                <div class="fw500">${item.animal.name}</div>
                                                <div class="small">${address}</div>
                                            </div>
                                        </div>
                                        <div class="column is-3">
                                            <div class="find-detail" onclick="findDetail('`+ res.data.name +`', '`+ res.data.progress[0].age +`', '`+ res.data.progress[0].width +`', '`+ res.data.progress[0].height +`', '`+ res.data.progress[0].chestSize +`', '`+ res.data.progress[0].weight +`', '`+dateUpdated+`', '`+res.data.uuid+`')">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"/></svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-price mt0h">
                                        <span class="disc-price">Rp ${numberFormat(item.price)}</span>
                                    </div>
                                    <div class="field">
                                        <a onclick="pengirimanTernak(${index})" class="button btn-akadQ is-fullwidth mt1">Pesan Sekarang</a>
                                    </div>
                                </div>
                            </div>
                        `)

                    })

                })

            })
        }
    }

    function numberFormat(x){
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    async function getAddress(ranchUuid){
        var methodRequest = "GET"
        var urlRequest = `{{ env("HOST_API") }}/api/ranch/${ranchUuid}`
        var headerRequest = {
            "Accept-Version":"1.0",
            Authorization:"{{ Session::get('accessToken') }}"
        }
        var dataRequest = ""

        let address = await GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest)
        return address.data.ranch.address.address
    }

    function changeHewan(val) {
        if(val == 'Sapi'){
            $('#filter_by_priceMin').val(10000000)
            $('#filter_by_priceMin').attr('min', 10000000)
            $('#filter_by_priceMax').val(100000000)
            $('#filter_by_priceMax').attr('max', 100000000)
            getAnimal('Sapi')
        }else{
            $('#filter_by_priceMin').val(1000000)
            $('#filter_by_priceMin').attr('min', 1000000)
            $('#filter_by_priceMax').val(10000000)
            $('#filter_by_priceMax').attr('max', 10000000)
            if(val == 'Kambing'){
                getAnimal('Kambing')
            }else{
                getAnimal('Domba')
            }
        }
    }

    function getAnimal(param) {
        var methodRequest = "GET"
        var urlRequest = "{{ env("HOST_API") }}/api/animal/categories?commodityType="+param
        var headerRequest = {
            "Accept-Version":"1.0",
        }
        var dataRequest = ""

        GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
            // console.log(response)
            var filter_by_any = $('#filter_by_any')
            filter_by_any.html('')

            response.data.map((item, index) => {
                filter_by_any.append(`
                    <option value="${item.uuid}">${item.name}</option>
                `)
            })
        })    
    }

    async function getDetailAnimal(animal_uuid){
      var response = await $.ajax({
          type: "GET",
          url: "{{ env("HOST_API") }}/api/animal/"+animal_uuid,
          data:'',
          headers:{
            'Accept-Version':'1.0',
            Authorization:"{{ Session::get('accessToken') }}"
          },
          dataType:"json",
          contentType: false,
          processData: false
      })

      return response
    }

    function getDistrict() {
        var methodRequest = "GET"
        var urlRequest = "{{ env("HOST_API") }}/api/area/districts"
        var headerRequest = {
            "Accept-Version":"1.0",
        }
        var dataRequest = ""

        GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
            // console.log(response)
            var filter_by_districts = $('#filter_by_districts')
            filter_by_districts.html('')

            response.data.map((item, index) => {
                filter_by_districts.append(`
                    <option value="${item.code}">${item.name}</option>
                `)
            })
        })
    }

    function getGrade() {
        var methodRequest = "GET"
        var urlRequest = "{{ env("HOST_API") }}/api/grade/"
        var headerRequest = {
            "Accept-Version":"1.0",
        }
        var dataRequest = ""

        GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
            // console.log(response)
            var filter_by_grade = $('#filter_by_grade')
            filter_by_grade.html('')

            response.data.grades.map((item, index) => {
                filter_by_grade.append(`
                    <option value="${item.uuid}">${item.name}</option>
                `)
            })
        })
    }

    function filterData() {
        let filter_by_commodityType = $('#filter_by_commodityType').val()
        let filter_by_any = $('#filter_by_any').val()
        let filter_by_grade = $('#filter_by_grade').val()
        let filter_by_districts = $('#filter_by_districts').val()
        let filter_by_priceMin = $('#filter_by_priceMin').val()
        let filter_by_priceMax = $('#filter_by_priceMax').val()

        var methodRequest = "GET"
        var urlRequest = "{{ env("HOST_API") }}/api/catalogue?start=1&length=3&filter_by_priceMin=1000&filter_by_priceMax=7500000"
        var headerRequest = {
            "Accept-Version":"1.0",
        }
        var dataRequest = ""

        GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
            // console.log(response)
            catalogMap(response.data)
        })     
    }

    function findDetail(nameAnimal, age, width, height, chestSize, weight, dateUpdated, animal_uuid){
        $('.detail-hewan').addClass('is-active');
        $('#detailNamaHewan').html(nameAnimal)
        $('#detailUmur').html(": &nbsp;"+age)
        $('#detailPanjang').html(": &nbsp;"+width+"cm")
        $('#detailTinggi').html(": &nbsp;"+height+"cm")
        $('#detailLingkarDada').html(": &nbsp;"+chestSize+"cm")
        $('#detailLingkarBerat').html(": &nbsp;"+weight+"kg")
        $('#detailDateUpdated').html("Terakhir update : "+dateUpdated+" WIB")
        
        var images = JSON.parse($('#imagesTmp'+animal_uuid).val())
        // swipper_detail_catalogue
        //swiper3.update();

        var swipper_detail_catalogue = $('.swipper_detail_catalogue')
        swipper_detail_catalogue.html("")

        for(var i = 0; i < images.length; i++){
            swipper_detail_catalogue.append(
            `<div class="swiper-slide"><img src="`+images[i].url+`" onerror="this.src='img/sapi-1-min.jpg'"></div>`
            )
        }

        var swiper3 = new Swiper('.swiper3', {
          slidesPerView: 1,
          loop: true,
          autoplay: true,
          pagination: {
            el: '.swiper-pagination.pag3',
            clickable: true,
          },
        });
    }

    function pengirimanTernak(index){
        let animal_uuid = $(`#animaluuid${index}`).val()
        window.location = `/checkout/pengiriman-ternak/${animal_uuid}`
    }
</script>
@endsection