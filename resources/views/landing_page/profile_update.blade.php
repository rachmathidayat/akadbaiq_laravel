@extends('landing_page.layout.index')

@section('content')
    <div class="container">
        <div class="columns mt3">
            <div class="column is-3">
                <div class="shadow-box left-metode pic-profile">
{{--                    <img src="{{ asset('assets/img/pic-profile.jpg') }}">--}}
                    <img id="photoProfile">
                </div>
                <hr class="mt2">
            </div>
            <div class="column">
                <div class="detail-profile shadow-box">
                    <div class="title-profile has-border-yellow">
                        <div class="level">
                            <div class="level-left">
                                <div class="level-item is-size-4 fw500">Ubah Biodata Diri</div>
                            </div>
                            <div class="level-right">
                                <div class="level-item">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-profile right-metode">
                        <div class="field is-horizontal">
                            <div class="field-label is-normal has-text-left">
                                <label class="label">Ubah Nama</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control is-expanded">
                                        <input class="input name-profile" type="text" value="{{ Session::get('fullName') }}">
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="field is-horizontal">
                            <div class="field-label is-normal has-text-left">
                                <label class="label">Email</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control is-expanded">
                                        <input class="input email-profile" type="email" value="{{ Session::get('username') }}">
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="field is-horizontal">
                            <div class="field-label is-normal has-text-left">
                                <label class="label">Nomor Telepon</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control is-expanded">
                                        <input class="input phone-profile" type="number" value="{{ Session::get('phoneNumber') }}">
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="field is-horizontal mt1">
                            <div class="field-label is-normal has-text-left">
                                <label class="label">Alamat</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <textarea class="textarea address-profile" placeholder="Isi alamat lengkap disini">{{ Session::get('address') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field is-horizontal">
                            <div class="field-label is-normal has-text-left">
                                <label class="label">Password</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control is-expanded">
                                        <input class="input" type="password" placeholder="******">
                                    </p>
                                </div>
                            </div>
                        </div>
                        <a class="has-text-right daftar block is-text-blue" onclick="changePassword()">Ganti Password?</a>
                        <div class="field has-text-centered mt2">
                            <a href="#" class="button btn-akadQ w-100" onclick="updateProfile()">Simpan</a>
                        </div>
                    </div>
                </div>
                <hr class="mt2">
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        const GlobalJS = new Global()

        $(document).ready(function(){
            checkLogin()
        })

        function checkLogin(){
            if("{{ Session::has('accessToken') }}"){
                $(".box-login").hide();
                $(".account_box").css("display","flex");
                $(".register_btn").html("Ajukan Kemitraan");
                $(".menu-ternak").find(".register_btn").attr("href","pengajuan-mitra1.html");
                $(".box-buy").css("display","block")

                getProfileApi()
            }
        }

        function getProfileApi(){
            var methodRequest = "GET"
            var urlRequest = "{{ url('/auth/get-profile') }}"
            var headerRequest = ""
            var dataRequest = ""
            var mitra_numb = 0
            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseProfile => {
//                console.log(responseProfile)
                if(responseProfile.success) {
                    $("#photoProfile").attr("src", responseProfile.data.userAccount.images[0].url)
            }else{
                alert(responseProfile.data.message)
            }
        })
        }

        function updateProfile(){
            var name_profile = $('.name-profile').val()
            var email_profile = $('.email-profile').val()
            var phone_profile = $('.phone-profile').val()
            var address_profile = $('.address-profile').val()

            var methodRequest = "POST"
            var urlRequest = "{{ env("HOST_API") }}/api/profile/updateProfile"
            var headerRequest = {
                Authorization: "{{ Session::get('accessToken') }}",
                'Accept-Version': '1.0'
            }
            var dataRequest = {
                fullName: name_profile,
                phone: phone_profile,
                'address.address': address_profile
            }

            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseUpdProfile => {
                alert(responseUpdProfile.message)
            })
        }

        function changePassword(){
            window.location = '/profile/change-password/{{ Session::get('userUUID') }}'
        }
    </script>
@endsection