@extends('landing_page.layout.index')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/extension.css') }}">
@endsection

@section('content')
    <div class="container">
      <div class="content has-text-centered">
        <div class="title is-size-4 mt3">
         Alamat Pengiriman
        </div>
        <p>Atur alamat pengiriman hewan ke lokasi yang Anda inginkan.</p>
      </div>
      <div class="columns is-centered">
        <div class="column is-8">
          <div class="field has-text-centered mt1">
            <label class="label mb1">Pilih Jenis Transaksi</label>
            <input class="is-checkradio is-info" id="dkm" type="radio" name="jenistransaksi" checked="checked" onclick="pilih('dkm')">
            <label for="dkm" class="ml0">DKM</label>
            <input class="is-checkradio is-info" id="pribadi" type="radio" name="jenistransaksi" onclick="pilih('pribadi')">
            <label for="pribadi">Pribadi</label>
            <!-- <input class="is-checkradio is-info" id="kelompok" type="radio" name="jenistransaksi" onclick="pilih('group')">
            <label for="kelompok">Kelompok</label> -->
          </div>
          <div class="dkm_content">
            <div class="columns">
              <div class="column">
                <div class="field mt1">
                  <label class="label has-text-left">Nama DKM / Masjid</label>
                  <div class="control">
                    <input class="input" id="dkm_dkmName" type="text" placeholder="Nama DKM / Masjid" onchange="setValue('dkm_dkmName')">
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Nama Penerima</label>
                  <div class="control">
                    <input class="input" id="dkm_receiverName" type="text" placeholder="Nama Penerima" onchange="setValue('dkm_receiverName')">
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Email</label>
                  <div class="control">
                    <input class="input" id="dkm_email" type="email" placeholder="Email" onchange="setValue('dkm_email')">
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">No Handphone / Telpon</label>
                  <div class="control">
                    <input class="input" id="dkm_phone" type="number" placeholder="081281532099" onchange="setValue('dkm_phone')">
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Alamat Lengkap DKM</label>
                  <div class="control">
                    <textarea class="textarea" id="dkm_address" placeholder="Alamat Lengkap DKM" onchange="setValue('dkm_address')"></textarea>
                  </div>
                </div>
              </div>
              <div class="column">
                <div class="field mt1">
                  <label class="label has-text-left">Pilih Provinsi</label>
                  <div class="control">
                    <div class="select is-fullwidth">
                      <select id="dkm_provinceCode" onchange="setValue('dkm_provinceCode')">
                        <option>Jakarta Selatan</option>
                        <option>Jakarta Timur</option>
                        <option>Jakarta Barat</option>
                        <option>Jakarta Utara</option>
                        <option>Jakarta Pusat</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Pilih Kecamatan</label>
                  <div class="control">
                    <div class="select is-fullwidth">
                      <select id="dkm_districtCode" onchange="setValue('dkm_districtCode')">
                        <option>Kebayoran Baru</option>
                        <option>Kebayoran Lama</option>
                        <option>Pasar Minggu</option>
                        <option>Pesanggrahan</option>
                        <option>Pancoran</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Pilih Kelurahan</label>
                  <div class="control">
                    <div class="select is-fullwidth">
                      <select id="dkm_subDistrictCode" onchange="setValue('dkm_subDistrictCode')">
                        <option>Grogol Utara</option>
                        <option>Grogol Selatan</option>
                        <option>Cipulir</option>
                        <option>Pondok Pinang</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Kode Pos</label>
                  <div class="control">
                    <input class="input" id="dkm_zipCode" type="number" onchange="setValue('dkm_zipCode')" placeholder="12210">
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Pin Alamat (opsional)</label>
                  <div class="pin-point relative">
                    <a href="#" class="button btn-map">Ubah Lokasi</a>
                  </div>
                  <form>
                    <input type="hidden" name="formatted_address" />
                    <input type="hidden" name="lat" id="dkm_lat" onchange="setValue('dkm_lat')"/>
                    <input type="hidden" name="lng" id="dkm_lng" onchange="setValue('dkm_lng')"/>
                    <label class="label location-label-lat mb0 lat-lang">Latitude - Longitude</label>
                    <label class="label location-label-add formatted_address">Alamat Lengkap</label>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="pribadi_content none">
            <div class="columns">
              <div class="column">
                <div class="field mt1">
                  <label class="label has-text-left">Nama Penerima</label>
                  <div class="control">
                    <input class="input" id="pribadi_receiverName" type="text" onchange="setValue('pribadi_receiverName')" placeholder="Nama Penerima">
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Email</label>
                  <div class="control">
                    <input class="input" id="pribadi_email" type="email" onchange="setValue('pribadi_email')" placeholder="Email">
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">No Handphone / Telpon</label>
                  <div class="control">
                    <input class="input" id="pribadi_phone" type="number" onchange="setValue('pribadi_phone')" placeholder="081281532099">
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Alamat Lengkap DKM</label>
                  <div class="control">
                    <textarea class="textarea" id="pribadi_address" onchange="setValue('pribadi_address')" placeholder="Alamat Lengkap DKM"></textarea>
                  </div>
                </div>
              </div>
              <div class="column">
                <div class="field mt1">
                  <label class="label has-text-left">Pilih Provinsi</label>
                  <div class="control">
                    <div class="select is-fullwidth">
                      <select id="pribadi_provinceCode" onchange="setValue('pribadi_provinceCode')">
                        <option>Jakarta Selatan</option>
                        <option>Jakarta Timur</option>
                        <option>Jakarta Barat</option>
                        <option>Jakarta Utara</option>
                        <option>Jakarta Pusat</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Pilih Kecamatan</label>
                  <div class="control">
                    <div class="select is-fullwidth">
                      <select id="pribadi_districtCode" onchange="setValue('pribadi_districtCode')">
                        <option>Kebayoran Baru</option>
                        <option>Kebayoran Lama</option>
                        <option>Pasar Minggu</option>
                        <option>Pesanggrahan</option>
                        <option>Pancoran</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Pilih Kelurahan</label>
                  <div class="control">
                    <div class="select is-fullwidth">
                      <select id="pribadi_subDistrictCode" onchange="setValue('pribadi_subDistrictCode')">
                        <option>Grogol Utara</option>
                        <option>Grogol Selatan</option>
                        <option>Cipulir</option>
                        <option>Pondok Pinang</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Kode Pos</label>
                  <div class="control">
                    <input class="input" id="pribadi_zipCode" type="number" onchange="setValue('pribadi_zipCode')" placeholder="12210">
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Pin Alamat (opsional)</label>
                  <div class="pin-point relative">
                    <a href="#" class="button btn-map">Ubah Lokasi</a>
                  </div>
                  <form>
                    <input type="hidden" name="formatted_address" />
                    <input type="hidden" name="lat" id="pribadi_lat" onchange="setValue('pribadi_lat')"/>
                    <input type="hidden" name="lng" id="pribadi_lng" onchange="setValue('pribadi_lang')"/>
                    <label class="label location-label-lat mb0 lat-lang">Latitude - Longitude</label>
                    <label class="label location-label-add formatted_address">Alamat Lengkap</label>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="kelompok_content none">
            <div class="columns">
              <div class="column">
                <div class="field mt1">
                  <label class="label has-text-left">Nama Penanggung Jawab</label>
                  <div class="control">
                    <input class="input" type="text" placeholder="Nama Penanggung Jawab">
                  </div>
                </div>
                <div class="box-anggota">
                  <div class="field mt1">
                    <label class="label has-text-left">Anggota 1</label>
                    <div class="control">
                      <input class="input" type="text" placeholder="Nama Anggota 1">
                    </div>
                  </div>
                  <div class="field mt1">
                    <label class="label has-text-left">Anggota 2</label>
                    <div class="control">
                      <input class="input" type="text" placeholder="Nama Anggota 2">
                    </div>
                  </div>
                  <div class="field mt1">
                    <label class="label has-text-left">Anggota 3</label>
                    <div class="control">
                      <input class="input" type="text" placeholder="Nama Anggota 3">
                    </div>
                  </div>
                  <div class="field mt1">
                    <label class="label has-text-left">Anggota 4</label>
                    <div class="control">
                      <input class="input" type="text" placeholder="Nama Anggota 4">
                    </div>
                  </div>
                  <div class="field mt1">
                    <label class="label has-text-left">Anggota 5</label>
                    <div class="control">
                      <input class="input" type="text" placeholder="Nama Anggota 5">
                    </div>
                  </div>
                  <div class="field mt1">
                    <label class="label has-text-left">Anggota 6</label>
                    <div class="control">
                      <input class="input" type="text" placeholder="Nama Anggota 6">
                    </div>
                  </div>
                  <div class="field mt1">
                    <label class="label has-text-left">Anggota 7</label>
                    <div class="control">
                      <input class="input" type="text" placeholder="Nama Anggota 7">
                    </div>
                  </div>
                </div>
                <!-- <div class="field has-text-right mt1">
                  <button class="button btn-yellow btn-akadQ" onclick="tambah()">Tambah Anggota</button>
                </div> -->
              </div>
              <div class="column">
                <div class="field mt1">
                  <label class="label has-text-left">Email</label>
                  <div class="control">
                    <input class="input" type="email" placeholder="Email">
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">No Handphone / Telpon</label>
                  <div class="control">
                    <input class="input" type="number" placeholder="081281532099">
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Pilih Provinsi</label>
                  <div class="control">
                    <div class="select is-fullwidth">
                      <select>
                        <option>Jakarta Selatan</option>
                        <option>Jakarta Timur</option>
                        <option>Jakarta Barat</option>
                        <option>Jakarta Utara</option>
                        <option>Jakarta Pusat</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Pilih Kecamatan</label>
                  <div class="control">
                    <div class="select is-fullwidth">
                      <select>
                        <option>Kebayoran Baru</option>
                        <option>Kebayoran Lama</option>
                        <option>Pasar Minggu</option>
                        <option>Pesanggrahan</option>
                        <option>Pancoran</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Pilih Kelurahan</label>
                  <div class="control">
                    <div class="select is-fullwidth">
                      <select>
                        <option>Grogol Utara</option>
                        <option>Grogol Selatan</option>
                        <option>Cipulir</option>
                        <option>Pondok Pinang</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Kode Pos</label>
                  <div class="control">
                    <input class="input" type="number" placeholder="12210">
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Alamat Lengkap DKM</label>
                  <div class="control">
                    <textarea class="textarea" placeholder="Alamat Lengkap DKM"></textarea>
                  </div>
                </div>
                <div class="field mt1">
                  <label class="label has-text-left">Pin Alamat (opsional)</label>
                  <div class="pin-point relative">
                    <a class="button btn-map">Ubah Lokasi</a>
                  </div>
                  <form>
                    <input type="hidden" name="formatted_address" />
                    <input type="hidden" name="lat" />
                    <input type="hidden" name="lng" />
                    <label class="label location-label-lat mb0 lat-lang">Latitude - Longitude</label>
                    <label class="label location-label-add formatted_address">Alamat Lengkap</label>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="field mb3 mt3 has-text-centered">
            <a onclick="submitAlamatPengiriman()" class="button btn-akadQ w-250">Proses Pembayaran</a>
          </div>
        </div>
      </div>
    </div>

    <div class="modal modal-left point-map">
        <div class="modal-background"></div>
        <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title is-size-6">Gunakan Titik Lokasi Anda</p>
            <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body relative">
            <div class="search-point">
            <div class="field">
                <div class="control">
                <input id="geocomplete" class="input" value="Jakarta, Indonesia" type="text" placeholder="Cari Alamat" />
                </div>
            </div>
            </div>
            <div id="map_canvas" class="map_canvas margin-auto" style="width: 500px; height: 400px;"></div>
        </section>
        <footer class="modal-card-foot j-center">
            <button class="button btn-akadQ btn-pick-address w-250">Ambil Alamat</button>
        </footer>
        </div>
    </div>
@endsection

@section('script')
    <script src='http://maps.google.com/maps/api/js?key=AIzaSyC58CZr-iYkzHQRS5d2JRmSRQ0-mzKD5-4&sensor=false&libraries=places'></script>
    <script src="{{ asset('assets/js/jquery.geocomplete.js') }}""></script>
    <script>
        const GlobalJS = new Global()

        $(document).ready(function() {
            $('.account_box').removeClass('none')
            pilih('dkm')
            getProvince('dkm')
        })

        function getProvince(jenisTransaksi) {
            var methodRequest = "GET"
            var urlRequest = "{{ env("HOST_API") }}/api/area/provinces"
            var headerRequest = {
                "Accept-Version":"1.0",
                Authorization:"{{ Session::get('accessToken') }}"
            }
            var dataRequest = ""

            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {

                if(jenisTransaksi == 'dkm'){
                    var dkm_provinceCode = $('#dkm_provinceCode')
                    dkm_provinceCode.html('')

                    response.data.map((item, index) => {
                        dkm_provinceCode.append(`
                            <option value="${item.code}">${item.name}</option>
                        `)
                    })
                }

                if(jenisTransaksi == 'pribadi'){
                    var pribadi_provinceCode = $('#pribadi_provinceCode')
                    pribadi_provinceCode.html('')

                    response.data.map((item, index) => {
                        pribadi_provinceCode.append(`
                            <option value="${item.code}">${item.name}</option>
                        `)
                    })
                }
            })
        }

        function getDistrict(provinceCode, jenisTransaksi) {
            var methodRequest = "GET"
            var urlRequest = "{{ env("HOST_API") }}/api/area/districts?provinceCode="+provinceCode
            var headerRequest = {
                "Accept-Version":"1.0",
                Authorization:"{{ Session::get('accessToken') }}"
            }
            var dataRequest = ""

            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
                console.log(response)

                if(jenisTransaksi == 'dkm'){
                    var dkm_districtCode = $('#dkm_districtCode')
                    dkm_districtCode.html('')

                    response.data.map((item, index) => {
                        dkm_districtCode.append(`
                            <option value="${item.code}">${item.name}</option>
                        `)
                    })
                }

                if(jenisTransaksi == 'pribadi'){
                    var pribadi_districtCode = $('#pribadi_districtCode')
                    pribadi_districtCode.html('')

                    response.data.map((item, index) => {
                        pribadi_districtCode.append(`
                            <option value="${item.code}">${item.name}</option>
                        `)
                    })
                }
            })
        }

        function getSubDistrict(districtCode, jenisTransaksi){
            var methodRequest = "GET"
            var urlRequest = "{{ env("HOST_API") }}/api/area/subdistricts?districtCode="+districtCode
            var headerRequest = {
                "Accept-Version":"1.0",
                Authorization:"{{ Session::get('accessToken') }}"
            }
            var dataRequest = ""

            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
                console.log(response)

                if(jenisTransaksi == 'dkm'){
                    var dkm_subDistrictCode = $('#dkm_subDistrictCode')
                    dkm_subDistrictCode.html('')

                    response.data.map((item, index) => {
                        dkm_subDistrictCode.append(`
                            <option value="${item.code}">${item.name}</option>
                        `)
                    })
                }

                if(jenisTransaksi == 'pribadi'){
                    var pribadi_subDistrictCode = $('#pribadi_subDistrictCode')
                    pribadi_subDistrictCode.html('')

                    response.data.map((item, index) => {
                        pribadi_subDistrictCode.append(`
                            <option value="${item.code}">${item.name}</option>
                        `)
                    })
                }
            })
        }

        var geodecoder =  new google.maps.Geocoder();
        var lats;
        var lngs;
        var maps;
        var coord = {lat:  -6.1762117, lng: 106.8259329};
        
        function initMap() {
        maps = new google.maps.Map(document.getElementById('map_canvas'), {
            center: coord,
            zoom: 10
            });  
        }

        /* MAP */
        $("#geocomplete").geocomplete({
            map: ".map_canvas",
            details: "form ",
            markerOptions: {
            draggable: true
            }
        });
        $("#geocomplete").val("Jakarta");
        $("#geocomplete").bind("geocode:dragged", function(event, latLng){
                lats = latLng.lat();
                lngs = latLng.lng();
                $(".lat-lang").html(lats+" - "+lngs);
                $(".lat-lang").html(lats+" - "+lngs);

                if(localStorage.getItem('jenisTransaksi') == 'dkm'){
                    localStorage.setItem('dkm_lat', lats)
                    localStorage.setItem('dkm_lng', lngs)
                }
                if(localStorage.getItem('jenisTransaksi') == 'pribadi'){
                    localStorage.setItem('pribadi_lat', lats)
                    localStorage.setItem('pribadi_lng', lngs)
                }

                $("#reset").show();
                geodecoder.geocode({
                'latLng': new google.maps.LatLng({lat: latLng.lat(), lng: latLng.lng()})
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        alert("Ambil alamat ini ("+results[0].formatted_address+") ?");
                        //alert(results[0].formatted_address);
                        $(".formatted_address").html(results[0].formatted_address);
                        $("#geocomplete").val(results[0].formatted_address); 

                        if(localStorage.getItem('jenisTransaksi') == 'dkm'){
                            localStorage.setItem('dkm_address', results[0].formatted_address)
                            $('#dkm_address').val(results[0].formatted_address)
                        }
                        if(localStorage.getItem('jenisTransaksi') == 'pribadi'){
                            localStorage.setItem('pribadi_address', results[0].formatted_address)
                            $('#pribadi_address').val(results[0].formatted_address)
                        }
                    } else {
                        alert('No results found');
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });
        });
        
        $("#reset").click(function(){
            $("#geocomplete").geocomplete("resetMarker");
            $("#reset").hide();
            return false;
        });
        
        $("#find").click(function(){
            $("#geocomplete").trigger("geocode");
        }).click();
        
        $('.btn-map').click(function(){
            $('.point-map').addClass('is-active');
            $("#geocomplete").trigger("geocode");
        })
        $('.delete, .btn-pick-address').click(function(){
            $(this).parents('.modal').removeClass('is-active')
        })
        $('.btn-pick-address').click(function(){
            // alert($("#geocomplete").val())
        })
        
        function pilih(param) {
            if(param == "pribadi") {

                let field = ['pribadi_receiverName', 'pribadi_email', 'pribadi_phone', 'pribadi_address', 'pribadi_provinceCode', 'pribadi_districtCode', 'pribadi_subDistrictCode', 'pribadi_zipCode', 'pribadi_lat', 'pribadi_lng']

                field.map((item, index) => {
                    $(`#${item}`).val('')
                    localStorage.removeItem(`${item}`)
                })

                localStorage.setItem('jenisTransaksi', param)

                getProvince('pribadi')

                $('.pribadi_content').addClass('block').removeClass('none')
                $('.dkm_content').addClass('none').removeClass('block')
                $('.kelompok_content').addClass('none').removeClass('block')
            } else if (param == "dkm"){

                let field = ['dkm_dkmName', 'dkm_receiverName', 'dkm_email', 'dkm_phone', 'dkm_address', 'dkm_provinceCode', 'dkm_districtCode', 'dkm_subDistrictCode', 'dkm_zipCode', 'dkm_lat', 'dkm_lng']

                field.map((item, index) => {
                    $(`#${item}`).val('')
                    localStorage.removeItem(`${item}`)
                })

                localStorage.setItem('jenisTransaksi', param)

                getProvince('dkm')

                $('.dkm_content').addClass('block').removeClass('none')
                $('.pribadi_content').addClass('none').removeClass('block')
                $('.kelompok_content').addClass('none').removeClass('block')
            } else if (param == "group"){
                $('.kelompok_content').addClass('block').removeClass('none')
                $('.pribadi_content').addClass('none').removeClass('block')
                $('.dkm_content').addClass('none').removeClass('block')
            }
        }

        function setValue(nameInput){
            console.log($(`#${nameInput}`).val())

            if(nameInput == 'dkm_provinceCode'){
                getDistrict($(`#${nameInput}`).val(), 'dkm')
            }
            if(nameInput == 'pribadi_provinceCode'){
                getDistrict($(`#${nameInput}`).val(), 'pribadi')
            }

            if(nameInput == 'dkm_districtCode'){
                getSubDistrict($(`#${nameInput}`).val(), 'dkm')
            }
            if(nameInput == 'pribadi_districtCode'){
                getSubDistrict($(`#${nameInput}`).val(), 'pribadi')
            }

            localStorage.setItem(nameInput, $(`#${nameInput}`).val())
        }

        function submitAlamatPengiriman(){
            let err = 0
            if(localStorage.getItem('jenisTransaksi') == 'pribadi'){
                let field = ['pribadi_receiverName', 'pribadi_email', 'pribadi_phone', 'pribadi_address', 'pribadi_provinceCode', 'pribadi_districtCode', 'pribadi_subDistrictCode', 'pribadi_zipCode', 'pribadi_lat', 'pribadi_lng']

                field.map((item, index) => {
                    let val = $(`#${item}`).val()

                    if(val){
                        err = 0
                    }else{
                        err = 1
                    }
                })

                if(err == 1){window.location = `/checkout/pengiriman-ternak/${animal_uuid}`
                    alert('isi data dulu')
                }else{
                    // alert('bisa di proses')
                    window.location = `/checkout/pembayaran-ternak/`+'{{ $animal_uuid }}'
                }
            }

            if(localStorage.getItem('jenisTransaksi') == 'dkm'){
                let field = ['dkm_dkmName', 'dkm_receiverName', 'dkm_email', 'dkm_phone', 'dkm_address', 'dkm_provinceCode', 'dkm_districtCode', 'dkm_subDistrictCode', 'dkm_zipCode', 'dkm_lat', 'dkm_lng']

                field.map((item, index) => {
                    let val = $(`#${item}`).val()

                    if(val){
                        err = 0
                    }else{
                        err = 1
                    }
                })

                if(err == 1){
                    alert('isi data dulu')
                }else{
                    alert('bisa di proses')
                    window.location = `/checkout/pembayaran-ternak/`+'{{ $animal_uuid }}'
                }
            }
        }
    </script>
@endsection