@extends('landing_page.layout.index')

@section('content')
    <div class="container hero-body">
        <div class="columns is-centered">
            <div class="column is-4">
                <div class="content has-text-centered">
                    <div class="title is-size-5">
                        Change Password
                    </div>
                    <p>Silahkan atur password baru Anda</p>
                </div>
                <div class="box-registration">

                    <div class="field icon margin-auto flex">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.585 47.496" width="40">
                            <defs>
                                <style>
                                    .cls-1 {
                                        fill: #00aeef;
                                    }

                                    .cls-2 {
                                        fill: #ffc60b;
                                    }
                                </style>
                            </defs>
                            <g id="Group_7" data-name="Group 7" transform="translate(-494.815 230.513)">
                                <g id="Group_2" data-name="Group 2" transform="translate(494.815 -230.513)">
                                    <path id="Path_8" data-name="Path 8" class="cls-1" d="M131.021,46.014V16.3s0-9.5-9.5-9.5H84.5v38s0,9.5,9.5,9.5h21.678V46.258H91.32V14.351h32.151v31.42h7.551ZM99.845,40.169H115.92V20.927H99.845V40.169Z" transform="translate(-84.5 -6.8)"/>
                                </g>
                                <g id="Group_3" data-name="Group 3" transform="translate(541.336 -191.298)">
                                    <path id="Path_9" data-name="Path 9" class="cls-2" d="M110.663,22.9H103.6v8.038h7.063Z" transform="translate(-103.6 -22.9)"/>
                                </g>
                            </g>
                        </svg>
                    </div>
                    <div class="field mt1">
                        <div class="control">
                            <input class="input is-primary inp_oldPassword" type="password" placeholder="Password Lama">
                        </div>
                    </div>
                    <div class="field mt1">
                        <div class="control">
                            <input class="input is-primary inp_newPassword" type="password" placeholder="Password Baru">
                        </div>
                    </div>
                    <div class="field mt1">
                        <div class="control">
                            <input class="input is-primary inp_confirmNewPassword" type="password" placeholder="Konfirmasi Password Baru">
                        </div>
                    </div>

                    <div class="field mt1">
                        <a class="button btn-akadQ is-fullwidth" onclick="changePassword()">Change Password</a>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @include('landing_page.layout.footer_content')
@endsection

@section('script')
    <script>
        const GlobalJS = new Global()

        $(document).ready(function(){
            checkLogin()
        })

        function checkLogin(){
            if("{{ Session::has('accessToken') }}"){
                $(".box-login").hide();
                $(".account_box").css("display","flex");
                $(".register_btn").html("Ajukan Kemitraan");
                $(".menu-ternak").find(".register_btn").attr("href","pengajuan-mitra1.html");
                $(".box-buy").css("display","block")

                getProfileApi()
            }
        }

        function getProfileApi(){
            var methodRequest = "GET"
            var urlRequest = "{{ url('/auth/get-profile') }}"
            var headerRequest = ""
            var dataRequest = ""
            var mitra_numb = 0
            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseProfile => {
                if(responseProfile.success) {

            }else{
                alert(responseProfile.data.message)
            }
        })
        }

        function changePassword(){
            var inp_oldPassword = $('.inp_oldPassword').val()
            var inp_newPassword = $('.inp_newPassword').val()
            var inp_confirmNewPassword = $('.inp_confirmNewPassword').val()

            if(inp_oldPassword == '' || inp_newPassword == '' || inp_confirmNewPassword == ''){
                alert("Old Password or New Password is required")
                return false
            }

            if(inp_newPassword != inp_confirmNewPassword){
                alert("New Password not match")
                return false
            }

            var methodRequest = "POST"
            var urlRequest = "{{ env("HOST_API") }}/api/auth/changePassword"
            var headerRequest = {
                Authorization: "{{ Session::get('accessToken') }}",
                'Accept-Version': '1.0',
                'Content-Type': 'application/x-www-form-urlencoded'
            }
            var dataRequest = {
                oldPassword: inp_oldPassword,
                newPassword: inp_newPassword,
                confirmNewPassword: inp_confirmNewPassword
            }

            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseChangePass => {
//                console.log(responseChangePass)
                if(responseChangePass.success) {
                    window.location = '/profile/update/{{ Session::get('userUUID') }}'
                }else{
                    alert(responseChangePass.message)
                }
            })
        }
    </script>
@endsection