@extends('landing_page.layout.index')

@section('content')
    <div class="container">
        <div class="content has-text-centered">
            <div class="title is-size-4 mt3">
                Silakan pilih sesuai keinginan Anda
            </div>
            <p>Di bawah ini beberapa program terbaiQ dari kami. </p>
        </div>
        <div class="columns is-centered">
            <div class="column is-6">
                <div class="columns">
                    <div class="column">
                        <div class="program-box has-text-centered">
                            <img src="{{ asset('assets/img/aqiqah.png') }}" alt="aqiqah">
                            <div class="content has-text-centered">
                                <div class="title is-size-5 mt2">
                                    AqiqahQ
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                            </div>
                            <div class="field">
                                <a class="button btn-akadQ is-fullwidth mt1 disabled">Mulai Sekarang</a>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="program-box has-text-centered">
                            <img src="{{ asset('assets/img/qurban.png') }}" alt="qurban">
                            <div class="content has-text-centered">
                                <div class="title is-size-5 mt2">
                                    QurbanQ
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                            </div>
                            <div class="field">
                                <a href="{{ route('cari_ternak') }}" class="button btn-akadQ is-fullwidth mt1">Mulai Sekarang</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection