@extends('landing_page.layout.index')

@section('content')
    <div class="container hero-body">
        <div class="columns is-centered">
            <div class="column is-4">
                <div class="content has-text-centered">
                    <div class="title is-size-5">
                        Daftar Akun
                    </div>
                    <p>Lengkapilah semua data dibawah ini untuk segera <br>mendapatkan akun akadbaiQ</p>
                </div>
                <div class="box-registration">

                    <div class="field icon margin-auto flex">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.585 47.496" width="40">
                            <defs>
                                <style>
                                    .cls-1 {
                                        fill: #00aeef;
                                    }

                                    .cls-2 {
                                        fill: #ffc60b;
                                    }
                                </style>
                            </defs>
                            <g id="Group_7" data-name="Group 7" transform="translate(-494.815 230.513)">
                                <g id="Group_2" data-name="Group 2" transform="translate(494.815 -230.513)">
                                    <path id="Path_8" data-name="Path 8" class="cls-1" d="M131.021,46.014V16.3s0-9.5-9.5-9.5H84.5v38s0,9.5,9.5,9.5h21.678V46.258H91.32V14.351h32.151v31.42h7.551ZM99.845,40.169H115.92V20.927H99.845V40.169Z" transform="translate(-84.5 -6.8)"/>
                                </g>
                                <g id="Group_3" data-name="Group 3" transform="translate(541.336 -191.298)">
                                    <path id="Path_9" data-name="Path 9" class="cls-2" d="M110.663,22.9H103.6v8.038h7.063Z" transform="translate(-103.6 -22.9)"/>
                                </g>
                            </g>
                        </svg>
                    </div>
                    <div class="field mt2">
                        <div class="control">
                            <input class="input is-primary inp_name" type="text" placeholder="Nama Lengkap">
                        </div>
                    </div>
                    <div class="field mt1">
                        <div class="control">
                            <input class="input is-primary inp_email" type="text" placeholder="Email Anda">
                        </div>
                    </div>
                    <div class="field mt1">
                        <div class="control">
                            <input class="input is-primary inp_phone" type="text" placeholder="Nomor Hanphone / Telpon">
                        </div>
                    </div>
                    <div class="field mt1">
                        <div class="control relative">
                            <input class="input is-primary inp_pass" type="password" placeholder="Password">
                            <div class="eye-pass">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 24" width="20">
                                    <defs>
                                        <style>
                                            .eyepass {
                                                fill: #707070;
                                            }
                                        </style>
                                    </defs>
                                    <g id="download" transform="translate(-0.006 -6.404)">
                                        <path id="Path_31762" data-name="Path 31762" class="eyepass" d="M20.006,6.4c-7.311,0-13.01,4.577-20,12,6.022,6.344,11.075,12,20,12s15.483-7.165,20-11.869c-4.623-5.511-11.183-12.13-20-12.13Zm0,22.666c-7.923,0-12.7-4.913-18.162-10.669C8.337,11.631,13.5,7.738,20.006,7.738c7.7,0,13.724,5.556,18.2,10.743-4.405,4.594-10.37,10.59-18.2,10.59Z" transform="translate(0 0)"/>
                                        <path id="Path_31763" data-name="Path 31763" class="eyepass" d="M17.6,9.6a8,8,0,1,0,8,8,8.009,8.009,0,0,0-8-8Zm0,14.666A6.666,6.666,0,1,1,24.269,17.6,6.674,6.674,0,0,1,17.6,24.269Z" transform="translate(2.403 0.801)"/>
                                        <path id="Path_31764" data-name="Path 31764" class="eyepass" d="M16.935,12.268A4.667,4.667,0,1,0,21.6,16.935,4.672,4.672,0,0,0,16.935,12.268Zm0,8a3.334,3.334,0,1,1,3.334-3.334A3.338,3.338,0,0,1,16.935,20.269Z" transform="translate(3.071 1.469)"/>
                                    </g>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="field mt1">
                        <div class="control relative">
                            <input class="input is-primary inp_repass" type="password" placeholder="Ulangi Password">
                            <div class="eye-pass">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 24" width="20">
                                    <defs>
                                        <style>
                                            .eyepass {
                                                fill: #707070;
                                            }
                                        </style>
                                    </defs>
                                    <g id="download" transform="translate(-0.006 -6.404)">
                                        <path id="Path_31762" data-name="Path 31762" class="eyepass" d="M20.006,6.4c-7.311,0-13.01,4.577-20,12,6.022,6.344,11.075,12,20,12s15.483-7.165,20-11.869c-4.623-5.511-11.183-12.13-20-12.13Zm0,22.666c-7.923,0-12.7-4.913-18.162-10.669C8.337,11.631,13.5,7.738,20.006,7.738c7.7,0,13.724,5.556,18.2,10.743-4.405,4.594-10.37,10.59-18.2,10.59Z" transform="translate(0 0)"/>
                                        <path id="Path_31763" data-name="Path 31763" class="eyepass" d="M17.6,9.6a8,8,0,1,0,8,8,8.009,8.009,0,0,0-8-8Zm0,14.666A6.666,6.666,0,1,1,24.269,17.6,6.674,6.674,0,0,1,17.6,24.269Z" transform="translate(2.403 0.801)"/>
                                        <path id="Path_31764" data-name="Path 31764" class="eyepass" d="M16.935,12.268A4.667,4.667,0,1,0,21.6,16.935,4.672,4.672,0,0,0,16.935,12.268Zm0,8a3.334,3.334,0,1,1,3.334-3.334A3.338,3.338,0,0,1,16.935,20.269Z" transform="translate(3.071 1.469)"/>
                                    </g>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="field mt1">
                        <a class="button btn-akadQ is-fullwidth regis_btn">Daftar</a>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @include('landing_page.layout.footer_content')
@endsection

@section('script')
<script>
    const GlobalJS = new Global()

    var username;
    var password;
    var repassword;
    var fullName;
    var phoneNumber;

    $(document).ready(function(){

        $(".regis_btn").click(register)

        function register(){
            username =$(".inp_email").val();
            password = $(".inp_pass").val();
            repassword= $(".inp_repass").val();
            fullName =$(".inp_name").val();
            phoneNumber =$(".inp_phone").val();

            if(password==repassword){
                var methodRequest = "POST"
                var urlRequest = "{{ env("HOST_API") }}/api/auth/register"
                var headerRequest = ""
                var dataRequest = {
                    username: username,
                    password: password,
                    fullName : fullName,
                    phone:phoneNumber,
                }

                GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
                    if(response.success){
                        alert("Register berhasil");
                        window.location="{{ url('/') }}";
                    }else {
                        alert("Register gagal karena, "+ response.message);
                    }
                })
            } else{
                alert("Password belum sesuai")
            }
        }

    })
</script>
@endsection