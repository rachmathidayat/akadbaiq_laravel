<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>AkadBaiQ</title>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bulma.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/swiper.css') }}">
<link rel="shortcut icon" type="image/png" href="{{ asset('assets/img/favicon.png') }}"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">