<!DOCTYPE html>
<html>
<head>
  @include('landing_page.layout.header')
  @yield('style')
</head>
<body>
<div class="index-page"></div>

@include('landing_page.layout.sidebar')

<section class="hero">
    <!-- navbar -->
    @include('landing_page.layout.navbar')

    @yield('content')

</section>
    @include('landing_page.layout.footer')
    @yield('script')
  <script>
    //swiper
    var swiper1 = new Swiper('.swiper1', {
      slidesPerView: 1,
      // loop: true,
      // autoplay: true,
      pagination: {
        el: '.swiper-pagination.pag1',
      },
    });

      //click
      $('.peternak').on('click', function(){
        $('.menu-ternak').toggle();
        $('.menu-akun').css('display', 'none')
      })

      $('.akun').on('click', function(){
        $('.menu-akun').toggle();
        $('.menu-ternak').css('display', 'none')
      })

      function contact(){
        window.location.assign("{{ url('/contact-us') }}")
      }
      function cariternak(){
        window.location.assign("{{ url('/cari-ternak') }}")
      }
      function bantuan(){
        window.location.assign("{{ url('/bantuan') }}")
      }
      function pembelian(){
          window.location.assign("{{ url('dashboard/buyer/transaksi-pembelian') }}")
      }
      function dashboard(){
          window.location.assign("{{ url('dashboard/mitra') }}")
      }
      function pengaturanMitra(){
          window.location.assign("{{ url('dashboard/mitra/pengaturan-peternakan') }}")
      }
      function pengaturanHewan(){
          window.location.assign("{{ url('dashboard/mitra/tambah-hewan') }}")
      }
      function transaksiAnda(){
          window.location.assign("mitra-transaksi-anda.html")
      }

      // sidebar
      function noscroll(){
        $('html').addClass('hide-overflow')
      }

      function scroll(){
        $('html').removeClass('hide-overflow')
      }

      $('.burger').click(function(){
        $('.side-bar').addClass('is-active')
        noscroll()
      })

      $(".modal-background, .close-modal").click(function(){
        $(this).parents('.modal').removeClass('is-active')
        scroll()
      })

      function number_format(number){
        str = parseInt(number).toString();
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(str)) {
          str = str.replace(rgx, '$1' + '.' + '$2');
        }
        return str;
      }

      $(document).ready(function(){
        localStorage.removeItem('uuid_animal_edit')
      })

    function profile(){
        window.location.href = "/profile/{{ Session::get('userUUID') }}"
    }

    function logout(){
        $.ajax({
            method: "POST",
            url: "{{ url('/auth/logout') }}",
            headers: "",
            data: "",
            success: function (response) {
                localStorage.removeItem('aq_login_status');
                localStorage.removeItem('aq_mitra_status');
                location.reload(true);
            }
        })
    }
    </script>
  </body>
  </html>