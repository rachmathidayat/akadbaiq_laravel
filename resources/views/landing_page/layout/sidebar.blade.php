<div class="modal side-bar">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title has-text-left">Menu</p>
            <a class="close-modal">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
                     width="24" height="24" viewBox="0 0 24 24">
                    <path d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z"
                          fill="#fff"/>
                </svg>
            </a>
        </header>
        <section class="modal-card-body is-paddingless list-bar">
            <!-- <div class="media flex">
                <div class="media-left flex">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 41.872 33" width="22" height="24">
                        <defs>
                            <style>
                                .mitra {
                                    fill: #707070;
                                }
                            </style>
                        </defs>
                        <g id="warehouse" transform="translate(-1.371 -0.002)">
                            <g id="Group_33045" data-name="Group 33045" transform="translate(1.371 0.002)">
                                <g id="Group_33044" data-name="Group 33044" transform="translate(0 0)">
                                    <path id="Path_31749" data-name="Path 31749" class="mitra"
                                          d="M61.841,14.156l-4.4-8.871a.887.887,0,0,0-.535-.454L41.263.041A.886.886,0,0,0,41,0h-.01a.886.886,0,0,0-.26.039L25.089,4.831a.887.887,0,0,0-.535.454l-4.4,8.871a.887.887,0,0,0,1.59.788l4.23-8.529L41,1.815l15.024,4.6,4.23,8.529a.887.887,0,0,0,1.59-.788Z"
                                          transform="translate(-20.062 -0.002)"/>
                                </g>
                            </g>
                            <g id="Group_33051" data-name="Group 33051" transform="translate(27.175 17.215)">
                                <g id="Group_33050" data-name="Group 33050">
                                    <path id="Path_31752" data-name="Path 31752" class="mitra"
                                          d="M312.465,194.293a.887.887,0,0,0-1.255,0l-.008.008a.887.887,0,0,0,1.255,1.255l.008-.008A.887.887,0,0,0,312.465,194.293Z"
                                          transform="translate(-310.942 -194.033)"/>
                                </g>
                            </g>
                            <g id="Group_33053" data-name="Group 33053" transform="translate(15.719 17.282)">
                                <g id="Group_33052" data-name="Group 33052">
                                    <path id="Path_31753" data-name="Path 31753" class="mitra"
                                          d="M183.322,195.063l-.008-.008a.887.887,0,0,0-1.255,1.255l.008.008a.887.887,0,0,0,1.255-1.255Z"
                                          transform="translate(-181.799 -194.795)"/>
                                </g>
                            </g>
                            <g id="Group_33063" data-name="Group 33063" transform="translate(4.382 2.84)">
                                <g id="Group_33062" data-name="Group 33062">
                                    <path id="Path_31758" data-name="Path 31758" class="mitra"
                                          d="M89.734,43.136,86,36.207a.887.887,0,0,0-.54-.433l-13.29-3.742A.953.953,0,0,0,71.916,32a.888.888,0,0,0-.24.033l-13.29,3.742a.887.887,0,0,0-.54.433l-3.738,6.929a.887.887,0,0,0-.106.421V61.273a.887.887,0,0,0,.887.887H88.942a.887.887,0,0,0,.887-.887l.011-17.716A.887.887,0,0,0,89.734,43.136ZM65.819,60.386l6.105-6.105,6.105,6.105Zm22.237,0h-7V44.773a.887.887,0,0,0-.887-.887H74.493a.887.887,0,0,0,0,1.774h4.79V59.132l-6.105-6.105,3.1-3.1a.887.887,0,0,0-1.255-1.255l-3.1,3.1L68.89,48.738a.887.887,0,1,0-1.255,1.255l3.034,3.034-6.112,6.111V45.66h4.79a.887.887,0,0,0,0-1.774H63.67a.887.887,0,0,0-.887.887V60.386H55.775V43.781l3.453-6.4,12.692-3.573L84.613,37.38l3.453,6.4Z"
                                          transform="translate(-54.001 -31.999)"/>
                                </g>
                            </g>
                            <g id="Group_33065" data-name="Group 33065" transform="translate(21.414 14.728)">
                                <g id="Group_33064" data-name="Group 33064">
                                    <path id="Path_31759" data-name="Path 31759" class="mitra"
                                          d="M246.887,166a.887.887,0,1,0,0,1.774h0a.887.887,0,0,0,0-1.774Z"
                                          transform="translate(-246 -165.998)"/>
                                </g>
                            </g>
                            <g id="Group_33067" data-name="Group 33067" transform="translate(18.842 6.833)">
                                <g id="Group_33066" data-name="Group 33066">
                                    <path id="Path_31760" data-name="Path 31760" class="mitra"
                                          d="M223.032,77h-5.145a.887.887,0,0,0-.887.887v5.145a.887.887,0,0,0,.887.887h5.145a.887.887,0,0,0,.887-.887V77.887A.887.887,0,0,0,223.032,77Zm-.887,5.145h-3.371V78.774h3.371Z"
                                          transform="translate(-217 -77)"/>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
                <div class="media-content align-self">
                    <div class="content is-marginless has-text-left">
                        @if(Session::has('accessToken'))
                            <a href="{{ url('dashboard/mitra') }}"><h3 class="subtitle is-6 is-marginless is-text-blue">Mitra</h3></a>
                        @else
                            <a onclick="alert('anda harus login dulu')"><h3 class="subtitle is-6 is-marginless is-text-blue">Mitra</h3></a>
                        @endif
                    </div>
                </div>
            </div> -->
            <div class="media flex">
                <div class="media-left flex">
                    <svg id="question" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 33 33" width="22" height="24">
                        <defs>
                            <style>
                                .help {
                                    fill: #707070;
                                }
                            </style>
                        </defs>
                        <g id="Group_36" data-name="Group 36">
                            <path id="Path_16" data-name="Path 16" class="help"
                                  d="M115.582,68.419a4.533,4.533,0,0,0-4.122.916,4.89,4.89,0,0,0-1.832,3.893,1.145,1.145,0,0,0,2.29,0,2.486,2.486,0,0,1,.916-2.061,2.663,2.663,0,0,1,2.29-.458,2.707,2.707,0,0,1,2.061,2.061,2.5,2.5,0,0,1-.916,2.748,6.829,6.829,0,0,0-2.748,5.5,1.082,1.082,0,0,0,1.145,1.145c.687,0,1.145-.458.916-1.145a5.043,5.043,0,0,1,2.061-3.664,4.962,4.962,0,0,0,1.832-5.037,5.192,5.192,0,0,0-3.893-3.893Z"
                                  transform="translate(-98.159 -60.912)"/>
                            <path id="Path_17" data-name="Path 17" class="help"
                                  d="M139.439,208.288a1.377,1.377,0,0,0-.229.916.841.841,0,0,0,.229.687,1.736,1.736,0,0,0,.916.458c.229,0,.687-.229.687-.458.229,0,.458-.458.458-.687a1.19,1.19,0,0,0-.458-.916A1.107,1.107,0,0,0,139.439,208.288Z"
                                  transform="translate(-123.848 -184.828)"/>
                            <path id="Path_18" data-name="Path 18" class="help"
                                  d="M16.5,0A16.5,16.5,0,1,0,33,16.5,16.486,16.486,0,0,0,16.5,0Zm0,31.059A14.665,14.665,0,0,1,1.941,16.5,14.665,14.665,0,0,1,16.5,1.941,14.665,14.665,0,0,1,31.059,16.5,14.665,14.665,0,0,1,16.5,31.059Z"/>
                        </g>
                    </svg>
                </div>
                <div class="media-content align-self" onclick="bantuan()">
                    <div class="content is-marginless has-text-left">
                        <h3 class="subtitle is-6 is-marginless is-text-blue">Bantuan</h3>
                    </div>
                </div>
            </div>
            <div class="media flex">
                <div class="media-left flex">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.604 34.7" width="22" height="24">
                        <defs>
                            <style>
                                .map {
                                    fill: none;
                                    stroke: #707070;
                                    stroke-width: 1.7px;
                                }
                            </style>
                        </defs>
                        <g id="location-sign" transform="translate(0.85 0.85)">
                            <path id="Path_30828" data-name="Path 30828" class="map"
                                  d="M71.983,0A11.465,11.465,0,0,0,60.531,11.452c0,6.078,10.39,20.28,10.833,20.881l.413.562a.256.256,0,0,0,.413,0l.413-.562c.443-.6,10.833-14.8,10.833-20.881A11.466,11.466,0,0,0,71.983,0Zm0,7.35a4.1,4.1,0,1,1-4.1,4.1A4.106,4.106,0,0,1,71.983,7.35Z"
                                  transform="translate(-60.531)"/>
                        </g>
                    </svg>
                </div>
                <div class="media-content align-self" onclick="cariternak()">
                    <div class="content is-marginless has-text-left">
                        <a><h3 class="subtitle is-6 is-marginless is-text-blue">Cari Ternak</h3></a>
                    </div>
                </div>
            </div>
            <div class="media flex">
                <div class="media-left flex">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 33 33" width="22" height="24">
                        <defs>
                            <style>
                                .call {
                                    fill: #707070;
                                }
                            </style>
                        </defs>
                        <g id="phone-call" transform="translate(0)">
                            <path id="Path_32293" data-name="Path 32293" class="call"
                                  d="M26.523,33a14.173,14.173,0,0,1-4.855-1.032A36.424,36.424,0,0,1,9.419,23.581,36.45,36.45,0,0,1,1.03,11.33C-.341,7.7-.344,4.712,1.022,3.347c.2-.2.4-.407.6-.625C2.871,1.411,4.28-.064,6.147,0,7.433.055,8.68.855,9.956,2.443c3.772,4.691,2.071,6.362.1,8.3l-.347.344c-.322.322-.93,1.813,4.728,7.471a31.377,31.377,0,0,0,4.681,4.017c.793.515,2.21,1.294,2.792.713l.35-.353c1.936-1.964,3.607-3.658,8.295.11,1.588,1.276,2.386,2.522,2.44,3.81.077,1.866-1.41,3.278-2.722,4.523-.216.206-.427.406-.624.6A4.324,4.324,0,0,1,26.523,33ZM6.014,1.11C4.687,1.11,3.49,2.371,2.43,3.486c-.213.224-.42.442-.623.645C.789,5.148.892,7.82,2.069,10.94A35.334,35.334,0,0,0,10.2,22.8,35.308,35.308,0,0,0,22.06,30.93c3.12,1.179,5.792,1.279,6.807.263.2-.2.421-.41.645-.622,1.14-1.083,2.433-2.311,2.376-3.673-.039-.936-.72-1.942-2.026-2.991-3.906-3.141-5-2.032-6.81-.2l-.355.359c-.866.867-2.272.675-4.181-.566a32.41,32.41,0,0,1-4.861-4.163c-4.7-4.7-6.2-7.569-4.728-9.042l.354-.35c1.841-1.811,2.952-2.9-.19-6.813C8.042,1.833,7.036,1.151,6.1,1.111Z"
                                  transform="translate(0 0)"/>
                        </g>
                    </svg>
                </div>
                <div class="media-content align-self" onclick="contact()">
                    <div class="content is-marginless has-text-left">
                        <a><h3 class="subtitle is-6 is-marginless is-text-blue">Hubungi Kami</h3></a>
                    </div>
                </div>
            </div>
            <div class="media flex">
                <div class="media-left flex">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="22" height="24">
                        <defs>
                            <style>
                                .user {
                                    fill: #707070;
                                }
                            </style>
                        </defs>
                        <g id="Group_33750" data-name="Group 33750" transform="translate(-1690 -34)">
                            <path id="download" class="user"
                                  d="M19.2,3.2a16,16,0,1,0,16,16A16,16,0,0,0,19.2,3.2Zm0,1.333A14.658,14.658,0,0,1,30.482,28.571c-1.462-.61-4.914-1.808-7.05-2.44-.183-.058-.211-.066-.211-.825a4.375,4.375,0,0,1,.509-1.791,9.18,9.18,0,0,0,.712-2.431,5.482,5.482,0,0,0,1.049-2.533,3.335,3.335,0,0,0-.033-2.126c-.018-.045-.035-.089-.049-.134a13.961,13.961,0,0,1,.236-2.99,5.474,5.474,0,0,0-1.152-3.95A5.9,5.9,0,0,0,19.988,7.2H18.64a5.867,5.867,0,0,0-4.464,2.155,5.479,5.479,0,0,0-1.15,3.95,13.992,13.992,0,0,1,.239,2.985.959.959,0,0,1-.05.139,3.34,3.34,0,0,0-.031,2.126,5.455,5.455,0,0,0,1.049,2.533,9.087,9.087,0,0,0,.712,2.431,4.329,4.329,0,0,1,.293,1.818c0,.759-.029.768-.2.822-2.21.653-5.726,1.923-7.038,2.5A14.655,14.655,0,0,1,19.2,4.538ZM8.965,29.692c1.5-.613,4.494-1.681,6.461-2.261,1.143-.36,1.143-1.323,1.143-2.1a5.754,5.754,0,0,0-.419-2.386,7.554,7.554,0,0,1-.618-2.221.658.658,0,0,0-.225-.444,3.973,3.973,0,0,1-.818-1.992c-.194-.967-.111-1.178-.033-1.383a2.736,2.736,0,0,0,.093-.27,13.077,13.077,0,0,0-.213-3.574,4.173,4.173,0,0,1,.889-2.89A4.553,4.553,0,0,1,18.682,8.53h1.264a4.583,4.583,0,0,1,3.5,1.646,4.165,4.165,0,0,1,.888,2.891,13.118,13.118,0,0,0-.213,3.573c.028.1.059.184.093.271.08.2.161.416-.031,1.383a3.969,3.969,0,0,1-.819,1.992.672.672,0,0,0-.225.444,7.5,7.5,0,0,1-.615,2.221,5.663,5.663,0,0,0-.635,2.358c0,.774,0,1.737,1.155,2.1,1.882.556,4.889,1.589,6.478,2.213a14.634,14.634,0,0,1-20.553.073Z"
                                  transform="translate(1686.795 30.795)"/>
                        </g>
                    </svg>
                </div>
                @if(Session::has('accessToken'))
                <div class="media-content align-self" onclick="profile()">
                @else
                <div class="media-content align-self" onclick="alert('anda harus login dulu')">
                @endif
                    <div class="content is-marginless has-text-left">
                        <a><h3 class="subtitle is-6 is-marginless is-text-blue">Akun</h3></a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>