<div class="hero-footer is-back-blue">
    <div class="container">
        <div class="columns">
            <div class="column">
                <ul>
                    <span class="fw500 is-size-5 mb1 block">Panduan &amp; Bantuan</span>
                    <li><a href="#">Bantuan Chat</a></li>
                    <li><a href="#">Daftar Pertanyaan (FAQ)</a></li>
                    <li><a href="#">Panduan Layanan</a></li>
                    <li><a href="#">Hubungi Kami</a></li>
                </ul>
            </div>
            <div class="column">
                <ul>
                    <span class="fw500 is-size-5 mb1 block">Legal</span>
                    <li><a href="kebijakan-privasi.html">Kebijakan Privasi</a></li>
                    <li><a href="syarat-ketentuan.html">Syarat &amp; Ketentuan</a></li>
                    <li><a href="sanggahan.html">Sanggahan</a></li>
                </ul>
            </div>
            <div class="column">
                <ul>
                    <span class="fw500 is-size-5 mb1 block">Pruduk</span>
                    <li><a href="#">AqiqahQ</a></li>
                    <li><a href="pilih-hewan.html">QurbanQ</a></li>
                </ul>
            </div>
        </div>
        <hr>
        <p class="has-text-centered">(c) 2018 AkadBaiq.com. Hak cipta dilindungi undang-undang</p>
    </div>
</div>