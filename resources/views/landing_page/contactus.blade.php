@extends('landing_page.layout.index')

@section('content')
    <div class="is-back-grey">
        <div class="container hero-body">
            <div class="content has-text-centered">
                <h3 class="title is-size-3 fw300 has-text-centered">
                    Hubungi Kami
                </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
            </div>
        </div>
    </div>
    <div class="container hero-body">
        <div class="columns">
            <div class="column is-6">
                <div class="shadow-box">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6575.001973462692!2d106.92058363405326!3d-6.212866872223972!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e698caeeb7e624b%3A0x39aa0852a1601dcc!2sJl.+Buaran+Raya%2C+Duren+Sawit%2C+Kota+Jakarta+Timur%2C+Daerah+Khusus+Ibukota+Jakarta%2C+Indonesia!5e0!3m2!1sen!2sus!4v1530004921807" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                    <div class="content map-info">
                        <div class="title is-size-4 fw300">
                            AkadbaiQ
                        </div>
                        <ul>
                            <li>Jl.Babelan  No. 5, Pejajaran barat, Pasar Minggu, Jakarta Barat, 12334, ID</li>
                            <li>Telephone: +62217495789098</li>
                            <li>E-Mail: sales@AkadbaiQ.com</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="column is-6">
                <div class="field">
                    <label class="label">Nama</label>
                    <div class="control">
                        <input class="input is-primary inp_name" type="text" placeholder="Nama Lengkap">
                    </div>
                </div>
                <div class="field mt1">
                    <label class="label">Email</label>
                    <div class="control">
                        <input class="input is-primary inp_email" type="text" placeholder="Email Anda">
                    </div>
                </div>
                <div class="field">
                    <label class="label">Pesan</label>
                    <div class="control">
                        <textarea class="textarea is-primary" type="text" placeholder="Warning textarea"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="field has-text-centered">
            <a class="button btn-akadQ mt2 w-250" id="setuju">Submit</a>
        </div>
    </div>

    @include('landing_page.layout.footer_content')
@endsection

@section('script')
    <script>
        const GlobalJS = new Global()

        $(document).ready(function(){
            checkLogin()
        })

        function checkLogin(){
            if("{{ Session::has('accessToken') }}"){
                $(".box-login").hide();
                $(".account_box").css("display","flex");
                $(".register_btn").html("Ajukan Kemitraan");
                $(".menu-ternak").find(".register_btn").attr("href","pengajuan-mitra1.html");
                $(".box-buy").css("display","block")

                getProfileApi()
            }
        }

        function getProfileApi(){
            var methodRequest = "GET"
            var urlRequest = "{{ url('/auth/get-profile') }}"
            var headerRequest = ""
            var dataRequest = ""
            var mitra_numb = 0
            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseProfile => {
                if(responseProfile.success) {

                }else{
                    alert(responseProfile.data.message)
                }
            })
        }
    </script>
@endsection