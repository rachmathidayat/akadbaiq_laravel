@extends('landing_page.layout.index')

@section('content')
    <!-- banner -->
    <div class="relative">
        <!-- login -->
        <div class="container">
            @if(!Session::has('accessToken'))
            <div class="box-login is-hidden-mobile">
                <div class="field icon margin-auto flex">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.585 47.496" width="40">
                        <defs>
                            <style>
                                .cls-1 {
                                    fill: #00aeef;
                                }

                                .cls-2 {
                                    fill: #ffc60b;
                                }
                            </style>
                        </defs>
                        <g id="Group_7" data-name="Group 7" transform="translate(-494.815 230.513)">
                            <g id="Group_2" data-name="Group 2" transform="translate(494.815 -230.513)">
                                <path id="Path_8" data-name="Path 8" class="cls-1"
                                      d="M131.021,46.014V16.3s0-9.5-9.5-9.5H84.5v38s0,9.5,9.5,9.5h21.678V46.258H91.32V14.351h32.151v31.42h7.551ZM99.845,40.169H115.92V20.927H99.845V40.169Z"
                                      transform="translate(-84.5 -6.8)"/>
                            </g>
                            <g id="Group_3" data-name="Group 3" transform="translate(541.336 -191.298)">
                                <path id="Path_9" data-name="Path 9" class="cls-2" d="M110.663,22.9H103.6v8.038h7.063Z"
                                      transform="translate(-103.6 -22.9)"/>
                            </g>
                        </g>
                    </svg>
                </div>
                <form id="formLogin">
                    <div class="field">
                        <div class="control">
                            <input class="input is-primary inp_email" type="text" placeholder="Email">
                        </div>
                    </div>
                    <div class="field">
                        <div class="control">
                            <input class="input is-primary" type="password" placeholder="Password" id="inp_password">
                        </div>
                    </div>
                    <div class="field">
                        <a class="button btn-akadQ is-fullwidth login_btn">Masuk ke AkadbaiQ</a>
                    </div>
                </form>
                <div class="field mb0">
                    <label class="checkbox">
                        <input type="checkbox">
                        Ingat Saya
                    </label>
                    <a class="is-pulled-right is-text-dark">Lupa Password</a>
                </div>
                <hr>
                <div class="field">
                    <button class="button is-fullwidth loginBtn loginBtn--facebook">
                        Login with Facebook
                    </button>
                </div>
                <div class="field">
                    <button class="button is-fullwidth loginBtn loginBtn--google">
                        Login with Google
                    </button>
                </div>
                <div class="field has-text-centered no-account">
                    Belum punya akun? <a href="{{ url('/daftar') }}" class="daftar is-text-dark">DAFTAR</a>
                </div>
            </div>
            @endif
        </div>
        <div class="swiper-container swiper1 is-desktop">
            <!-- banner slider -->
            <div class="swiper-wrapper">
                <div class="swiper-slide"><img src="{{ asset('assets/img/banner-1.jpg') }}"></div>
                <div class="swiper-slide"><img src="{{ asset('assets/img/banner-2.jpg') }}"></div>
                <div class="swiper-slide"><img src="{{ asset('assets/img/banner-3.jpg') }}"></div>
            </div>
            <!-- pagination -->
            <div class="swiper-pagination pag1"></div>
            <!-- button -->
            <div class="container box-buy none">
                <a href="{{ url('pilih-program') }}" class="buy-now align-items">
                    <div class="margin-auto">
                        <div class="for-buy title fw500 is-text-white">Mau Beli <br>Hewan ?</div>
                        <div class="flex is-text-white">
                            Beli Sekarang
                            <span>
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" width="20" class="ml0h">
                    <defs>
                      <style>
                        .arrow-right {
                            fill: #fff;
                        }
                      </style>
                    </defs>
                    <g id="up-arrow" transform="translate(42.887 0.005) rotate(90)">
                      <path id="Path_30826" data-name="Path 30826" class="arrow-right"
                            d="M39.995,22.887a20,20,0,1,0-20,20,20,20,0,0,0,20-20Zm-38.4,0A18.4,18.4,0,1,1,20,41.287a18.421,18.421,0,0,1-18.4-18.4Z"
                            transform="translate(0 0)"/>
                      <path id="Path_30827" data-name="Path 30827" class="arrow-right"
                            d="M25.056,18.944l1.132-1.132L16.459,8.083,6.731,17.812l1.132,1.132,7.8-7.8V30.558h1.6V11.147Z"
                            transform="translate(3.538 2.729)"/>
                    </g>
                  </svg>
                </span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <!-- video -->
    <div class="is-back-grey">
        <div class="container hero-body">
            <div class="content has-text-centered">
                <h1 class="title fw300">
                    <span class="fw600">Selalu dapatkan ternak</span> yang Anda impikan. <br>
                    <span class="fw300">
              Cara terbaik untuk <span class="fw600">Qurban</span> dan <span class="fw600">Aqiqah.</span></span>
                    </span>
                </h1>
                <img class="pointer" src="{{ asset('assets/img/download-app.png') }}" width="150">
            </div>
            <div class="columns pt2">
                <div class="column">
                    <div class="youtube">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 142.82 101.063" width="60">
                            <defs>
                                <style>
                                    .youtube {
                                        fill: #f61c0d;
                                    }
                                </style>
                            </defs>
                            <path id="youtube" class="youtube"
                                  d="M113.158,67.393h-83.5A29.662,29.662,0,0,0,0,97.055v41.739a29.662,29.662,0,0,0,29.662,29.662h83.5a29.662,29.662,0,0,0,29.662-29.662V97.055A29.662,29.662,0,0,0,113.158,67.393ZM93.1,119.955,54.044,138.581a1.568,1.568,0,0,1-2.243-1.415V98.749a1.568,1.568,0,0,1,2.276-1.4l39.054,19.791A1.568,1.568,0,0,1,93.1,119.955Z"
                                  transform="translate(0 -67.393)"/>
                        </svg>
                    </div>
                </div>
            </div>
            <div class="columns is-multiline pt2">
                <div class="column is-12">
                    <div class="columns">
                        <div class="column is-3">
                            <img src="{{ asset('assets/img/peternak.png') }}" class="flex margin-auto" width="100">
                        </div>
                        <div class="column">
                            <div class="content">
                                <div class="title is-size-5 mt1">
                                    Mengapa customer memilih AkadBaiQ untuk berqurban dan beraqiqah?
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisc ingelit, sed do eiusmod tempor incidi
                                    dunt utlab ore et dolor e magna aliqua. Ut enim ad minime niam, quis nostr
                                    exercitation ullamco laboris nisi ut aliquip ex e a commodo consequat. </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-12">
                    <div class="columns">
                        <div class="column is-3">
                            <img src="{{ asset('assets/img/peternak.png') }}" class="flex margin-auto" width="100">
                        </div>
                        <div class="column">
                            <div class="content">
                                <div class="title is-size-5 mt1">
                                    Mengapa mitra memilih berpatner dengan AkadBaiQ
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisc ingelit, sed do eiusmod tempor incidi
                                    dunt utlab ore et dolor e magna aliqua. Ut enim ad minime niam, quis nostr
                                    exercitation ullamco laboris nisi ut aliquip ex e a commodo consequat. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- flow -->
    <div class="container hero-body has-text-centered">
        <div class="content">
            <div class="title"><span class="flow-save">Metode Menabung</span></div>
        </div>
        <img src="{{ asset('assets/img/flow.png') }}" width="70%" class="mt2">
    </div>
    <!-- content 1 -->
    <div class="how-to is-back-grey">
        <div class="container">
            <div class="columns is-multiline">
                <div class="column is-4">
                    <div class="has-text-centered">
                        <img src="{{ asset('assets/img/pencet-tombol.png') }}" width="60%">
                        <div class="content">
                            <div class="title is-size-5 mt1">
                                Cari dan temukan ternaknya
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisc ingelit, sed do eiusmod tempor incidi
                                dunt utlab ore et dolor e magna aliqua. Ut enim ad minime niam, quis nostr exercitation
                                ullamco laboris nisi ut aliquip ex e a commodo consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="has-text-centered">
                        <img src="{{ asset('assets/img/metode-pembayaran.png') }}" width="60%">
                        <div class="content">
                            <div class="title is-size-5 mt1">
                                Pilih cara pembayaran
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisc ingelit, sed do eiusmod tempor incidi
                                dunt utlab ore et dolor e magna aliqua. Ut enim ad minime niam, quis nostr exercitation
                                ullamco laboris nisi ut aliquip ex e a commodo consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="has-text-centered">
                        <img src="{{ asset('assets/img/rating.png') }}" width="60%">
                        <div class="content">
                            <div class="title is-size-5 mt1">
                                Beri rating
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisc ingelit, sed do eiusmod tempor incidi
                                dunt utlab ore et dolor e magna aliqua. Ut enim ad minime niam, quis nostr exercitation
                                ullamco laboris nisi ut aliquip ex e a commodo consequat. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- estimasi harga -->
    <div class="container hero-body">
        <div class="columns">
            <div class="column is-4">
                <div class="content">
                    <div class="title is-size-3 fw300">
                        <span class="fw600">Estimasi</span> Harga
                    </div>
                    <div class="field">
                        <label class="label">Pilih Jenis Hewan</label>
                        <div class="control">
                            <div class="select is-fullwidth">
                                <select class="selectEstimasiHarga">
                                    <option>Pilih Jenis Hewan</option>
                                    <!-- <optgroup label="Sapi">
                                      <option value="Sapi Jawa">Sapi Jawa</option>
                                      <option value="Sapi Limosin">Sapi Limosin</option>
                                    </optgroup>
                                    <optgroup label="Kambing">
                                      <option value="Kambing Etawa">Kambing Etawa</option>
                                      <option value="Kambing Kacang">Kambing Kacang</option>
                                    </optgroup>
                                    <optgroup label="Domba">
                                      <option value="Kambing Etawa">Domba Garut</option>
                                      <option value="Kambing Kacang">Domba Batur</option>
                                    </optgroup> -->
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label class="label">Pilih Kota</label>
                        <div class="control">
                            <div class="select is-fullwidth">
                                <select>
                                    <option value="Jakarta">Jakarta</option>
                                    <option value="Bogor">Bogor</option>
                                    <option value="Depok">Depok</option>
                                    <option value="Tangerang">Tangerang</option>
                                    <option value="Bekasi">Bekasi</option>
                                    <option value="Karawang">Karawang</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <a class="button btn-akadQ is-fullwidth mt2" onclick="filterternak()">Cari</a>
                    </div>
                </div>
            </div>
            <div class="column display_info">
                <!-- no data -->
                <div class="box-no-data">
                    <div class="no-data-estimasi">
                        <img src="{{ asset('assets/img/sapi.png') }}" width="50%">
                        <div class="content text-no-data is-size-3 has-text-centered">
                            Lakukan Pencarian hewan ternak <br>yang Anda cari
                        </div>
                    </div>
                </div>
                <!-- data -->
                <div class="box-have-data none">
                    <ul class="grade-info">
                        <li class="menu-grade is-active" id="menugrade1" onclick="selectgrade(1)">Grade A</li>
                        <li class="menu-grade" id="menugrade2" onclick="selectgrade(2)">Grade B</li>
                        <li class="menu-grade" id="menugrade3" onclick="selectgrade(3)">Grade C</li>
                    </ul>
                    <!-- grade a -->
                    <div class="grade" id="tablegrade1">
                        <table class="table table-grade is-fullwidth is-striped">
                            <tbody id="tableGradeA">
                            <!-- <tr>
                              <td>Jumlah</td>
                              <td>: &nbsp;8ekor</td>
                            </tr>
                            <tr>
                              <td>Umur</td>
                              <td>: &nbsp;2 ~ 3tahun</td>
                            </tr>
                            <tr>
                              <td>Panjang</td>
                              <td>: &nbsp;80 ~ 100cm</td>
                            </tr>
                            <tr>
                              <td>Tinggi</td>
                              <td>: &nbsp;40cm ~ 50cm</td>
                            </tr>
                            <tr>
                              <td>Lingkar Dada</td>
                              <td>: &nbsp;120  - 160cm</td>
                            </tr>
                            <tr>
                              <td>Berat</td>
                              <td>: &nbsp;55 ~ 90kg</td>
                            </tr>
                            <tr>
                              <td>Harga</td>
                              <td>: &nbsp;Rp 2.500.000 ~ Rp 3.500.000</td>
                            </tr> -->
                            </tbody>
                        </table>
                    </div>
                    <!-- grade b -->
                    <div class="grade none" id="tablegrade2">
                        <table class="table table-grade is-fullwidth is-striped">
                            <tbody>
                            <tr>
                                <td>Jumlah</td>
                                <td>: &nbsp;2ekor</td>
                            </tr>
                            <tr>
                                <td>Umur</td>
                                <td>: &nbsp;2 ~ 2.5tahun</td>
                            </tr>
                            <tr>
                                <td>Panjang</td>
                                <td>: &nbsp;70 ~ 80cm</td>
                            </tr>
                            <tr>
                                <td>Tinggi</td>
                                <td>: &nbsp;30cm ~ 40cm</td>
                            </tr>
                            <tr>
                                <td>Lingkar Dada</td>
                                <td>: &nbsp;100 - 120cm</td>
                            </tr>
                            <tr>
                                <td>Berat</td>
                                <td>: &nbsp;50 ~ 70kg</td>
                            </tr>
                            <tr>
                                <td>Harga</td>
                                <td>: &nbsp;Rp 2.000.000 ~ Rp 2.500.000</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- grade c -->
                    <div class="grade none" id="tablegrade3">
                        <table class="table table-grade is-fullwidth is-striped">
                            <tbody>
                            <tr>
                                <td>Jumlah</td>
                                <td>: &nbsp;12ekor</td>
                            </tr>
                            <tr>
                                <td>Umur</td>
                                <td>: &nbsp;1 ~ 2tahun</td>
                            </tr>
                            <tr>
                                <td>Panjang</td>
                                <td>: &nbsp;50 ~ 70cm</td>
                            </tr>
                            <tr>
                                <td>Tinggi</td>
                                <td>: &nbsp;20cm ~ 30cm</td>
                            </tr>
                            <tr>
                                <td>Lingkar Dada</td>
                                <td>: &nbsp;80 - 100cm</td>
                            </tr>
                            <tr>
                                <td>Berat</td>
                                <td>: &nbsp;25 ~ 50kg</td>
                            </tr>
                            <tr>
                                <td>Harga</td>
                                <td>: &nbsp;Rp 1.000.000 ~ Rp 2.000.000</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- benefit mitra -->
    <div class="is-back-grey">
        <div class="container hero-body">
            <div class="title fw300 has-text-centered">
                <span class="fw600">Jualan yang menguntungkan Anda.</span><br> Jual dan dapatkan keuntungan setiap bulan
            </div>
            <div class="columns is-multiline">
                <div class="column is-4">
                    <div class="benefit-mitra shadow-box has-text-centered">
                        <img src="{{ asset('assets/img/jadwal.svg') }}" width="80">
                        <div class="content">
                            <div class="title is-size-5 mt1">
                                Jadwalkan sendiri
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisc ingelit, sed do eiusmod tempor incidi
                                dunt utlab ore et dolor e magna aliqua. Ut enim ad minime niam, quis nostr exercitation
                                ullamco laboris nisi ut aliquip ex e a commodo consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="benefit-mitra shadow-box has-text-centered">
                        <img src="{{ asset('assets/img/cara-jual.svg') }}" width="80">
                        <div class="content">
                            <div class="title is-size-5 mt1">
                                Pilih Cara Penjualan
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisc ingelit, sed do eiusmod tempor incidi
                                dunt utlab ore et dolor e magna aliqua. Ut enim ad minime niam, quis nostr exercitation
                                ullamco laboris nisi ut aliquip ex e a commodo consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="benefit-mitra shadow-box has-text-centered">
                        <img src="{{ asset('assets/img/serahkan.svg') }}" width="80">
                        <div class="content">
                            <div class="title is-size-5 mt1">
                                Serahkan pada aplikasi kami
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisc ingelit, sed do eiusmod tempor incidi
                                dunt utlab ore et dolor e magna aliqua. Ut enim ad minime niam, quis nostr exercitation
                                ullamco laboris nisi ut aliquip ex e a commodo consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="benefit-mitra shadow-box has-text-centered">
                        <img src="{{ asset('assets/img/daftar.svg') }}" width="80">
                        <div class="content">
                            <div class="title is-size-5 mt1">
                                Daftar Online
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisc ingelit, sed do eiusmod tempor incidi
                                dunt utlab ore et dolor e magna aliqua. Ut enim ad minime niam, quis nostr exercitation
                                ullamco laboris nisi ut aliquip ex e a commodo consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="benefit-mitra shadow-box has-text-centered">
                        <img src="{{ asset('assets/img/document.svg') }}" width="80">
                        <div class="content">
                            <div class="title is-size-5 mt1">
                                Penyerahan Dokumen
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisc ingelit, sed do eiusmod tempor incidi
                                dunt utlab ore et dolor e magna aliqua. Ut enim ad minime niam, quis nostr exercitation
                                ullamco laboris nisi ut aliquip ex e a commodo consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="benefit-mitra shadow-box has-text-centered">
                        <img src="{{ asset('assets/img/pasang.svg') }}" width="80">
                        <div class="content">
                            <div class="title is-size-5 mt1">
                                Pasang Aplikasi dan Mulai
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisc ingelit, sed do eiusmod tempor incidi
                                dunt utlab ore et dolor e magna aliqua. Ut enim ad minime niam, quis nostr exercitation
                                ullamco laboris nisi ut aliquip ex e a commodo consequat. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- partner -->
    <div class="container hero-body">
        <div class="title fw300 has-text-centered">
            Our Partner
        </div>
        <div class="columns">
            <div class="column has-text-centered"><img src="{{ asset('assets/img/partnership/jasindo.png') }}"
                                                       width="200"></div>
            <div class="column has-text-centered"><img src="{{ asset('assets/img/partnership/aspin.png') }}"
                                                       width="200"></div>
            <div class="column has-text-centered"><img src="{{ asset('assets/img/partnership/gapuspindo.jpg') }}"></div>
        </div>
        <!-- <div class="swiper-container swiper2">
          <div class="swiper-wrapper align-items">
            <div class="swiper-slide"><img src="img/partnership/jasindo.png" width="200"></div>
            <div class="swiper-slide"><img src="img/partnership/aspin.png" width="200"></div>
            <div class="swiper-slide"><img src="img/partnership/gapuspindo.jpg"></div>
          </div>
        </div>
        <div class="swiper-pagination pag2"></div> -->
    </div>
    <!-- testi -->
    <div class="is-back-grey">
        <div class="container hero-body">
            <div class="title fw300 has-text-centered">
                Testimonial
            </div>
            <div class="columns">
                <div class="column">
                    <div class="box-testi shadow-box">
                        <img src="{{ asset('assets/img/testi1.jpg') }}">
                        <div class="content has-text-centered">
                            <div class="title is-size-5 mt1">
                                John_Witch
                            </div>
                            <div class="subtitle is-size-6 fw300">
                                Kebayoran Baru
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisc ingelit, sed do eiusmod tempor incidi
                                dunt utlab ore et dolor e magna aliqua. Ut enim ad minime niam, quis nostr exercitation
                                ullamco laboris nisi ut aliquip ex e a commodo consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="box-testi shadow-box">
                        <img src="{{ asset('assets/img/testi2.jpg') }}">
                        <div class="content has-text-centered">
                            <div class="title is-size-5 mt1">
                                Daniel
                            </div>
                            <div class="subtitle is-size-6 fw300">
                                Depok Timur
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisc ingelit, sed do eiusmod tempor incidi
                                dunt utlab ore et dolor e magna aliqua. Ut enim ad minime niam, quis nostr exercitation
                                ullamco laboris nisi ut aliquip ex e a commodo consequat. </p>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="box-testi shadow-box">
                        <img src="{{ asset('assets/img/testi3.jpg') }}">
                        <div class="content has-text-centered">
                            <div class="title is-size-5 mt1">
                                Sunny K
                            </div>
                            <div class="subtitle is-size-6 fw300">
                                Pasar Minggu
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisc ingelit, sed do eiusmod tempor incidi
                                dunt utlab ore et dolor e magna aliqua. Ut enim ad minime niam, quis nostr exercitation
                                ullamco laboris nisi ut aliquip ex e a commodo consequat. </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field has-text-centered">
                <a href="testimonial.html" class="button btn-akadQ mt2 w-250">Lihat Semua</a>
            </div>
        </div>
    </div>
    <!-- footer -->
    <div class="hero-footer is-back-blue">
        <div class="container">
            <div class="columns">
                <div class="column">
                    <ul>
                        <span class="fw500 is-size-5 mb1 block">Panduan &amp; Bantuan</span>
                        <li><a href="#">Bantuan Chat</a></li>
                        <li><a href="#">Daftar Pertanyaan (FAQ)</a></li>
                        <li><a href="#">Panduan Layanan</a></li>
                        <li><a href="#">Hubungi Kami</a></li>
                    </ul>
                </div>
                <div class="column">
                    <ul>
                        <span class="fw500 is-size-5 mb1 block">Legal</span>
                        <li><a href="kebijakan-privasi.html">Kebijakan Privasi</a></li>
                        <li><a href="syarat-ketentuan.html">Syarat &amp; Ketentuan</a></li>
                        <li><a href="#">Sanggahan</a></li>
                    </ul>
                </div>
                <div class="column">
                    <ul>
                        <span class="fw500 is-size-5 mb1 block">Produk</span>
                        <li><a href="#">AqiqahQ</a></li>
                        <li><a href="pilih-hewan.html">QurbanQ</a></li>
                    </ul>
                </div>
            </div>
            <hr>
            <p class="has-text-centered">(c) 2018 AkadBaiq.com. Hak cipta dilindungi undang-undang</p>
        </div>
    </div>
@endsection

@section('script')
    <script>
        const GlobalJS = new Global()

        $(document).ready(function(){
            checkLogin()
            getJenisHewan()
        })

        function checkLogin(){
            if("{{ Session::has('accessToken') }}"){
                $(".box-login").hide();
                $(".account_box").css("display","flex");
                $(".register_btn").html("Ajukan Kemitraan");
                $(".menu-ternak").find(".register_btn").attr("href","pengajuan-mitra1.html");
                $(".box-buy").css("display","block")

                getSaldo();
                getProfileApi();
            }
        }

        $("#formLogin input").keypress(function(e) {
            if (e.keyCode == 13) {
                processLogin()
            }
        })

        $(".login_btn").click(function(){
            processLogin()
        })

        function processLogin(){
            email=$(".inp_email").val() ;
            pass= $("#inp_password").val();

            if(email == '' || pass == ''){
                alert("email and password required")
                return false
            }

            var methodRequest = "POST"
            var urlRequest = "{{ url('/auth/login') }}"
            var headerRequest = ""
            var dataRequest = {
                username: email,
                password: pass,
            }

            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseLogin => {
                if(responseLogin.success){
                    $(".box-login").hide();
                    $(".account_box").css("display","flex");
                    localStorage.setItem('aq_login_status', "login");
                    localStorage.setItem('aq_acc_token', responseLogin.data.accessToken);
                    $(".menu-ternak").find(".register_btn").attr("href","pengajuan-mitra1.html");
                    $(".register_btn").html("Ajukan Kemitraan");
                    $(".box-buy").css("display","block");

                    getProfileApi().then((responseProfile) => {
                        if(responseProfile.success) {
                            location.reload()
                        }else{
                            alert(responseProfile.data.message)
                        }
                    })
                }else{
                    alert(responseLogin.data.message)
                }
            })
        }

        function getProfileApi(){
            var methodRequest = "GET"
            var urlRequest = "{{ url('/auth/get-profile') }}"
            var headerRequest = ""
            var dataRequest = ""
            var mitra_numb = 0

            let promise = new Promise((resolve, reject) => {
                GlobalJS.requestAjaxSync(methodRequest, urlRequest, headerRequest, dataRequest).then(responseProfile => {
                    resolve(responseProfile)
                })
            })

            return promise
        }

        function getSaldo(){
            var methodRequest = "GET"
            var urlRequest = "{{ url('/auth/get-profile') }}"
            var headerRequest = ""
            var dataRequest = ""

            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseSaldo => {
                if(responseSaldo.success) {

                }else{
                    alert(responseLogin.data.message)
                }
            })
        }

        function getJenisHewan(){
            var methodRequest = "GET"
            var urlRequest = "{{ env("HOST_API") }}/api/animal"
            var headerRequest = {
                Authorization: "{{ Session::get('accessToken') }}",
                'Accept-Version': '1.0'
            }
            var dataRequest = ''

            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseJenisHewan => {
                var jenisHewan = responseJenisHewan.data
                var selectEstimasiHarga = $('.selectEstimasiHarga')
                selectEstimasiHarga.html("")

                var jenisHewanTmp = []
                for (let i = 0; i < jenisHewan.length; i++) {

                    var response1 = []
                    for (let j = 0; j < jenisHewan.length; j++) {

                        if (jenisHewan[i].commodityType == jenisHewan[j].commodityType) {
                            response1.push(jenisHewan[j].name)
                        }

                    }

                    var response2 = {
                        commodityType: jenisHewan[i].commodityType,
                        name: response1
                    }

                    jenisHewanTmp.push(response2)

                }

                var jenisHewanTmp = unique(jenisHewanTmp)

                jenisHewanTmp.forEach(function(item, index){
                    selectEstimasiHarga.append(`<optgroup label="` + item.commodityType + `" id="parent` + index + `"></optgroup>`)
                    item.name.forEach(function(itemChild, indexChild){
                    $('#parent'+index).append(`<option value="` + itemChild + `">` + itemChild + `</option>`)
                    })
                })
            })
        }

        function unique(jenisHewanTmp){
            jenisHewanTmp = jenisHewanTmp.filter((item, index, self) =>
                index === self.findIndex((t) => (
                    t.commodityType === item.commodityType
                ))
            )

            return jenisHewanTmp
        }

        function filterternak() {
            getGradeHewan()
            $('.box-no-data').addClass('none')
            $('.box-have-data').addClass('block')
            $('.box-have-data').removeClass('none')
        }

        function selectgrade(id){
            $div = $('#menugrade'+id).addClass('is-active');
            $('.menu-grade').not($div).removeClass('is-active');
            $diva = $('#tablegrade'+id).addClass('block').removeClass('none');
            $('.grade').not($diva).removeClass('block').addClass('none');
        }

        function getGradeHewan(){
            var methodRequest = "GET"
            var urlRequest = "{{ env("HOST_API") }}/api/grade/"
            var headerRequest = {
                'Accept-Version': '1.0'
            }
            var dataRequest = ''

            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseGrade => {
                var gradeA = responseGrade.data.grades
                var tableGradeA = $('#tableGradeA')
                tableGradeA.html("")
                tableGradeA.append(`<tr>
                        <td>Jumlah</td>
                        <td>: &nbsp;8ekor</td>
                    </tr>
                    <tr>
                        <td>Umur</td>
                        <td>: &nbsp;2 ~ 3tahun</td>
                    </tr>
                    <tr>
                        <td>Panjang</td>
                        <td>: &nbsp;80 ~ 100cm</td>
                    </tr>
                    <tr>
                        <td>Tinggi</td>
                        <td>: &nbsp;`+gradeA[0].lowestHeight+`cm ~ `+gradeA[0].highestHeight+`cm</td>
                    </tr>
                    <tr>
                        <td>Lingkar Dada</td>
                        <td>: &nbsp;120  - 160cm</td>
                    </tr>
                    <tr>
                        <td>Berat</td>
                        <td>: &nbsp;`+gradeA[0].lowestWeight+` ~ `+gradeA[0].highestWeight+`kg</td>
                    </tr>
                    <tr>
                        <td>Harga</td>
                        <td>: &nbsp;Rp `+number_format(gradeA[0].lowestPrice)+` ~ Rp `+number_format(gradeA[0].highestPrice)+`</td>
                    </tr>`)
            })
        }
    </script>
@endsection