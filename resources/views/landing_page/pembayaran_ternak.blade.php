@extends('landing_page.layout.index')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/extension.css') }}">
@endsection

@section('content')
    <div class="container">
      <div class="content has-text-centered">
        <div class="title is-size-4 mt3">
         Metode Pembayaran
        </div>
        <p>Silahkan pilih metode pembayaran yang sesuai dengan keinginan Anda.</p>
      </div>
      <input type="hidden" id="catalogueUUID" />
      <div class="columns col-netral">
        <div class="column is-4">
          <div class="shadow-box left-metode komoditas-detail">
            <div class="columns col-netral">
              <div class="column">
                <div class="img-transaksi h-100">
                    <img id="image_komoditas_detail" src="{{ asset('assets/img/sapi-1-min.jpg') }}">
                </div>
              </div>
              <div class="column">
                <div class="content">
                  <div class="fw500" id="nama_komoditas_detail">-</div>
                  <div class="small" id="price_komoditas_detail">Rp. 0</div>
                </div>
              </div>
            </div>
          </div>
          <div class="left-metode mt1">
            <div class="level mb0h">
              <div class="level-left">
                <div class="level-item small">Pengiriman</div>
              </div>
              <div class="level-right">
                <div class="level-item small" id="shippingCost">Rp 700.000</div>
              </div>
            </div>
            <hr>
            <div class="level">
              <div class="level-left">
                <div class="level-item fw500">Total Harga</div>
              </div>
              <div class="level-right">
                <div class="level-item fw500" id="subTotal">Rp 30.700.000</div>
              </div>
            </div>
          </div>
        </div>
        <div class="column">
          <div class="shadow-box right-metode pengiriman-detail">
            <div class="columns col-netral">
              <div class="column">
                <div class="content desc-ternak">
                  <div class="farm-name fw500">Alamat Pengiriman</div>
                  <div class="small" id="alamat_pengiriman">-</div>
                </div>
              </div>
            </div>
          </div>
          <div class="shadow-box right-metode mt2">
            <!-- <div class="field metode">
              <label class="label mb1">Cara Pembayaran</label>
              <input class="is-checkradio is-info" id="tunai" type="radio" name="carabayar" checked="checked" onclick="pilih('dkm')">
              <label for="tunai" class="ml0">Tunai</label>
              <input class="is-checkradio is-info" id="tabungan" type="radio" name="carabayar" onclick="pilih('pribadi')">
              <label for="tabungan">Tabungan</label>
            </div> -->
            <!-- tunai -->
            <div class="tunai">
              <div class="columns col-netral">
                <div class="column">
                  <div class="field mt1">
                    <label class="label has-text-left">Metode Pembayaran</label>
                    <div class="control">
                      <div class="select is-fullwidth">
                        <select name="paymentMethod" id="paymentMethod">
                          <option value="BANK_TRANSFER">Transfer Bank</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="field mt1">
                    <label class="label has-text-left">Tanggal Pengiriman</label>
                    <div class="control">
                      <input class="input" type="date" name="shippingDate" id="shippingDate" placeholder="dd/mm/yyyy">
                    </div>
                  </div>
                  <div class="field mt1">
                    <label class="label has-text-left">Pilih Tanggal Tagihan</label>
                    <div class="control">
                      <div class="select is-fullwidth">
                        <select name="dueDate" id="dueDate">
                          <option value="1">1</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="column">
                  <div class="field mt1">
                    <label class="label has-text-left">Pilihan Bank</label>
                    <div class="control">
                      <div class="select is-fullwidth">
                        <select name="bank" id="bank">
                          <option value="Bank Mandiri">Bank Mandiri</option>
                          <option value="Bank BNI">Bank BNI</option>
                          <option value="Bank BCA">Bank BCA</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="field mt1">
                    <label class="label has-text-left">Tempo Tabungan</label>
                    <div class="control">
                      <div class="select is-fullwidth">
                        <select name="paymentTerms" id="paymentTerms">
                          <option value="1">1 Kali Bayar</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- tabungan -->
            <div class="tabungan none">
              <div class="columns col-netral">
                <div class="column">
                  <div class="field mt1">
                    <label class="label has-text-left">Metode Pembayaran</label>
                    <div class="control">
                      <div class="select is-fullwidth">
                        <select>
                          <option>Auto Debet</option>
                          <option>Transfer Bank</option>
                          <option>Indomaret</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="field mt1">
                    <label class="label has-text-left">Transfer per Tanggal</label>
                    <div class="control">
                      <input class="input" type="date" placeholder="dd/mm/yyyy">
                    </div>
                  </div>
                  <div class="field mt1">
                    <label class="label has-text-left">Nomor Rekening</label>
                    <div class="control">
                      <input class="input" type="number" placeholder="nomor rekening">
                    </div>
                  </div>
                  <div class="field mt1">
                    <label class="label has-text-left">Tanggal Pengiriman</label>
                    <div class="control">
                      <input class="input" type="date" placeholder="dd/mm/yyyy">
                    </div>
                  </div>
                </div>
                <div class="column">
                  <div class="field mt1">
                    <label class="label has-text-left">Pilihan Bank</label>
                    <div class="control">
                      <div class="select is-fullwidth">
                        <select>
                          <option>BCA</option>
                          <option>Mandiri</option>
                          <option>DKI</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="field mt1">
                    <label class="label has-text-left">Tenor Tabungan</label>
                    <div class="control">
                      <div class="select is-fullwidth">
                        <select>
                          <option>3</option>
                          <option>6</option>
                          <option>9</option>
                          <option>12</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="field mt1">
                    <label class="label has-text-left">Nama Rekening</label>
                    <div class="control">
                      <input class="input" type="text" placeholder="nama rekening">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="field mb3 mt2 has-text-centered">
        <a onclick="saveOrder()" class="button btn-akadQ w-250">Konfirmasi Pembayaran</a>
      </div>
    </div>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script>
    const GlobalJS = new Global()

    $(document).ready(function(){
      getAnimalDetail()
      setDueDate()
    })

    function setDueDate() {
      let dueDate = $('#dueDate')
      dueDate.html('')
      let no = 1
      for(let i = 0; i < 31; i++){
        dueDate.append(
          `<option value=${no}>${no}</option>`
        )
        no++
      }
    }

    function getAnimalDetail(){
      let animal_uuid = "{{ $animal_uuid }}"

      var methodRequest = "GET"
      var urlRequest = "{{ env("HOST_API") }}/api/animal/"+animal_uuid
      var headerRequest = {
          "Accept-Version":"1.0",
          Authorization:"{{ Session::get('accessToken') }}"
      }
      var dataRequest = ""

      GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
        console.log(response)
        
        let address = localStorage.getItem(`${localStorage.getItem('jenisTransaksi')}_address`)
        $('#image_komoditas_detail').attr('src', response.data.images[0].url)
        $('#nama_komoditas_detail').html(response.data.name)
        $('#price_komoditas_detail').html(`Rp. ${formatPrice(response.data.catalogue.price)}`)
        $('#alamat_pengiriman').html(address)
        $('#catalogueUUID').val(response.data.catalogue.uuid)

        getPrice(response.data.catalogue.uuid, response.data.catalogue.price)
        getMaxPaymentTerm(response.data.catalogue.uuid)
      })
    }

    function formatPrice(value) {
        let val = (value/1).toFixed(2).replace('.', ',')
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
    }

    function getPrice(catalogue_uuid, price) {

      let districtCode = localStorage.getItem(`${localStorage.getItem('jenisTransaksi')}_districtCode`)

      var methodRequest = "GET"
      var urlRequest = "{{ env("HOST_API") }}/api/catalogue/"+catalogue_uuid+'/price?districtCode='+districtCode
      var headerRequest = {
          "Accept-Version":"1.0",
          Authorization:"{{ Session::get('accessToken') }}"
      }
      var dataRequest = ""

      GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
        console.log(response)
        $('#shippingCost').html(`Rp. ${formatPrice(response.data.shippingCost)}`)
        $('#subTotal').html(`Rp. ${formatPrice(price + response.data.shippingCost)}`)
      })  
    }

    function getMaxPaymentTerm(catalogue_uuid){
      var methodRequest = "GET"
      var urlRequest = "{{ env("HOST_API") }}/api/catalogue/"+catalogue_uuid
      var headerRequest = {
          "Accept-Version":"1.0",
      }
      var dataRequest = ""

      GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
        console.log(response)
        let paymentTerms = $('#paymentTerms')
        paymentTerms.html('')
        
        let no = 1
        for(let i = 0; i < response.data.catalogue.maxPaymentTerms; i++){
          paymentTerms.append(`
            <option value="${no}">${no} Kali Bayar</option>
          `)
          no++
        }
      })
    }

    function saveOrder(){
      let orderType = ''
      if(localStorage.getItem('jenisTransaksi') == 'pribadi'){
        orderType = 'PERSONAL'
      }else{
        orderType = localStorage.getItem('jenisTransaksi').toUpperCase()
      }

      let dateShipping = moment($('#shippingDate').val()).format('DD-MM-YYYY')
      let shippingDate = dateShipping
      let data = {
        catalogueUuid: $('#catalogueUUID').val(),
        dkmName: localStorage.getItem('jenisTransaksi') == 'dkm' ? localStorage.getItem('dkm_dkmName') : '',
        receiverName: localStorage.getItem(`${localStorage.getItem('jenisTransaksi')}_receiverName`),
        email: localStorage.getItem(`${localStorage.getItem('jenisTransaksi')}_email`),
        phone: localStorage.getItem(`${localStorage.getItem('jenisTransaksi')}_phone`),
        address: localStorage.getItem(`${localStorage.getItem('jenisTransaksi')}_address`),
        provinceCode: localStorage.getItem(`${localStorage.getItem('jenisTransaksi')}_provinceCode`),
        districtCode: localStorage.getItem(`${localStorage.getItem('jenisTransaksi')}_districtCode`),
        subDistrictCode: localStorage.getItem(`${localStorage.getItem('jenisTransaksi')}_subDistrictCode`),
        lat: localStorage.getItem(`${localStorage.getItem('jenisTransaksi')}_lat`),
        lng: localStorage.getItem(`${localStorage.getItem('jenisTransaksi')}_lng`),
        transactionType: 'SAVING',
        paymentMethod: $('#paymentMethod').val(),
        bank: $('#bank').val(),
        zipCode: localStorage.getItem(`${localStorage.getItem('jenisTransaksi')}_zipCode`),
        shippingDate: shippingDate,
        paymentTerms: $('#paymentTerms').val(),
        dueDate: $('#dueDate').val(),
        orderType: orderType,
        persons: ''
      }

      console.log(data)

      var methodRequest = "POST"
      var urlRequest = "{{ route('save_order') }}"
      var headerRequest = ""
      var dataRequest = data

      GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
        console.log(response)
      })
    
    }
</script>
@endsection