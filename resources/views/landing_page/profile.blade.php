@extends('landing_page.layout.index')

@section('content')
    <div class="container">
        <div class="columns col-netral mt3">
            <div class="column is-3">
                <div class="shadow-box left-metode pic-profile">
{{--                    <img id="photoProfile" src="{{ asset('assets/img/pic-profile.jpg') }}">--}}
                    <img id="photoProfile">
                </div>
                <hr class="mt2">
            </div>
            <div class="column">
                <div class="detail-profile shadow-box">
                    <div class="title-profile has-border-yellow">
                        <div class="level">
                            <div class="level-left">
                                <div class="level-item is-size-4 fw500 name-profile">{{ Session::get('fullName') }}</div>
                            </div>
                            <div class="level-right">
                                <div class="level-item">
                                    <a class="button btn-akadQ w-100" onclick="profileUpdate()">Ubah</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-profile right-metode">
                        <table class="table for-profile">
                            <tbody>
                            <tr>
                                <td>Email</td>
                                <td>:</td>
                                <td class="email-profile">{{ Session::get('username') }}</td>
                            </tr>
                            <tr>
                                <td>No Hp/Telp</td>
                                <td>:</td>
                                <td class="phone-profile">{{ Session::get('phoneNumber') }}</td>
                            </tr>
                            <tr>
                                <td class="birthdate">Tanggal Lahir</td>
                                <td>:</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>:</td>
                                <td>{{ Session::get('address') }}</td>
                            </tr>
                            <tr>
                                <td>Mitra</td>
                                <td>:</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td>Komoditas Utama</td>
                                <td>:</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td>Status Kemitraan</td>
                                <td>:</td>
                                <td>-</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr class="mt2">
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        const GlobalJS = new Global()

        $(document).ready(function(){
            checkLogin()
        })

        function checkLogin(){
            if("{{ Session::has('accessToken') }}"){
                $(".box-login").hide();
                $(".account_box").css("display","flex");
                $(".register_btn").html("Ajukan Kemitraan");
                $(".menu-ternak").find(".register_btn").attr("href","pengajuan-mitra1.html");
                $(".box-buy").css("display","block")

                getProfileApi()
            }
        }

        function getProfileApi(){
            var methodRequest = "GET"
            var urlRequest = "{{ url('/auth/get-profile') }}"
            var headerRequest = ""
            var dataRequest = ""
            var mitra_numb = 0
            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseProfile => {
                if(responseProfile.success) {
                    $("#photoProfile").attr("src", responseProfile.data.userAccount.images[0].url)
            }else{
                alert(responseProfile.data.message)
            }
        })
        }

        function profileUpdate(){
            window.location = '/profile/update/{{ Session::get('userUUID') }}'
        }
    </script>
@endsection