@extends('dashboard.mitra.layout.index')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/extension.css') }}">
@endsection

@section('content')
    <div class="content has-text-centered">
        <div class="title">
            Edit Hewan
        </div>
        <p class="subtitle is-size-6 fw300">Masukan data-data hewan yang Anda jual dibawah ini secara lengkap</p>
        <!-- content -->
        <div class="tambah-ternak">
            <div class="columns col-netral is-centered mt1">
                <div class="column is-8">
                    <div class="columns col-netral">
                        <div class="column">
                            <div class="field mt1">
                                <label class="label has-text-left">Komoditas</label>
                                <div class="control">
                                    <div class="select is-fullwidth">
                                        <select class="sel_hewan">
                                            <option value="SAPI">Sapi</option>
                                            <option value="KAMBING">Kambing</option>
                                            <option value="DOMBA">Domba</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="column">
                            <div class="field mt1">
                                <label class="label has-text-left">Umur</label>
                                <div class="control">
                                    <input class="input inp_umur" type="number" placeholder="contoh: 1">
                                    <div class="small has-text-left req_age"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field1">
                        <div class="columns col-netral">
                            <div class="column">
                                <div class="field">
                                    <label class="label has-text-left">Jenis Komoditas</label>
                                    <div class="control">
                                        <div class="select is-fullwidth">
                                            <select class="sel_jenis">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="column">
                                <div class="field">
                                    <label class="label has-text-left">Display Nama Hewan</label>
                                    <div class="control">
                                        <input class="input inp_name" type="text" placeholder="Sapi Super Besar">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field2">
                        <div class="columns col-netral">
                            <div class="column">
                                <div class="field has-text-left mt1">
                                    <label class="label">Pilih Jenis Transaksi</label>
                                    <input class="is-checkradio is-info sel_trans" id="tabungan" type="checkbox" name="jenistransaksi" value="SAVING">
                                    <label for="tabungan" class="no-margin">Tabungan</label>
                                    <input class="is-checkradio is-info sel_trans" id="tunai" type="checkbox" name="jenistransaksi" value="CASH">
                                    <label for="tunai" class="no-margin">Tunai</label>
                                </div>
                            </div>
                            <div class="column">
                                <div class="field mt1">
                                    <label class="label has-text-left">Pilih Mitra</label>
                                    <div class="control">
                                        <div class="select is-fullwidth">
                                            <select class="sel_kandang">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field3">
                        <div class="columns col-netral">
                            <div class="column">
                                <div class="field has-text-left mt1">
                                    <label class="label">Hitung Berat</label>
                                    <input class="is-checkradio is-info method_weight" id="timbangan" type="radio" name="cekberat" checked="checked" value="AUTO" onclick="timbangan()">
                                    <label for="timbangan" class="ml0">Timbangan</label>
                                    <input class="is-checkradio is-info method_weight" id="manual" type="radio" name="cekberat" value="MANUAL" onclick="manual()">
                                    <label for="manual">Manual</label>
                                </div>
                            </div>
                            <div class="column is-6">
                                <div class="field mt1">
                                    <label class="label label-weight has-text-left">Berat saat ini</label>
                                    <div class="control">
                                        <input class="input weight calcManual inp_berat" type="number" placeholder="contoh: 103kg">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field4 none">
                        <div class="columns col-netral is-multiline">
                            <div class="column is-4">
                                <div class="field mt1">
                                    <label class="label has-text-left">Tinggi</label>
                                    <div class="control">
                                        <input class="input calc-weight inp_tinggi" id="height" type="number" placeholder="contoh: 120cm">
                                    </div>
                                </div>
                            </div>
                            <div class="column is-4">
                                <div class="field mt1">
                                    <label class="label has-text-left">Panjang</label>
                                    <div class="control">
                                        <input class="input calc-weight inp_lebar" id="long" type="number" placeholder="contoh: 20cm">
                                    </div>
                                </div>
                            </div>
                            <div class="column is-4">
                                <div class="field mt1">
                                    <label class="label has-text-left">Lingkar dada</label>
                                    <div class="control">
                                        <input class="input calc-weight inp_dada" id="chest" type="number" placeholder="contoh: 200cm">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field5">
                        <div class="columns col-netral is-multiline">
                            <div class="column is-6">
                                <div class="field mt1">
                                    <label class="label has-text-left">Jenis Pakan</label>
                                    <div class="control">
                                        <div class="select is-fullwidth">
                                            <select class="sel_pakan">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="field mt1">
                                    <label class="label has-text-left">Grade</label>
                                    <div class="control no-select">
                                        <input type="hidden" class="inp_grade">
                                        <input class="input display_grade" type="text" readonly placeholder="grade">
                                    </div>
                                </div>
                            </div>
                            <div class="column is-6">
                                <div class="field mt1">
                                    <label class="label has-text-left">Harga Kisaran</label>
                                    <div class="control no-select relative">
                                        <input class="input inp_hargaEstimasi" type="text" readonly placeholder="estimasi">
                                        <div class="include small is-text-blue none">harga sudah include asuransi</div>
                                    </div>

                                </div>
                                <div class="field mt1">
                                    <label class="label has-text-left">Harga</label>
                                    <div class="control no-select">
                                        <input class="input inp_harga" type="text" placeholder="harga">
                                    </div>
                                </div>
                            </div>
                            <div class="column is-12">
                                <div class="field mt1">
                                    <label class="label has-text-left mb-1h">Upload Foto Hewan</label>
                                    <div class="columns col-netral">
                                        <div class="column">
                                            <form method="post" action="" enctype="multipart/form-data">
                                                <input type="file" class="inputfile inputfile-2 file1" id="file1" data-preview=".pick1">
                                                <label for="file1" class="labelPic">
                                                    <div class="pic-ternak pointer pick1">
                                                        <div>
                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75 67.5" width="50">
                                                                <defs>
                                                                    <style>
                                                                        .pic-ternak {
                                                                            fill: #a7a7a7;
                                                                        }
                                                                    </style>
                                                                </defs>
                                                                <g id="camera" transform="translate(0 -25.5)">
                                                                    <g id="camera-alt" transform="translate(0 25.5)">
                                                                        <circle id="Ellipse_71" data-name="Ellipse 71" class="pic-ternak" cx="12" cy="12" r="12" transform="translate(25.5 25.5)"/>
                                                                        <path id="Path_31604" data-name="Path 31604" class="pic-ternak" d="M26.25,25.5,19.5,33H7.5A7.522,7.522,0,0,0,0,40.5v45A7.522,7.522,0,0,0,7.5,93h60A7.522,7.522,0,0,0,75,85.5v-45A7.522,7.522,0,0,0,67.5,33h-12l-6.75-7.5ZM37.5,81.75A18.75,18.75,0,1,1,56.25,63,18.568,18.568,0,0,1,37.5,81.75Z" transform="translate(0 -25.5)"/>
                                                                    </g>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </label>
                                            </form>
                                        </div>
                                        <div class="column">
                                            <form method="post" action="" enctype="multipart/form-data">
                                                <input type="file" class="inputfile inputfile-2 file1" id="file2" data-preview=".pick1">
                                                <label for="file2" class="labelPic">
                                                    <div class="pic-ternak pointer pick2">
                                                        <div>
                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75 67.5" width="50">
                                                                <defs>
                                                                    <style>
                                                                        .pic-ternak {
                                                                            fill: #a7a7a7;
                                                                        }
                                                                    </style>
                                                                </defs>
                                                                <g id="camera" transform="translate(0 -25.5)">
                                                                    <g id="camera-alt" transform="translate(0 25.5)">
                                                                        <circle id="Ellipse_71" data-name="Ellipse 71" class="pic-ternak" cx="12" cy="12" r="12" transform="translate(25.5 25.5)"/>
                                                                        <path id="Path_31604" data-name="Path 31604" class="pic-ternak" d="M26.25,25.5,19.5,33H7.5A7.522,7.522,0,0,0,0,40.5v45A7.522,7.522,0,0,0,7.5,93h60A7.522,7.522,0,0,0,75,85.5v-45A7.522,7.522,0,0,0,67.5,33h-12l-6.75-7.5ZM37.5,81.75A18.75,18.75,0,1,1,56.25,63,18.568,18.568,0,0,1,37.5,81.75Z" transform="translate(0 -25.5)"/>
                                                                    </g>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </label>
                                            </form>
                                        </div>
                                        <div class="column">
                                            <form method="post" action="" enctype="multipart/form-data">
                                                <input type="file" class="inputfile inputfile-2 file1" id="file3" data-preview=".pick1">
                                                <label for="file3" class="labelPic">
                                                    <div class="pic-ternak pointer pick3">
                                                        <div>
                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75 67.5" width="50">
                                                                <defs>
                                                                    <style>
                                                                        .pic-ternak {
                                                                            fill: #a7a7a7;
                                                                        }
                                                                    </style>
                                                                </defs>
                                                                <g id="camera" transform="translate(0 -25.5)">
                                                                    <g id="camera-alt" transform="translate(0 25.5)">
                                                                        <circle id="Ellipse_71" data-name="Ellipse 71" class="pic-ternak" cx="12" cy="12" r="12" transform="translate(25.5 25.5)"/>
                                                                        <path id="Path_31604" data-name="Path 31604" class="pic-ternak" d="M26.25,25.5,19.5,33H7.5A7.522,7.522,0,0,0,0,40.5v45A7.522,7.522,0,0,0,7.5,93h60A7.522,7.522,0,0,0,75,85.5v-45A7.522,7.522,0,0,0,67.5,33h-12l-6.75-7.5ZM37.5,81.75A18.75,18.75,0,1,1,56.25,63,18.568,18.568,0,0,1,37.5,81.75Z" transform="translate(0 -25.5)"/>
                                                                    </g>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </label>
                                            </form>
                                        </div>
                                        <div class="column">
                                            <form method="post" action="" enctype="multipart/form-data">
                                                <input type="file" class="inputfile inputfile-2 file1" id="file4" data-preview=".pick1">
                                                <label for="file4" class="labelPic">
                                                    <div class="pic-ternak pointer pick4">
                                                        <div>
                                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75 67.5" width="50">
                                                                <defs>
                                                                    <style>
                                                                        .pic-ternak {
                                                                            fill: #a7a7a7;
                                                                        }
                                                                    </style>
                                                                </defs>
                                                                <g id="camera" transform="translate(0 -25.5)">
                                                                    <g id="camera-alt" transform="translate(0 25.5)">
                                                                        <circle id="Ellipse_71" data-name="Ellipse 71" class="pic-ternak" cx="12" cy="12" r="12" transform="translate(25.5 25.5)"/>
                                                                        <path id="Path_31604" data-name="Path 31604" class="pic-ternak" d="M26.25,25.5,19.5,33H7.5A7.522,7.522,0,0,0,0,40.5v45A7.522,7.522,0,0,0,7.5,93h60A7.522,7.522,0,0,0,75,85.5v-45A7.522,7.522,0,0,0,67.5,33h-12l-6.75-7.5ZM37.5,81.75A18.75,18.75,0,1,1,56.25,63,18.568,18.568,0,0,1,37.5,81.75Z" transform="translate(0 -25.5)"/>
                                                                    </g>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </label>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="field mt2">
            <a class="button btn-akadQ w-250 submit_btn">Simpan</a>
        </div>
    </div>
@endsection

@section('script')
<script>
    const GlobalJS = new Global()

    $(".inp_umur").keyup(function(){
        setTimeout(function(){
            var ternak = $('.sel_hewan').val()
            var umur = $(".inp_umur").val()

            function showError(message){
                $('.req_age').text(message)
            }

            if(ternak === "SAPI") {
                if(umur >= 5 && umur <=24) {
                } else {
                    showError('dalam satuan bulan, min 5 bulan max 24 bulan')
                }
            }

            if(ternak === "KAMBING") {
                if(umur >= 5 && umur <=12) {
                } else {
                    showError('dalam satuan bulan, min 5 bulan max 12 bulan')
                }
            }

            if(ternak === "DOMBA") {
                if(umur >= 5 && umur <=8) {
                } else {
                    showError('dalam satuan bulan, min 5 bulan max 8 bulan')
                }
            }

        }, 250);
    })

    $('.sel_hewan').change(function(){
        if($(this).val() == "KAMBING") {
            $('#manual').attr('disabled', true)
            $('.req_age').text('')
        } else if ($(this).val() == "SAPI") {
            $('#manual').attr('disabled', false)
            $('.req_age').text('')
        } else {
            $('#manual').attr('disabled', true)
            $('.req_age').text('')
        }
    })

    $('.weight').keyup(function(){
        var weightdata = $('.weight').val();
        if(weightdata != ""){
            $('.btn-akadQ').removeClass('disabled')
        } else {
            $('.btn-akadQ').addClass('disabled')
        }
    });

    $('.calc-weight').keyup(function(){
        var height = $('#height').val();
        var long = $('#long').val();
        var chest = $('#chest').val();
        if(height != "" && long != "" && chest != "") {
            $('.btn-akadQ').removeClass('disabled')
        } else {
            $('.btn-akadQ').addClass('disabled')
        }
    });

    $('.inp_tinggi, .inp_lebar, .inp_dada').keyup(function(){
        var height = $('.inp_tinggi').val();
        var long = $('.inp_lebar').val();
        var chest = $('.inp_dada').val();
        if(height != "" && long != "" && chest != "") {
            var berat = Math.floor(((chest*chest)*long)/10815.15)
            $('.inp_berat').val(berat)
        } else {
            return false;
        }
    })

    var jenis_hewan = $("select.sel_hewan").val();
    $(".sel_hewan").change(function(){
        jenis_hewan = $(this).val()

        var methodRequest = "GET"
        var urlRequest = "{{ env("HOST_API") }}/api/animal/categories?commodityType="+ jenis_hewan +""
        var headerRequest = {
            "Accept-Version":"1.0",
            Authorization:"{{ Session::get('accessToken') }}"
        }
        var dataRequest = ""

        $(".sel_jenis").html("")
        GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
            for(var i=0;i < response.data.length;i++){
                $(".sel_jenis").append("<option value="+ response.data[i].uuid +">"+ response.data[i].name +"</option>");
            }
        })
    })

    function manual() {
        $('.field5').removeClass('none');
        $('.field4').removeClass('none');
        $('.input.weight').attr('readonly', true);
        $('.label-weight').text('Estimasi Berat')
    }
    function timbangan() {
        $('.field5').removeClass('none');
        $('.field4').addClass('none');
        $('.field3').removeClass('none');
        $('.label-weight').text('Berat Saat ini');
        $('.input.weight').attr('readonly', false);
    }

    /* get kandang */
    function getKandang(){
        var methodRequest = "GET"
        var urlRequest = "{{ env("HOST_API") }}/api/ranch/me?start=0"
        var headerRequest = {
            "Accept-Version":"1.0",
            Authorization:"{{ Session::get('accessToken') }}"
        }
        var dataRequest = ""

        $(".sel_kandang").html("")
        GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
            for(var i=0;i < response.data.length;i++){
                $(".sel_kandang").append("<option value="+ response.data[i].uuid +">"+ response.data[i].name +"</option>");
            }
        })
    }

    /* get grade */
    function addCommas(nStr){
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
    $("input.inp_berat").keyup(function(){
        var takeweight = $(this).val()
        setTimeout(function(){
            var methodRequest = "GET"
            var urlRequest = "{{ env("HOST_API") }}/api/grade/findByWeight?weight="+takeweight+""
            var headerRequest = {
                "Accept-Version":"1.0",
                Authorization:"{{ Session::get('accessToken') }}"
            }
            var dataRequest = ""

            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
                if(response.data.length > 0){
                    $("input.inp_tinggi").val(response.data[0].highestHeight)
                    $("input.inp_lebar").val(response.data[0].highestWidth)
                    $("input.inp_dada").val(response.data[0].highestWeight)
                    $("input.display_grade").val(response.data[0].name)
                    $("input.inp_grade").val(response.data[0].uuid)
                    $("input.inp_hargaEstimasi").val("Rp. " + addCommas(response.data[0].lowestPrice) + " - " + "Rp. " + addCommas(response.data[0].highestPrice))
                    $('.include').removeClass('none')
                }
            })
        }, 250)
    })


    /* get pakan */
    function getPakan(){
        var methodRequest = "GET"
        var urlRequest = "{{ env("HOST_API") }}/api/animal/foodTypes"
        var headerRequest = {
            "Accept-Version":"1.0",
            Authorization:"{{ Session::get('accessToken') }}"
        }
        var dataRequest = ""

        $(".sel_pakan").html("")
        GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
            for(var i=0;i < response.data.length;i++){
                $(".sel_pakan").append("<option value="+ response.data[i].uuid +">"+ response.data[i].name +"</option>");
            }
        })
    }

    function getComodity(){
        var methodRequest = "GET"
        var urlRequest = "{{ env("HOST_API") }}/api/animal/categories?commodityType=SAPI"
        var headerRequest = {
            "Accept-Version":"1.0",
            Authorization:"{{ Session::get('accessToken') }}"
        }
        var dataRequest = ""

        $(".sel_jenis").html("")
        GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
            for(var i=0;i < response.data.length;i++){
                $(".sel_jenis").append("<option value="+ response.data[i].uuid +">"+ response.data[i].name +"</option>");
            }
        })
    }

    $(".submit_btn").click(function(){
        if($('.inp_name').val() === ""){
            alert('ada data yang belum diisi')
            return false
        } else {
            if(files1 == undefined && files2 == undefined && files3 == undefined && files4 == undefined){
                alert('upload minimal 1 gambar')
                return false
            }

            var fd = new FormData();

            fd.append('image_1',files1);
            fd.append('image_2',files2);
            fd.append('image_3',files3);
            fd.append('image_4',files4);

            var name = $("input.inp_name").val();
            var age = $("input.inp_umur").val();
            var width = $("input.inp_lebar").val();
            var height = $("input.inp_tinggi").val();
            var weight = $("input.inp_berat").val();
            var price = $("input.inp_harga").val();
            var chestSize = $("input.inp_dada").val();
            var commodityType = $("select.sel_hewan").val();
            var image_1= JSON.stringify(fd);
            var ranchUuid = $("select.sel_kandang").val();
            var categoryUuid = $("select.sel_jenis").val();
            var foodTypeUuid = $("select.sel_pakan").val();
            var transactionType = $("input.sel_trans").val();
            var gradeUuid = $("input.inp_grade").val();
            var calculationMethod = $("input.method_weight:checked").val();
            var uuid = localStorage.getItem("uuid_animal_edit")

            fd.append("name",name)
            fd.append("age",age)
            fd.append("width",width)
            fd.append("height",height)
            fd.append("weight",weight)
            fd.append("price",price)
            fd.append("chestSize",chestSize)
            fd.append("commodityType",commodityType)
            fd.append("ranchUuid",ranchUuid)
            fd.append("categoryUuid",categoryUuid)
            fd.append("foodTypeUuid",foodTypeUuid)
            fd.append("transactionType",transactionType)
            fd.append("gradeUuid",gradeUuid)
            fd.append("calculationMethod",calculationMethod)
            fd.append("uuid", uuid)

            $.ajax({
                type: "POST",
                url: "{{ env("HOST_API") }}/api/animal/",
                data:fd,
                headers:{
                    Authorization:localStorage.aq_acc_token
                },
                dataType:"json",
                contentType: false,
                processData: false,
                success:function( response ) {
                    if(response.success){
                        window.location="{{ url('dashboard/mitra/hewan-dijual') }}";
                    }
                    else {
                        alert(response.data.message)
                    }
                },
                error:function(er){
                    // alert(er.responseJSON.error_description);
                }
            })
        }
    })

    var files;

    $('#file1').change(function(){
        setImage(this, 1)
    })
    $('#file2').change(function(){
        setImage(this, 2)
    })
    $('#file3').change(function(){
        setImage(this, 3)
    })
    $('#file4').change(function(){
        setImage(this, 4)
    })

    var files1;
    var files2;
    var files3;
    var files4;

    function setImage(self, param){
        if (self.files && self.files[0]) {

            var FR= new FileReader();

            FR.addEventListener("load", function(e) {

                console.log(e.target.result)

                $(".img_prev").attr("src",e.target.result)

                $('.pick'+param+' svg').addClass('none');
                $('.pick'+param).css(
                    {
                        'background-image': 'url('+e.target.result+')',
                        'background-size': 'contain',
                        'background-repeat': 'no-repeat',
                        'background-position': 'center'
                    });

                // console.log(e.target.result)
                if(param == 1){
                    files1 = e.target.result;
                }
                if(param == 2){
                    files2 = e.target.result;
                }
                if(param == 3){
                    files3 = e.target.result;
                }
                if(param == 4){
                    files4 = e.target.result;
                }
            });

            FR.readAsDataURL( self.files[0] );
        }
    }

    $(document).ready(function(){
        getKandang()
        getPakan()
        getComodity()
        getDetailAnimal('{{ $animal_uuid }}')
    })

    function getDetailAnimal(uuid){
        var methodRequest = "GET"
        var urlRequest = "{{ env("HOST_API") }}/api/animal/"+uuid
        var headerRequest = {
            "Accept-Version":"1.0",
            Authorization:localStorage.aq_acc_token
        }
        var dataRequest = ""
        $(".sel_jenis").html("")
        GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
            $("input.inp_umur").val(25)
            $("input.display_grade").val(response.data.grade.name);
            $("input.inp_grade").val(response.data.grade.uuid);
            $(".inp_berat").val(response.data.progress[0].weight)
            $("select.sel_kandang").val(response.data.ranch.uuid)
            $("input.inp_tinggi").val(response.data.progress[0].height)
            $("input.inp_lebar").val(response.data.progress[0].width)
            $("input.inp_dada").val(response.data.progress[0].chestSize)
            getHargaKisaranManual(response.data.progress[0].weight)
            $(".inp_harga").val(response.data.progress[0].price)
            $("select.sel_kandang").val(response.data.ranch.uuid)
            $("select.sel_jenis").val(response.data.categoryUuid)
            $('.inp_name').val(response.data.name)
            if(response.data.catalogue.transactionType == 'Tabungan'){
                $('input.sel_trans').val("SAVING")
                $('#tabungan').attr('checked', 'checked')
            }
            if(response.data.catalogue.transactionType == 'Tunai'){
                $('input.sel_trans').val("CASH")
                $('#tunai').attr('checked', 'checked')
            }
            $("select.sel_pakan").val(response.data.foodTypeUuid)
        })
    }

    function getHargaKisaranManual(takeweight) {
        $('.inp_berat').val(takeweight)

        var methodRequest = "GET"
        var urlRequest = "{{ env("HOST_API") }}/api/grade/findByWeight?weight=" + takeweight + ""
        var headerRequest = {
            "Accept-Version": "1.0",
            Authorization: localStorage.aq_acc_token
        }
        var dataRequest = ""

        GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(response => {
            if (response.data.length == 0) {
                $("input.display_grade").val('')
                $("input.inp_grade").val('')
                $("input.inp_hargaEstimasi").val("Rp. 0 - Rp. 0")
                $('.include').removeClass('none')
            } else {
                $("input.display_grade").val(response.data[0].name)
                $("input.inp_grade").val(response.data[0].uuid)
                $("input.inp_hargaEstimasi").val("Rp. " + addCommas(response.data[0].lowestPrice) + " - " + "Rp. " + addCommas(response.data[0].highestPrice))
                $('.include').removeClass('none')
            }
        })
    }
</script>
@endsection