@extends('dashboard.mitra.layout.index')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/swiper.css') }}">
@endsection

@section('content')
    <div class="modal modal-left detail-informasi update">
        <div class="modal-background"></div>
        <div class="modal-card">

            <section class="modal-card-body">
                <button class="delete del-informasi" aria-label="close"></button>
                <ul class="tabbing">
                    <li class="tab-link current" data-tab="tab-1">Bulan 1</li>
                    <li class="tab-link" data-tab="tab-2">Bulan 2</li>
                    <li class="tab-link" data-tab="tab-3">Bulan 3</li>
                </ul>

                <div id="tab-1" class="tab-content current">
                    <div class="columns col-netral">
                        <div class="column is-5">
                            <div class="swiper-container update swiper3">
                                <!-- banner slider -->
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide"><img src="{{ asset('assets/img/sapi-1-min.jpg') }}"></div>
                                    <div class="swiper-slide"><img src="{{ asset('assets/img/sapi-2-min.jpg') }}"></div>
                                    <div class="swiper-slide"><img src="{{ asset('assets/img/sapi-3-min.jpg') }}"></div>
                                </div>

                            </div>
                            <!-- pagination -->
                            <div class="swiper-pagination pag3"></div>
                            <!-- button -->
                            <a class="button is-fullwidth btn-akadQ mt1">Perbarui Kondisi Hewan</a>
                        </div>
                        <div class="column">
                            <div class="content desc-ternak">
                                <div class="fw500">Sapi Bali Super</div>
                                <div class="table-responsive">
                                    <table class="table mt1 is-fullwidth is-striped">
                                        <tbody>
                                        <tr>
                                            <td>Umur</td>
                                            <td>: &nbsp;5 Bulan</td>
                                        </tr>
                                        <tr>
                                            <td>Panjang</td>
                                            <td>: &nbsp;150cm</td>
                                        </tr>
                                        <tr>
                                            <td>Tinggi</td>
                                            <td>: &nbsp;110cm</td>
                                        </tr>
                                        <tr>
                                            <td>Lingkar Dada</td>
                                            <td>: &nbsp;260cm</td>
                                        </tr>
                                        <tr>
                                            <td>Berat</td>
                                            <td>: &nbsp;90kg</td>
                                        </tr>
                                        <tr>
                                            <td>Tenor</td>
                                            <td>: &nbsp;1/6</td>
                                        </tr>
                                        <tr>
                                            <td>Pembayaran</td>
                                            <td>: &nbsp;Rp 3.000.000</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="level">
                        <div class="level-left">
                            <div class="level-item">
                                <div class="last-update">
                                    Update gambar : 5 Juni 2018
                                </div>
                            </div>
                        </div>
                        <div class="level-right">
                            <div class="level-item">
                                <div class="last-update">
                                    Update konten : 5 Juni 2018
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab-2" class="tab-content">
                    <div class="content has-text-centered">
                        <div class="title is-size-5">
                            Update Hewan Tabungan
                        </div>
                        <p class="subtitle is-size-6 fw300">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <img src="{{ asset('assets/img/icon-hewan.png') }}" width="150">
                        <!-- button -->
                        <div class="field mt1">
                            <a class="button btn-akadQ w-250 mt1">Update Hewan Sekarang</a>
                        </div>
                    </div>
                </div>
                <div id="tab-3" class="tab-content">
                    <div class="content has-text-centered">
                        <div class="title is-size-5">
                            Update Hewan Tabungan
                        </div>
                        <p class="subtitle is-size-6 fw300">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <img src="{{ asset('assets/img/icon-hewan.png') }}" width="150">
                        <!-- button -->
                        <div class="field mt1">
                            <a class="button btn-akadQ w-250 mt1">Update Hewan Sekarang</a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="modal modal-left detail-tenor">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title is-size-6 fw600">Rincian Tenor</p>
                <button class="delete del-reminder" id="setujuDel" aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                <table class="table intro-tenor is-fullwidth">
                    <tr>
                        <td>
                            Pembayaran Tanggal
                        </td>
                        <td>
                            : &nbsp; 26 setiap bulan
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Status Pembayaran
                        </td>
                        <td>
                            : &nbsp; 3/6
                        </td>
                    </tr>
                </table>
                <table class="table informasi-tenor is-fullwidth">
                    <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Jumlah</th>
                        <th>Tenor</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>05/06/18</td>
                        <td>Rp 5.000.000</td>
                        <td>1 (Lunas)</td>
                    </tr>
                    <tr>
                        <td>05/07/18</td>
                        <td>Rp 5.000.000</td>
                        <td>2 (Lunas)</td>
                    </tr>
                    <tr>
                        <td>05/08/18</td>
                        <td>Rp 5.000.000</td>
                        <td>3 (Lunas)</td>
                    </tr>
                    <tr>
                        <td>05/09/18</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>05/10/18</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>05/11/18</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td class="is-text-blue fw500">Total</td>
                        <td class="is-text-blue fw500" colspan="2">Rp 20.000.000</td>
                    </tr>
                    </tbody>
                </table>
            </section>
        </div>
    </div>

    <div class="content has-text-left">
        <div class="title">
            Update Hewan Tabungan
        </div>
        <div class="columns col-netral">
            <div class="column is-7">
                <p class="subtitle is-size-6 fw300">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </div>
    </div>
    <div class="columns col-netral is-multiline">
        <div class="column is-6">
            <div class="box-transaksi shadow-box">
                <div class="info-transaksi">
                    <div class="level">
                        <div class="level-left">
                            <div class="level-item tgl-transaksi">
                                05 Juni 2018
                            </div>
                        </div>
                        <div class="level-right">
                            <div class="level-item invoice">
                                INV/20180605/XVIII/VI/168854789
                            </div>
                        </div>
                    </div>
                </div>
                <div class="columns col-netral align-items detail-transaksi mb0 pb0 is-multiline">
                    <div class="column is-7 is-12-mobile">
                        <div class="columns col-netral align-items">
                            <div class="column">
                                <div class="img-transaksi">
                                    <img src="{{ asset('assets/img/sapi-1-min.jpg') }}">
                                </div>
                            </div>
                            <div class="column">
                                <div class="content">
                                    <div class="fw500">Sapi Bali Super</div>
                                    <div class="small">UD Baharudin MS</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="content">
                            <div class="fw500">Menabung</div>
                            <div class="small">1/6 X Rp 5.000.000</div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="more-detail">
                    <div class="columns col-netral">
                        <div class="column">
                            <a class="button is-fullwidth btn-akadQ informasiClick">Informasi</a>
                        </div>
                        <div class="column">
                            <a class="button is-fullwidth btn-akadQ-grey tenorClick">Rincian Tenor</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column is-6">
            <div class="box-transaksi shadow-box">
                <div class="info-transaksi">
                    <div class="level">
                        <div class="level-left">
                            <div class="level-item tgl-transaksi">
                                05 Juni 2018
                            </div>
                        </div>
                        <div class="level-right">
                            <div class="level-item invoice">
                                INV/20180605/XVIII/VI/168854789
                            </div>
                        </div>
                    </div>
                </div>
                <div class="columns col-netral align-items detail-transaksi mb0 pb0 is-multiline">
                    <div class="column is-7 is-12-mobile">
                        <div class="columns col-netral align-items">
                            <div class="column">
                                <div class="img-transaksi">
                                    <img src="{{ asset('assets/img/sapi-1-min.jpg') }}">
                                </div>
                            </div>
                            <div class="column">
                                <div class="content">
                                    <div class="fw500">Sapi Bali Super</div>
                                    <div class="small">UD Baharudin MS</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="content">
                            <div class="fw500">Menabung</div>
                            <div class="small">1/6 X Rp 5.000.000</div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="more-detail">
                    <div class="columns col-netral">
                        <div class="column">
                            <a class="button is-fullwidth btn-akadQ informasiClick">Informasi</a>
                        </div>
                        <div class="column">
                            <a class="button is-fullwidth btn-akadQ-grey tenorClick">Rincian Tenor</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column is-6">
            <div class="box-transaksi shadow-box">
                <div class="info-transaksi">
                    <div class="level">
                        <div class="level-left">
                            <div class="level-item tgl-transaksi">
                                05 Juni 2018
                            </div>
                        </div>
                        <div class="level-right">
                            <div class="level-item invoice">
                                INV/20180605/XVIII/VI/168854789
                            </div>
                        </div>
                    </div>
                </div>
                <div class="columns col-netral align-items detail-transaksi mb0 pb0 is-multiline">
                    <div class="column is-7 is-12-mobile">
                        <div class="columns col-netral align-items">
                            <div class="column">
                                <div class="img-transaksi">
                                    <img src="{{ asset('assets/img/sapi-1-min.jpg') }}">
                                </div>
                            </div>
                            <div class="column">
                                <div class="content">
                                    <div class="fw500">Sapi Bali Super</div>
                                    <div class="small">UD Baharudin MS</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="content">
                            <div class="fw500">Menabung</div>
                            <div class="small">1/6 X Rp 5.000.000</div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="more-detail">
                    <div class="columns col-netral">
                        <div class="column">
                            <a class="button is-fullwidth btn-akadQ informasiClick">Informasi</a>
                        </div>
                        <div class="column">
                            <a class="button is-fullwidth btn-akadQ-grey tenorClick">Rincian Tenor</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column is-6">
            <div class="box-transaksi shadow-box">
                <div class="info-transaksi">
                    <div class="level">
                        <div class="level-left">
                            <div class="level-item tgl-transaksi">
                                05 Juni 2018
                            </div>
                        </div>
                        <div class="level-right">
                            <div class="level-item invoice">
                                INV/20180605/XVIII/VI/168854789
                            </div>
                        </div>
                    </div>
                </div>
                <div class="columns col-netral align-items detail-transaksi mb0 pb0 is-multiline">
                    <div class="column is-7 is-12-mobile">
                        <div class="columns col-netral align-items">
                            <div class="column">
                                <div class="img-transaksi">
                                    <img src="{{ asset('assets/img/sapi-1-min.jpg') }}">
                                </div>
                            </div>
                            <div class="column">
                                <div class="content">
                                    <div class="fw500">Sapi Bali Super</div>
                                    <div class="small">UD Baharudin MS</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="content">
                            <div class="fw500">Menabung</div>
                            <div class="small">1/6 X Rp 5.000.000</div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="more-detail">
                    <div class="columns col-netral">
                        <div class="column">
                            <a class="button is-fullwidth btn-akadQ informasiClick">Informasi</a>
                        </div>
                        <div class="column">
                            <a class="button is-fullwidth btn-akadQ-grey tenorClick">Rincian Tenor</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/swiper.min.js') }}"></script>
    <script>
        $('ul.tabbing li').click(function(){
            var tab_id = $(this).attr('data-tab');

            $('ul.tabbing li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $("#"+tab_id).addClass('current');
        })

        $('.informasiClick').click(function(){
            $('.detail-informasi').addClass('is-active')
            $('html').addClass('hide-overflow')
            var swiper3 = new Swiper('.swiper3', {
                slidesPerView: 1,
                loop: true,
                autoplay: true,
                pagination: {
                    el: '.swiper-pagination.pag3',
                    clickable: true,
                },
            });
        });

        $('.tenorClick').click(function(){
            $('.detail-tenor').addClass('is-active')
        })

        $('.modal .delete').click(function(){
            $(this).parents('.modal').removeClass('is-active')
            $('html').removeClass('hide-overflow')
        })
    </script>
@endsection