@extends('dashboard.mitra.layout.index')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/swiper.css') }}">
@endsection

@section('content')
    <div class="modal modal-left detail-reminder" id="tolakBox">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title is-size-6 fw600">Konfirmasi Persetujuan</p>
                <button class="delete del-reminder" id="tolakDel" aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                <div class="content desc-ternak">
                    <div class="columns col-netral">
                        <div class="column is-narrow">
                            <div class="picprofile">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="100" height="100" viewBox="0 0 24 24"><path d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2C6.47,2 2,6.47 2,12C2,17.53 6.47,22 12,22C17.53,22 22,17.53 22,12C22,6.47 17.53,2 12,2M14.59,8L12,10.59L9.41,8L8,9.41L10.59,12L8,14.59L9.41,16L12,13.41L14.59,16L16,14.59L13.41,12L16,9.41L14.59,8Z" fill="red"/></svg>
                            </div>
                        </div>
                        <div class="column align-items">
                            <div class="content desc-ternak">
                                <div class="farm-name fw500">Pesanan Dibatalkan</div>
                                <div class="small">Anda telah membatalkan pesanan ini. Sistem akan otomatis mengirim notifikasi ke Akun Pembeli</div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="modal modal-left detail-pembeli">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title is-size-6 fw600">Detail Pembeli</p>
                <button class="delete del-detail" aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                <div class="content desc-ternak">
                    <div class="columns col-netral">
                        <div class="column is-narrow">
                            <div class="picprofile">
                                <img src="{{ asset('assets/img/profile.jpg') }}">
                            </div>
                        </div>
                        <div class="column align-items">
                            <div class="content desc-ternak">
                                <div class="farm-name fw500">Ajang Salim</div>
                                <div class="small">Jalan Kemandoran 3 RT 11 RW 03 No.44, Kel: Grogol Utara, Kec: Kebayoran Lama, 12210</div>
                                <a class="button btn-akadQ mt1">Point Map</a>
                            </div>
                        </div>
                    </div>
                    <table class="table mt1 is-fullwidth is-striped">
                        <tbody>
                        <tr>
                            <td>Komoditas</td>
                            <td class="fw500">: &nbsp;Sapi Limosin</td>
                        </tr>
                        <tr>
                            <td>Transaksi</td>
                            <td class="fw500">: &nbsp;12/05/18</td>
                        </tr>
                        <tr>
                            <td>Jenis Transaksi</td>
                            <td class="fw500">: &nbsp;Tabungan - 1/6</td>
                        </tr>
                        <tr>
                            <td>Jatuh Tempo</td>
                            <td class="fw500">: &nbsp;25/05/18</td>
                        </tr>
                        <tr>
                            <td>Harga</td>
                            <td class="fw500">: &nbsp;Rp 20.000.000</td>
                        </tr>
                        <tr>
                            <td>Keperluan</td>
                            <td class="fw500">: &nbsp;Pribadi</td>
                        </tr>
                        <tr>
                            <td>No Handphone / Telpon</td>
                            <td class="fw500">: &nbsp;081281532099</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>

    <div class="content has-text-left">
        <div class="title">
            Menunggu Persetujuan
        </div>
        <div class="columns col-netral ">
            <div class="column is-7">
                <p class="subtitle is-size-6 fw300">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </div>
    </div>
    <!-- table -->
    <div class="table-responsive">
        <table class="table table-summary is-striped is-fullwidth">
            <thead>
            <tr>
                <th>Detail</th>
                <th>Pembeli</th>
                <th>Komoditas</th>
                <th>Transaksi</th>
                <th>Tenor</th>
                <th>Jatuh Tempo</th>
                <th>Harga</th>
                <th>Persetujuan</th>
            </tr>
            </thead>
            <tbody>
            <tr id="1">
                <td>
                    <div class="find-detail is-pulled-left">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>
                    </div>
                </td>
                <td>Ajang Salim</td>
                <td>Sapi Limosin</td>
                <td>12/05/18</td>
                <td>1/6</td>
                <td>25/05/18</td>
                <td>Rp 20.000.000</td>
                <td class="approval">
                    <a href="#" class="button btn-akadQ w-65 delivery" data-id="1">Setuju</a>
                    <a href="#" class="button btn-akadQ invert w-65 tolak">Tidak</a>
                </td>
            </tr>
            <tr id="2">
                <td>
                    <div class="find-detail is-pulled-left">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>
                    </div>
                </td>
                <td>Sarifudin</td>
                <td>Kambing Kacang</td>
                <td>13/05/18</td>
                <td>Tunai</td>
                <td>-</td>
                <td>Rp 3.000.000</td>
                <td class="approval">
                    <a href="#" class="button btn-akadQ w-65 delivery" data-id="2">Setuju</a>
                    <a href="#" class="button btn-akadQ invert w-65 tolak">Tidak</a>
                </td>
            </tr>
            <tr id="3">
                <td>
                    <div class="find-detail is-pulled-left">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>
                    </div>
                </td>
                <td>Saidah</td>
                <td>Kambing Jantan</td>
                <td>20/05/18</td>
                <td>Tunai</td>
                <td>-</td>
                <td>Rp 2.000.000</td>
                <td class="approval">
                    <a href="#" class="button btn-akadQ w-65 delivery" data-id="3">Setuju</a>
                    <a href="#" class="button btn-akadQ invert w-65 tolak">Tidak</a>
                </td>
            </tr>
            <tr id="4">
                <td>
                    <div class="find-detail is-pulled-left">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>
                    </div>
                </td>
                <td>Muhammad Saleh</td>
                <td>Sapi Jawa</td>
                <td>12/05/18</td>
                <td>1/3</td>
                <td>25/05/18</td>
                <td>Rp 40.000.000</td>
                <td class="approval">
                    <a href="#" class="button btn-akadQ w-65 delivery" data-id="4">Setuju</a>
                    <a href="#" class="button btn-akadQ invert w-65 tolak">Tidak</a>
                </td>
            </tr>
            <tr id="5">
                <td>
                    <div class="find-detail is-pulled-left">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>
                    </div>
                </td>
                <td>Ahmad Fatah</td>
                <td>Sapi Aceh</td>
                <td>11/05/18</td>
                <td>1/12</td>
                <td>25/05/18</td>
                <td>Rp 15.000.000</td>
                <td class="approval">
                    <a href="#" class="button btn-akadQ w-65 delivery" data-id="5">Setuju</a>
                    <a href="#" class="button btn-akadQ invert w-65 tolak">Tidak</a>
                </td>
            </tr>
            <tr id="6">
                <td>
                    <div class="find-detail is-pulled-left">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>
                    </div>
                </td>
                <td>Sapri</td>
                <td>Domba Garut</td>
                <td>12/05/18</td>
                <td>Tunai</td>
                <td>-</td>
                <td>Rp 1.500.000</td>
                <td class="approval">
                    <a href="#" class="button btn-akadQ w-65 delivery" data-id="6">Setuju</a>
                    <a href="#" class="button btn-akadQ invert w-65 tolak">Tidak</a>
                </td>
            </tr>

            </tbody>
        </table>
    </div>

@endsection

@section('script')
    <script src="{{ asset('assets/js/swiper.min.js') }}"></script>
    <script>
        $('.delete').click(function(){
            $(this).parents('.modal').removeClass('is-active');
            scroll()
        });

        $('.find-detail').on('click', function(){
            $('.detail-pembeli').addClass('is-active')
            noscroll()
        });
        $('.tolak').click(function(){
            $('#tolakBox').addClass('is-active')
            noscroll()
        });
        $('.delivery').click(function(){
            var produkId = $(this).data('id')
            $('#' + produkId).remove();
        });
    </script>
@endsection