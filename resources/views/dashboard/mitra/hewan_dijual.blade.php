@extends('dashboard.mitra.layout.index')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/swiper.css') }}">
@endsection

@section('content')
    <div class="content has-text-centered">
        <div class="title">
            Hewan Dijual
        </div>
        <p class="subtitle is-size-6 fw300">Berikut daftar hewan yang sudah masuk ke list penjualan AkadBaiQ
            <strong class="animal_total">(0 Hewan)</strong>
            <input type="hidden" id="totalHewan"/>
        </p>
    </div>
    <div class="field">
        <label class="label"></label>
        <div class="control has-text-centered">
            <div class="select">
                <select class="w-250">
                    <option value="all">Semua Hewan</option>
                    <option value="sapi">Sapi</option>
                    <option value="kambing">Kambing</option>
                    <option value="domba">Domba</option>
                </select>
            </div>
        </div>
    </div>
    <div class="columns col-netral is-multiline mt2 animal_list">

    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/swiper.min.js') }}"></script>
    <script>
        const GlobalJS = new Global()

        $('.detail-hewan').find(".swiper-wrapper").html("");
        $('.detail-hewan').find(".swiper-pagination").html("")

        function findDetail() {
            $('.detail-hewan').addClass('is-active');
            $('.detail-hewan').find(".swiper-wrapper").html("");
            $('.detail-hewan').find(".swiper-pagination").html("")
            //swiper3.update();
            var this_uuid = $(this).data("uuid");

            var methodRequest = "GET"
            var urlRequest = "http://103.253.107.139:8080/api/animal/" + this_uuid + ""
            var headerRequest = {
                "Accept-Version": "1.0",
                Authorization: "{{ Session::get('accessToken') }}"
            }
            var dataRequest = ""

            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(result => {
                if(result.success){
                    $('#detailNameAnimal').html(result.data.name)
                    $('#detailTinggiAnimal').html(": &nbsp;"+result.data.progress[0].height+"cm")
                    $('#detailBeratAnimal').html(": &nbsp;"+result.data.progress[0].weight+"kg")
                    $('#detailLingkarDadaAnimal').html(": &nbsp;"+result.data.progress[0].chestSize+"cm")

                    var last_progress = result.data.progress.length - 1;
                    for (var x = 0; x < result.data.progress[last_progress].images.length; x++) {
                        $('.detail-hewan').find(".swiper-wrapper").append('<div class="swiper-slide">' +
                            '<img src="' + result.data.progress[last_progress].images[x].url + '">' +
                            '</div>')
                    }

                    var swiper3 = new Swiper('.swiper3', {
                        slidesPerView: 1,
                        loop: true,
                        autoplay: true,
                        pagination: {
                            el: '.swiper-pagination.pag3',
                            clickable: true,
                        },
                    });
                }
            })
        }

        // sidebar
        function noscroll() {
            $('html').addClass('hide-overflow')
        }

        function scroll() {
            $('html').removeClass('hide-overflow')
        }

        $('.burger').click(function () {
            $('.side-bar').addClass('is-active')
            noscroll()
        })

        $(".modal-background, .close-modal").click(function () {
            $(this).parents('.modal').removeClass('is-active')
            scroll()
        })

        $('.del-detail').click(function () {
            $('.detail-hewan').removeClass('is-active');
        })

        $('.del-reason').click(function () {
            $('.box-reason').removeClass('is-active');
        })



        function showMenu() {
            if ($('.sub-menus').hasClass('none')) {
                $('.sub-menus').addClass('block').removeClass('none');
                $('.menu-add-hewan svg').css('transform', 'rotate(90deg)')
            } else {
                $('.sub-menus').addClass('none').removeClass('block')
                $('.menu-add-hewan svg').css('transform', 'rotate(0deg)')
            }
        }

        function addCommas(nStr) {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            return x1 + x2;
        }

        function get_animals() {
            $.ajax({
                url: "http://103.253.107.139:8080/api/animal/me",
                type: "GET",
                headers: {
                    "Accept-Version": "1.0",
                    Authorization: "{{ Session::get('accessToken') }}"
                },
                beforeSend: function () {
                    $(".animal_list").html("")
                },
                success: function (response) {
                    var animal_length = response.data.length;
                    $("#totalHewan").val(animal_length)
                    $(".animal_total").html("(" + $("#totalHewan").val() + " Hewan)")
                    for (var i = 0; i < animal_length; i++) {
                        $(".animal_list").append('<div class="column is-4 animal_view" id="boxWrapper' + i + '" data-uuid="' + response.data[i].uuid + '">' +
                            '<input type="hidden" id="uuid_animal'+i+'" name="uuid_animal" value="' + response.data[i].uuid + '">' +
                            '<div class="box-hewan relative">' +
                            '<div class="img-hewan">' +
                            '<img src="{{ asset('assets/img/sapi.jpg') }}" onerror="this.src=`{{ asset('assets/img/sapi.jpg') }}`">' +
                            '</div>' +
                            '<div class="columns col-netral mb0">' +
                            '<div class="column">' +
                            '<div class="content desc-ternak">' +
                            '<div class="fw500">' + response.data[i].name + '</div>' +
                            '<div class="small animal_type">Depok, Jawa Barat</div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="column is-3">' +
                            '<div class="find-detail" onclick="findDetail()" data-uuid="' + response.data[i].uuid + '">' +
                            '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"/></svg>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="transaksi">' +
                            'Transaksi :<span class="jenis-transaksi is-text-blue">Tunai</span>' +
                            '</div>' +
                            '<div class="box-price mt0h">' +
                            '<strike class="normal-price">Rp 45.000.000</strike>' +
                            '<span class="disc-price animal_price">Rp 35.000.000</span>' +
                            '</div>' +
                            '<div class="field has-addons for-button two mt1">' +
                            '  <p class="control">' +
                            '    <a class="button is-fullwidth edit-farm" onclick="editFarm('+i+')" data-uuid'+i+'="' + response.data[i].uuid + '">' +
                            '      <span class="icon mr0h">' +
                            '       <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="M5,3C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V12H19V19H5V5H12V3H5M17.78,4C17.61,4 17.43,4.07 17.3,4.2L16.08,5.41L18.58,7.91L19.8,6.7C20.06,6.44 20.06,6 19.8,5.75L18.25,4.2C18.12,4.07 17.95,4 17.78,4M15.37,6.12L8,13.5V16H10.5L17.87,8.62L15.37,6.12Z" /></svg>' +
                            '      </span>' +
                            '      <span>Edit</span>' +
                            '    </a>' +
                            '  </p>' +
                            '  <p class="control">' +
                            '    <a class="button btn-danger is-fullwidth del-farm" onclick="tester('+i+')" id="1">' +
                            '      <span class="icon">' +
                            '       <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="M21.03,3L18,20.31C17.83,21.27 17,22 16,22H8C7,22 6.17,21.27 6,20.31L2.97,3H21.03M5.36,5L8,20H16L18.64,5H5.36M9,18V14H13V18H9M13,13.18L9.82,10L13,6.82L16.18,10L13,13.18Z" /></svg>' +
                            '      </span>' +
                            '      <span>Hapus</span>' +
                            '    </a>' +
                            '  </p>' +
                            '</div>' +
                            '</div>' +
                            '</div>');

                        $('.edit-farm').click(function () {
                            // window.location.href = 'mitra-edit-hewan.html';
                        })

                    }


                },
                error: function (er) {
                    //alert("Periksa Jaringan");
                    //location.reload();
                },
                complete: function () {
                    $(".find-detail").click(findDetail);
                    $(".animal_view").each(function (index, element) {
                        var this_uuid = $(this).data("uuid");
                        var this_el = $(this);
                        /* ANIMAL DATA GET */
                        $.ajax({
                            url: "http://103.253.107.139:8080/api/animal/" + this_uuid + "",
                            type: "GET",
                            headers: {
                                "Accept-Version": "1.0",
                                Authorization: "{{ Session::get('accessToken') }}"
                            },
                            beforeSend: function () {
                                //$(".animal_list").html("")
                            },
                            success: function (result) {
                                var last_progress = result.data.progress.length - 1;
                                if (result.data.progress[last_progress].images.length != 0) {
                                    this_el.find("img").attr("src", result.data.progress[last_progress].images[0].url)
                                    this_el.find(".animal_type").html(result.data.progress[last_progress].category);
                                    this_el.find(".animal_price").html("Rp. " + addCommas(result.data.progress[last_progress].price) + "");
                                } else {
                                    this_el.remove();
                                }


                            }
                        })
                    })
                }
            })
        }

        function tester(id) {
            var totalHewan = $("#totalHewan").val()
            var uuid = $('#uuid_animal'+id).val()
            $('.box-reason').addClass('is-active');
            $('.detail-farm').removeClass('is-active');
            $('.reason li').click(function () {
                $('.box-reason').removeClass('is-active')
                $('#boxWrapper' + id).remove()
                $("#totalHewan").val(totalHewan - 1)
                $(".animal_total").html("(" + $("#totalHewan").val() + " Hewan)")
                deleteAnimal(uuid)
                // id++;
            })
        }

        function editFarm(id){
            window.location.href = 'edit-hewan/'+$('#uuid_animal'+id).val();
        }

        function deleteAnimal(uuid) {
            var methodRequest = "POST"
            var urlRequest = "http://103.253.107.139:8080/api/animal/" + uuid + "/delete"
            var headerRequest = {
                "Accept-Version": "1.0",
                Authorization: "{{ Session::get('accessToken') }}"
            }
            var dataRequest = ""

            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(result => {

            })
        }

        $(document).ready(function () {
            get_animals()
        })
    </script>
@endsection