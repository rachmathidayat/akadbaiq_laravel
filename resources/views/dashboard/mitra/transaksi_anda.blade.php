@extends('dashboard.mitra.layout.index')

@section('content')
    <div class="modal modal-left detail-pembeli">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title is-size-6 fw600">Detail Pembeli</p>
                <button class="delete del-detail" aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                <div class="content desc-ternak">
                    <div class="columns is-desktop col-netral">
                        <div class="column is-narrow">
                            <div class="picprofile">
                                <img src="{{ asset('assets/img/profile.jpg') }}">
                            </div>
                        </div>
                        <div class="column align-items">
                            <div class="content desc-ternak">
                                <div class="farm-name fw500">Ajang Salim</div>
                                <div class="small">Jalan Kemandoran 3 RT 11 RW 03 No.44, Kel: Grogol Utara, Kec: Kebayoran Lama, 12210</div>
                                <a class="button btn-akadQ mt1">Point Map</a>
                            </div>
                        </div>
                    </div>
                    <table class="table mt1 is-fullwidth is-striped">
                        <tbody>
                        <tr>
                            <td>Komoditas</td>
                            <td class="fw500">: &nbsp;Sapi Limosin</td>
                        </tr>
                        <tr>
                            <td>Transaksi</td>
                            <td class="fw500">: &nbsp;12/05/18</td>
                        </tr>
                        <tr>
                            <td>Jenis Transaksi</td>
                            <td class="fw500">: &nbsp;Tabungan - 1/6</td>
                        </tr>
                        <tr>
                            <td>Jatuh Tempo</td>
                            <td class="fw500">: &nbsp;25/05/18</td>
                        </tr>
                        <tr>
                            <td>Harga</td>
                            <td class="fw500">: &nbsp;Rp 20.000.000</td>
                        </tr>
                        <tr>
                            <td>Keperluan</td>
                            <td class="fw500">: &nbsp;Pribadi</td>
                        </tr>
                        <tr>
                            <td>No Handphone / Telpon</td>
                            <td class="fw500">: &nbsp;081281532099</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>

    <div class="field">
        <label class="label"></label>
        <div class="control">
            <div class="select">
                <select>
                    <option value="all">Semua Transaksi</option>
                    <option value="sapi">Meunggu Persetujuan</option>
                    <option value="kambing">Sudah Bayar</option>
                    <option value="domba">Menabung</option>
                    <option value="domba">Dibatalkan</option>
                </select>
            </div>
        </div>
    </div>
    <div class="trans_list">
        <div class="box-transaksi shadow-box" data-animal="res.data[i].animal.uuid">
            <div class="info-transaksi">
                <div class="level">
                    <div class="level-left">
                        <div class="level-item tgl-transaksi">res.data[i].dateCreated</div>
                    </div>
                    <div class="level-right">
                        <div class="level-item invoice">res.data[i].orderNo</div>
                    </div>
                </div>
            </div>
            <div class="columns is-desktop col-netral align-items detail-transaksi mb0 pb0 is-multiline">
                <div class="column is-4 is-12-mobile">
                    <div class="columns is-desktop col-netral align-items">
                        <div class="column">
                            <div class="img-transaksi">
                                <img src="{{ asset('assets/img/sapi-1-min.jpg') }}">
                            </div>
                        </div>
                        <div class="column">
                            <div class="content">
                                <div class="fw500">res.data[i].animal.name</div>
                                <div class="small">Rp. res.data[i].orderPrice</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-6-mobile">
                    <div class="content">
                        <div class="fw500">Bayar</div>
                        <div class="small">Tabungan</div>
                    </div>
                </div>
                <div class="column is-3 is-6-mobile">
                    <div class="content">
                        <div class="fw500">Nama Pembeli</div>
                        <div class="small">res.data[i].buyer.userProfile.fullName</div>
                    </div>
                </div>
                <div class="column is-2">
                    <div class="content">
                        <div class="fw500">Proses</div>
                        <div class="small is-text-blue">res.data[i].transactionType</div>
                    </div>
                </div>
                <div class="column is-2">
                    <div class="field">
                        <a class="button is-fullwidth btn-akadQ findDetail">Detail Pembeli</a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="more-detail">
                <div class="level">
                    <div class="level-left">
                        <div class="level-item">
                            <table class="table">
                                <tr>
                                    <td>Harga Ongkir</td>
                                    <td>Rp. res.data[i].shippingPrice</td>
                                </tr>
                                <tr class="fw600">
                                    <td>Subtotal</td>
                                    <td>Rp. res.data[i].subTotal</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="level-right">
                        <a class="level-item cancelOrder">
                            <svg class="mr0h" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="M21.03,3L18,20.31C17.83,21.27 17,22 16,22H8C7,22 6.17,21.27 6,20.31L2.97,3H21.03M5.36,5L8,20H16L18.64,5H5.36M9,18V14H13V18H9M13,13.18L9.82,10L13,6.82L16.18,10L13,13.18Z"/></svg>
                            <span>Batalkan Pesanan</span>
                        </a>
                        <a class="level-item delivery">
                            <svg class="mr0h" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="M12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2M12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20A8,8 0 0,0 20,12A8,8 0 0,0 12,4M11,16.5L6.5,12L7.91,10.59L11,13.67L16.59,8.09L18,9.5L11,16.5Z"/></svg>
                            <span>Terima Pesanan</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
    $('.del-detail').click(function () {
        $('.detail-pembeli').removeClass('is-active');
    });

    $('.cancelOrder').click(function () {
        $('.box-reason').addClass('is-active');
    });

    $('.del-reason').click(function () {
        $('.box-reason').removeClass('is-active');
    });

    $('.delivery').click(function () {
        $('.box-delivery').addClass('is-active')
    });

    $('.del-delivery').click(function () {
        $('.box-delivery').removeClass('is-active')
    });

    $('.findDetail').click(function(){
        $('.detail-pembeli').addClass('is-active');
    })

    function showMenu() {
        if ($('.sub-menus').hasClass('none')) {
            $('.sub-menus').addClass('block').removeClass('none');
            $('.menu-add-hewan svg').css('transform', 'rotate(90deg)')
        } else {
            $('.sub-menus').addClass('none').removeClass('block')
            $('.menu-add-hewan svg').css('transform', 'rotate(0deg)')
        }
    }
</script>
@endsection