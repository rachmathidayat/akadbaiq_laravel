<div class="modal modal-left box-reason">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title is-size-6 fw600">Alasan kenapa dihapus?</p>
            <button class="delete del-reason" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <ul class="reason">
                <li>Peternakan pindah tempat</li>
                <li>Stok hewan habis</li>
                <li>Pergantian pemilik</li>
                <li>Gulung tikar</li>
            </ul>
        </section>
    </div>
</div>