<div class="modal side-bar">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title has-text-left">Menu</p>
            <a class="close-modal">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" fill="#fff"/></svg>
            </a>
        </header>
        <section class="modal-card-body is-paddingless list-bar">
            <div class="media flex">
                <div class="media-content align-self">
                    <div class="content is-marginless has-text-left">
                        <a href="{{ url('dashboard/mitra') }}"><h3 class="subtitle is-6 is-marginless is-text-blue">Dashboard</h3></a>
                    </div>
                </div>
            </div>
            <div class="media flex">
                <div class="media-content align-self">
                    <div class="content is-marginless has-text-left">
                        <a href="{{ url('dashboard/mitra/pengaturan-peternakan') }}">
                            <h3 class="subtitle is-6 is-marginless is-text-blue">Pengaturan Mitra</h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="media flex">
                <div class="media-content align-self">
                    <div class="content is-marginless has-text-left">
                        <h3 class="subtitle is-6 is-marginless is-text-blue">Pengaturan Hewan</h3>
                        <div class="sub-mobile">
                            <a href="{{ url('dashboard/mitra/tambah-hewan') }}" class="mt1">Tambah Hewan</a>
                            <a href="{{ url('dashboard/mitra/hewan-dijual') }}">Hewan Dijual</a>
                            <a href="{{ url('dashboard/mitra/hewan-terjual') }}">Sudah Terjual</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="media flex">
                <div class="media-content align-self">
                    <div class="content is-marginless has-text-left">
                        <a href="{{ url('dashboard/mitra/transaksi-anda') }}">
                            <h3 class="subtitle is-6 is-marginless is-text-blue">Transaksi Anda</h3>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>