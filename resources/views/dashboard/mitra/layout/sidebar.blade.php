<div class="column for-left is-narrow pb0 is-hidden-mobile">
    <div class="left-menu is-back-grey">
        <div class="brand-icon has-text-centered">
            <a href="{{ url('/') }}"><img src="{{ asset('assets/img/logo.png') }}" alt="Logo" width="70%"></a>
        </div>
        <aside class="menu">
            <ul class="menu-list">
                @php
                    $currentUrl = explode('/', url()->current());
                    $currUrl = (isset($currentUrl[3]) ? $currentUrl[3] : '').'/'.(isset($currentUrl[4]) ? $currentUrl[4] : '').'/'.(isset($currentUrl[5]) ? $currentUrl[5] : '').'/'.(isset($currentUrl[6]) ? $currentUrl[6] : '');
                @endphp
                <li class="{{ Request::is('dashboard/mitra') || Request::is('dashboard/mitra/menunggu-persetujuan') || Request::is('dashboard/mitra/menunggu-pembayaran') || Request::is('dashboard/mitra/tunai-berhasil') || Request::is('dashboard/mitra/tabungan-berhasil') || Request::is('dashboard/mitra/tabungan-hewan') || Request::is('dashboard/mitra/menunggu-pengiriman') ? 'is-active' : '' }}"><a href="{{ url('dashboard/mitra') }}">Dashboard</a></li>
                <li class="{{ Request::is('dashboard/mitra/pengaturan-peternakan') || Request::is('dashboard/mitra/edit-peternakan') ? 'is-active' : '' }}"><a href="{{ url('dashboard/mitra/pengaturan-peternakan') }}">Pengaturan Mitra</a></li>
                <li class="menu-add-hewan {{ Request::is('dashboard/mitra/tambah-hewan') || Request::is('dashboard/mitra/hewan-dijual') || Request::is('dashboard/mitra/hewan-terjual') || Request::is($currUrl) ? 'is-active' : '' }}">
                    <a class="align-items relative" onclick="showMenu()">
                        Pengaturan Hewan
                        <span>
                    <svg class="align-items" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z"/></svg>
                  </span>
                    </a>
                    <ul class="sub-menus none">
                        <li><a href="{{ url('dashboard/mitra/tambah-hewan') }}">Tambah Hewan</a></li>
                        <li><a href="{{ url('dashboard/mitra/hewan-dijual') }}">Hewan Dijual</a></li>
                        <li><a href="{{ url('dashboard/mitra/hewan-terjual') }}">Sudah Terjual</a></li>
                    </ul>
                </li>
                <li class="{{ Request::is('dashboard/mitra/transaksi-anda') ? 'is-active' : '' }}"><a href="{{ url('dashboard/mitra/transaksi-anda') }}">Transaksi Anda</a></li>
            </ul>
        </aside>
    </div>
</div>