<div class="modal modal-left detail-hewan">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title is-size-6 fw600">Detail Hewan</p>
            <button class="delete del-detail" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <div class="swiper-container swiper3">
                <!-- banner slider -->
                <div class="swiper-wrapper">
                    <div class="swiper-slide"><img src="{{ asset('assets/img/sapi-1-min.jpg') }}"></div>
                    <div class="swiper-slide"><img src="{{ asset('assets/img/sapi-2-min.jpg') }}"></div>
                    <div class="swiper-slide"><img src="{{ asset('assets/img/sapi-3-min.jpg') }}"></div>
                </div>
                <!-- pagination -->
                <div class="swiper-pagination pag3"></div>
            </div>
            <div class="last-update has-text-right">
                Terakhir update : 5 Juni 2018 20:145 WIB
            </div>

            <div class="content desc-ternak mt1">
                <div class="fw500">Sapi Bali Super</div>
                <table class="table mt1 is-fullwidth is-striped">
                    <tbody>
                    <tr>
                        <td>Umur</td>
                        <td>: &nbsp;2</td>
                    </tr>
                    <tr>
                        <td>Panjang</td>
                        <td>: &nbsp;100cm</td>
                    </tr>
                    <tr>
                        <td>Tinggi</td>
                        <td>: &nbsp;50cm</td>
                    </tr>
                    <tr>
                        <td>Lingkar Dada</td>
                        <td>: &nbsp;160cm</td>
                    </tr>
                    <tr>
                        <td>Berat</td>
                        <td>: &nbsp;90kg</td>
                    </tr>
                    <tr>
                        <td>Tenor</td>
                        <td>: &nbsp;2/6</td>
                    </tr>
                    </tbody>
                </table>
                <div class="last-update has-text-right mt2">
                    Terakhir update : 5 Juni 2018 20:145 WIB
                </div>
            </div>
        </section>
    </div>
</div>