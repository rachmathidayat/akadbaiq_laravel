<!DOCTYPE html>
<html>
<head>
    @include('dashboard.mitra.layout.header')
    @yield('style')
</head>
<body>

@include('dashboard.mitra.layout.modal_sidebar')

@include('dashboard.mitra.layout.modal_box_reason')

@include('dashboard.mitra.layout.modal_detail_farm')

@include('dashboard.mitra.layout.modal_detail_hewan')

<section class="hero">
    <div class="hero-head relative shadow-bottom is-hidden-desktop">
        <nav class="navbar">
            <div class="container">
                <div class="navbar-brand">
                    <a href="{{ url('/') }}" class="navbar-item">
                        <img src="{{ asset('assets/img/logo.png') }}" alt="Logo">
                    </a>
                    <span class="navbar-burger burger">
          <span></span>
          <span></span>
          <span></span>
          </span>
                </div>
            </div>
        </nav>
    </div>
    <div class="columns col-netral">

        @include('dashboard.mitra.layout.sidebar')

        <div class="column for-right">
            <div class="right-menu">

                @include('dashboard.mitra.layout.navbar')

                <!-- content -->
                @yield('content')

            </div>
        </div>
    </div>
</section>
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/global.js') }}" type="text/javascript"></script>
@yield('script')
<script>
//    function detailFarm(){
//        $('.detail-farm').addClass('is-active');
//    }

    $('.del-detail').click(function(){
        $('.detail-farm').removeClass('is-active');
    })

//    $('.del-farm').click(function(){
//        $('.box-reason').addClass('is-active');
//        $('.detail-farm').removeClass('is-active');
//    })

    $('.del-reason').click(function(){
        $('.box-reason').removeClass('is-active');
    })

    $('.add-farm').click(function(){
        window.location.href='mitra-tambah-peternakan.html';
    })
    $('.edit-farm').click(function(){
        window.location.href='{{ url('dashboard/mitra/edit-peternakan') }}';
    })

    function showMenu(){
        if($('.sub-menus').hasClass('none')) {
            $('.sub-menus').addClass('block').removeClass('none');
            $('.menu-add-hewan svg').css('transform', 'rotate(90deg)')
        } else {
            $('.sub-menus').addClass('none').removeClass('block')
            $('.menu-add-hewan svg').css('transform', 'rotate(0deg)')
        }
    }

    // sidebar
    function noscroll(){
        $('html').addClass('hide-overflow')
    }
    function scroll(){
        $('html').removeClass('hide-overflow')
    }

    $('.burger').click(function(){
        $('.side-bar').addClass('is-active')
        noscroll()
    })
    $(".modal-background, .close-modal").click(function(){
        $(this).parents('.modal').removeClass('is-active')
        scroll()
    })
    function profile(){
        window.location.href = "/profile/{{ Session::get('userUUID') }}"
    }
    function pembelian(){
        window.location.assign("{{ url('dashboard/buyer/transaksi-pembelian') }}")
    }
    function logout(){
        $.ajax({
            method: "POST",
            url: "{{ url('/auth/logout') }}",
            headers: "",
            data: "",
            success: function (response) {
                localStorage.removeItem('aq_login_status');
                localStorage.removeItem('aq_mitra_status');
                location.reload(true);
            }
        })
    }
    // for inbox
    $('.for-inbox').on('click', function(){
        $('.menu-inbox').toggle()
        $('.menu-notif').css('display', 'none')
        $('.menu-akun-inside').css('display', 'none')
    });
    $('.for-notif').on('click', function(){
        $('.menu-notif').toggle()
        $('.menu-inbox').css('display', 'none')
        $('.menu-akun-inside').css('display', 'none')
    });
    $('.for-akun').on('click', function(){
        $('.menu-akun-inside').toggle()
        $('.menu-notif').css('display', 'none')
        $('.menu-inbox').css('display', 'none')
    });
</script>
</body>
</html>