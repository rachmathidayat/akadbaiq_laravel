<div class="modal modal-left detail-farm">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title is-size-6 fw600">Detail Mitra</p>
            <button class="delete del-detail" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <div class="columns col-netral">
                <div class="column is-5">
                    <div class="img-ternak-detail">
                        <img src="{{ asset('assets/img/banner-1.jpg') }}">
                    </div>
                </div>
                <div class="column relative">
                    <div class="columns col-netral">
                        <div class="column">
                            <div class="content desc-ternak">
                                <div class="fw500">Bonsle Farm.UD</div>
                                <div class="small">Depok, Jawa Barat</div>
                            </div>
                            <div class="content desc-ternak">
                                <div class="fw500">Penanggung Jawab</div>
                                <div class="small">Dadan Suramdan</div>
                            </div>
                        </div>
                        <div class="column">
                            <div class="content desc-ternak">
                                <div class="fw500">No Telp/HP</div>
                                <div class="small">081193337511</div>
                            </div>
                            <div class="content desc-ternak">
                                <div class="fw500">Email</div>
                                <div class="small">dadan.suramdan@gmail.com</div>
                            </div>
                        </div>
                    </div>
                    <div class="field has-addons action-button two">
                        <p class="control">
                            <a class="button is-fullwidth edit-farm">
                  <span class="icon mr0h">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="M5,3C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V12H19V19H5V5H12V3H5M17.78,4C17.61,4 17.43,4.07 17.3,4.2L16.08,5.41L18.58,7.91L19.8,6.7C20.06,6.44 20.06,6 19.8,5.75L18.25,4.2C18.12,4.07 17.95,4 17.78,4M15.37,6.12L8,13.5V16H10.5L17.87,8.62L15.37,6.12Z" /></svg>
                  </span>
                                <span>Edit</span>
                            </a>
                        </p>
                        <p class="control">
                            <a class="button btn-danger is-fullwidth del-farm">
                  <span class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="M21.03,3L18,20.31C17.83,21.27 17,22 16,22H8C7,22 6.17,21.27 6,20.31L2.97,3H21.03M5.36,5L8,20H16L18.64,5H5.36M9,18V14H13V18H9M13,13.18L9.82,10L13,6.82L16.18,10L13,13.18Z" /></svg>
                  </span>
                                <span>Hapus</span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>