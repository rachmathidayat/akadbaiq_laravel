@extends('dashboard.mitra.layout.index')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/swiper.css') }}">
@endsection

@section('content')
    <div class="modal modal-left detail-pembeli">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title is-size-6 fw600">Detail Transaksi Tunai Berhasil</p>
                <button class="delete del-detail" aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                <div class="content desc-ternak">
                    <div class="columns col-netral">
                        <div class="column is-narrow">
                            <div class="picprofile">
                                <img src="{{ asset('assets/img/profile.jpg') }}">
                            </div>
                        </div>
                        <div class="column align-items">
                            <div class="content desc-ternak">
                                <div class="farm-name fw500">Ajang Salim</div>
                                <div class="small">Jalan Kemandoran 3 RT 11 RW 03 No.44, Kel: Grogol Utara, Kec: Kebayoran Lama, 12210</div>
                                <a class="button btn-akadQ mt1">Point Map</a>
                            </div>
                        </div>
                    </div>
                    <table class="table mt1 is-fullwidth is-striped" id="printTable">
                        <tbody>
                        <tr>
                            <td>Komoditas</td>
                            <td class="fw500">: &nbsp;Kambing Kacang</td>
                        </tr>
                        <tr>
                            <td>Transaksi</td>
                            <td class="fw500">: &nbsp;12/05/18</td>
                        </tr>
                        <tr>
                            <td>Nomor Invoice</td>
                            <td class="fw500">: &nbsp;INV/20180605/XVIII/VI/168854789</td>
                        </tr>
                        <tr>
                            <td>Jenis Transaksi</td>
                            <td class="fw500">: &nbsp;Tunai</td>
                        </tr>
                        <tr>
                            <td>Jumlah Pembayaran</td>
                            <td class="fw500">: &nbsp;Rp 3.000.000</td>
                        </tr>
                        <tr>
                            <td>Tanggal Pembayaran</td>
                            <td class="fw500">: &nbsp;15/05/18</td>
                        </tr>
                        <tr>
                            <td>No Handphone / Telpon</td>
                            <td class="fw500">: &nbsp;081281532099</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>

    <div class="content has-text-left">
        <div class="title">
            Transaksi Tunai Berhasil
        </div>
        <div class="columns col-netral">
            <div class="column is-7">
                <p class="subtitle is-size-6 fw300">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </div>
    </div>
    <!-- table -->
    <div class="table-responsive">
        <table class="table table-tunai is-striped is-fullwidth">
            <thead>
            <tr>
                <th>Detail</th>
                <th>Pembeli</th>
                <th>Komoditas</th>
                <th>Transaksi</th>
                <th>Harga</th>
                <th>Invoice</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <div class="find-detail is-pulled-left">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>
                    </div>
                </td>
                <td>Ajang Salim</td>
                <td>Sapi Limosin</td>
                <td>12/05/2018</td>
                <td>Rp 20.000.000</td>
                <td class="approval">
                    <a href="#" class="button btn-akadQ w-100">Print</a>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="find-detail is-pulled-left">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>
                    </div>
                </td>
                <td>Sarifudin</td>
                <td>Kambing Kacang</td>
                <td>13/05/2018</td>
                <td>Rp 3.000.000</td>
                <td class="approval">
                    <a href="#" class="button btn-akadQ w-100">Print</a>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="find-detail is-pulled-left">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>
                    </div>
                </td>
                <td>Saidah</td>
                <td>Kambing Jantan</td>
                <td>20/05/2018</td>
                <td>Rp 2.000.000</td>
                <td class="approval">
                    <a href="#" class="button btn-akadQ w-100">Print</a>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="find-detail is-pulled-left">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>
                    </div>
                </td>
                <td>Muhammad Saleh</td>
                <td>Sapi Jawa</td>
                <td>12/05/2018</td>
                <td>Rp 40.000.000</td>
                <td class="approval">
                    <a href="#" class="button btn-akadQ w-100">Print</a>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="find-detail is-pulled-left">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>
                    </div>
                </td>
                <td>Ahmad Fatah</td>
                <td>Sapi Aceh</td>
                <td>11/05/2018</td>
                <td>Rp 15.000.000</td>
                <td class="approval">
                    <a href="#" class="button btn-akadQ w-100">Print</a>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="find-detail is-pulled-left">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>
                    </div>
                </td>
                <td>Sapri</td>
                <td>Domba Garut</td>
                <td>12/05/2018</td>
                <td>Rp 1.500.000</td>
                <td class="approval">
                    <a href="#" class="button btn-akadQ w-100">Print</a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/swiper.min.js') }}"></script>
    <script>
        $('.find-detail').click(function(){
            $('.detail-pembeli').addClass('is-active');
        });

        $('.del-detail').click(function(){
            $('.detail-pembeli').removeClass('is-active');
        });

        $('.approval a').click(function(){
            var divToPrint=document.getElementById("printTable");
            newWin= window.open("");
            newWin.document.write(divToPrint.outerHTML);
            newWin.print();
            newWin.close();
        });
    </script>
@endsection