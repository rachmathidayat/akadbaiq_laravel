@extends('dashboard.mitra.layout.index')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/swiper.css') }}">
@endsection

@section('content')
    @include('dashboard.mitra.layout.modal_detail_hewan')

    <div class="content has-text-centered">
        <div class="title">
            Hewan Terjual
        </div>
        <p class="subtitle is-size-6 fw300">Berikut daftar hewan Anda yang sudah terjual di AkadBaiQ</p>
    </div>
    <div class="field">
        <label class="label"></label>
        <div class="control has-text-centered">
            <div class="select">
                <select class="w-250">
                    <option value="all">Semua Hewan</option>
                    <option value="sapi">Sapi</option>
                    <option value="kambing">Kambing</option>
                    <option value="domba">Domba</option>
                </select>
            </div>
        </div>
    </div>
    <div class="columns col-netral is-multiline mt2">
        <div class="column is-4">
            <div class="box-hewan relative">
                <div class="img-hewan">
                    <img src="{{ asset('assets/img/sapi.jpg') }}">
                </div>
                <div class="columns col-netral mb0">
                    <div class="column">
                        <div class="content desc-ternak">
                            <div class="fw500">Sapi Bali Super</div>
                            <div class="small">Depok, Jawa Barat</div>
                        </div>
                    </div>
                    <div class="column is-3">
                        <div class="find-detail" onclick="findDetail()">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"/></svg>
                        </div>
                    </div>
                </div>
                <div class="transaksi">
                    Transaksi :<span class="jenis-transaksi is-text-blue">Tabungan (2/6)</span>
                </div>
                <div class="box-price mt0h">
                    <strike class="normal-price">Rp 45.000.000</strike>
                    <span class="disc-price">Rp 35.000.000</span>
                </div>
                <div class="field mt2">
                    <a href="mitra-hewan-dijual.html" class="button btn-akadQ is-fullwidth disabled">Sudah Terjual</a>
                </div>
            </div>
        </div>
        <div class="column is-4">
            <div class="box-hewan relative">
                <div class="img-hewan">
                    <img src="{{ asset('assets/img/sapi.jpg') }}">
                </div>
                <div class="columns col-netral mb0">
                    <div class="column">
                        <div class="content desc-ternak">
                            <div class="fw500">Sapi Bali Super</div>
                            <div class="small">Depok, Jawa Barat</div>
                        </div>
                    </div>
                    <div class="column is-3">
                        <div class="find-detail" onclick="findDetail()">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"/></svg>
                        </div>
                    </div>
                </div>
                <div class="transaksi">
                    Transaksi :<span class="jenis-transaksi is-text-blue">Tunai</span>
                </div>
                <div class="box-price mt0h">
                    <strike class="normal-price">Rp 45.000.000</strike>
                    <span class="disc-price">Rp 35.000.000</span>
                </div>
                <div class="field mt2">
                    <a href="mitra-hewan-dijual.html" class="button btn-akadQ is-fullwidth disabled">Sudah Terjual</a>
                </div>
            </div>
        </div>
        <div class="column is-4">
            <div class="box-hewan relative">
                <div class="img-hewan">
                    <img src="{{ asset('assets/img/sapi.jpg') }}">
                </div>
                <div class="columns col-netral mb0">
                    <div class="column">
                        <div class="content desc-ternak">
                            <div class="fw500">Sapi Bali Super</div>
                            <div class="small">Depok, Jawa Barat</div>
                        </div>
                    </div>
                    <div class="column is-3">
                        <div class="find-detail" onclick="findDetail()">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"/></svg>
                        </div>
                    </div>
                </div>
                <div class="transaksi">
                    Transaksi :<span class="jenis-transaksi is-text-blue">Tunai</span>
                </div>
                <div class="box-price mt0h">
                    <strike class="normal-price">Rp 45.000.000</strike>
                    <span class="disc-price">Rp 35.000.000</span>
                </div>
                <div class="field mt2">
                    <a href="mitra-hewan-dijual.html" class="button btn-akadQ is-fullwidth disabled">Sudah Terjual</a>
                </div>
            </div>
        </div>
        <div class="column is-4">
            <div class="box-hewan relative">
                <div class="img-hewan">
                    <img src="{{ asset('assets/img/sapi.jpg') }}">
                </div>
                <div class="columns col-netral mb0">
                    <div class="column">
                        <div class="content desc-ternak">
                            <div class="fw500">Sapi Bali Super</div>
                            <div class="small">Depok, Jawa Barat</div>
                        </div>
                    </div>
                    <div class="column is-3">
                        <div class="find-detail" onclick="findDetail()">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"/></svg>
                        </div>
                    </div>
                </div>
                <div class="transaksi">
                    Transaksi :<span class="jenis-transaksi is-text-blue">Tunai</span>
                </div>
                <div class="box-price mt0h">
                    <strike class="normal-price">Rp 45.000.000</strike>
                    <span class="disc-price">Rp 35.000.000</span>
                </div>
                <div class="field mt2">
                    <a href="mitra-hewan-dijual.html" class="button btn-akadQ is-fullwidth disabled">Sudah Terjual</a>
                </div>
            </div>
        </div>
        <div class="column is-4">
            <div class="box-hewan relative">
                <div class="img-hewan">
                    <img src="{{ asset('assets/img/sapi.jpg') }}">
                </div>
                <div class="columns col-netral mb0">
                    <div class="column">
                        <div class="content desc-ternak">
                            <div class="fw500">Sapi Bali Super</div>
                            <div class="small">Depok, Jawa Barat</div>
                        </div>
                    </div>
                    <div class="column is-3">
                        <div class="find-detail" onclick="findDetail()">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"/></svg>
                        </div>
                    </div>
                </div>
                <div class="transaksi">
                    Transaksi :<span class="jenis-transaksi is-text-blue">Tunai</span>
                </div>
                <div class="box-price mt0h">
                    <strike class="normal-price">Rp 45.000.000</strike>
                    <span class="disc-price">Rp 35.000.000</span>
                </div>
                <div class="field mt2">
                    <a href="mitra-hewan-dijual.html" class="button btn-akadQ is-fullwidth disabled">Sudah Terjual</a>
                </div>
            </div>
        </div>
        <div class="column is-4">
            <div class="box-hewan relative">
                <div class="img-hewan">
                    <img src="{{ asset('assets/img/sapi.jpg') }}">
                </div>
                <div class="columns col-netral mb0">
                    <div class="column">
                        <div class="content desc-ternak">
                            <div class="fw500">Sapi Bali Super</div>
                            <div class="small">Depok, Jawa Barat</div>
                        </div>
                    </div>
                    <div class="column is-3">
                        <div class="find-detail" onclick="findDetail()">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"/></svg>
                        </div>
                    </div>
                </div>
                <div class="transaksi">
                    Transaksi :<span class="jenis-transaksi is-text-blue">Tunai</span>
                </div>
                <div class="box-price mt0h">
                    <strike class="normal-price">Rp 45.000.000</strike>
                    <span class="disc-price">Rp 35.000.000</span>
                </div>
                <div class="field mt2">
                    <a href="mitra-hewan-dijual.html" class="button btn-akadQ is-fullwidth disabled">Sudah Terjual</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/swiper.min.js') }}"></script>
    <script>
        function findDetail(){
            $('.detail-hewan').addClass('is-active');
            $('html').addClass('hide-overflow')
            var swiper3 = new Swiper('.swiper3', {
                slidesPerView: 1,
                loop: true,
                autoplay: true,
                pagination: {
                    el: '.swiper-pagination.pag3',
                    clickable: true,
                },
            });
        }

        $('.del-detail').click(function(){
            $('.detail-hewan').removeClass('is-active');
            $('html').removeClass('hide-overflow')
        })

        function showMenu(){
            if($('.sub-menus').hasClass('none')) {
                $('.sub-menus').addClass('block').removeClass('none');
                $('.menu-add-hewan svg').css('transform', 'rotate(90deg)')
            } else {
                $('.sub-menus').addClass('none').removeClass('block')
                $('.menu-add-hewan svg').css('transform', 'rotate(0deg)')
            }
        }
    </script>
@endsection