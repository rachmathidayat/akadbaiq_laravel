@extends('dashboard.mitra.layout.index')

@section('content')
    <p class="ranch_notes">Terdapat <strong class="ranch_total">0</strong> Peternakan</p>
    <div class="columns col-netral is-multiline ranch_list">
        <div class="column is-3">
            <div class="box-ternak shadow-box">
                <div class="img-ternak">
                    <img src="{{ asset('assets/img/domba.jpg') }}">
                </div>
                <div class="content desc-ternak">
                    <div class="farm-name fw500">ForkFarm</div>
                    <div class="small" style="height: 21px; overflow:  hidden;">Jl. Anggur III Blok C6 No.11, RT.5/RW.8</div>
                </div>
                <div class="field mt1">
                    <a href="#" class="button btn-akadQ" onclick="detailFarm()">Lihat Detail</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function detailFarm(){
            $('.detail-farm').addClass('is-active');
        }

        $('.del-farm').click(function(){
            $('.box-reason').addClass('is-active');
            $('.detail-farm').removeClass('is-active');
        })
    </script>
@endsection