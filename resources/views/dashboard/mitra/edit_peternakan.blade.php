@extends('dashboard.mitra.layout.index')

@section('content')
    <div class="modal modal-left point-map">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title is-size-6">Gunakan Titik Lokasi Anda</p>
                <button class="delete" aria-label="close"></button>
            </header>
            <section class="modal-card-body relative">
                <div class="search-point">
                    <div class="field">
                        <div class="control">
                            <input id="geocomplete" class="input" value="Jakarta, Indonesia" type="text" placeholder="Cari Alamat" />
                        </div>
                    </div>
                </div>
                <div id="map_canvas" class="map_canvas margin-auto" style="width: 500px; height: 400px;"></div>
            </section>
            <footer class="modal-card-foot j-center">
                <button class="button btn-akadQ btn-pick-address w-250">Ambil Alamat</button>
            </footer>
        </div>
    </div>

    <div class="modal modal-left keterangan">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title is-size-6">Keterangan Area</p>
                <button class="delete" aria-label="close"></button>
            </header>
            <section class="modal-card-body relative">
                <div class="content">
                    <div class="fw500">Tentang Area Penjualan</div>
                    <div class="small">Area penjualan adalah jangkauan pengiriman yang dapat anda sanggupi untuk Mitra ini</div>
                    <div class="fw500 mt1">Area penjualan yang tersedia</div>
                    <ol class="areaDetail">
                        <li>Area Jabodetabek
                            <ul>
                                <li>DKI Jakarta</li>
                                <li>Kota Bogot</li>
                                <li>Kota Depok</li>
                                <li>Kota Tangerang</li>
                                <li>Kabupaten Tangerang</li>
                                <li>Kota Tangerang Sekatan</li>
                                <li>Kota Bekasi</li>
                            </ul>
                        </li>
                        <li>Area Banten
                            <ul>
                                <li>Kabupaten Lebak</li>
                                <li>Kota Serang</li>
                                <li>Kabupaten Pandeglang</li>
                                <li>Kota Cilegon</li>
                            </ul>
                        </li>
                        <li>Area Jawa Barat 1
                            <ul>
                                <li>Kabupaten Bekasi</li>
                                <li>Kabupaten Bogor</li>
                                <li>Kabupaten Sukabumi</li>
                                <li>Kabupaten Cianjur</li>
                                <li>Kota Sukabumi</li>
                            </ul>
                        </li>
                        <li>Area Jawa Barat 2
                            <ul>
                                <li>Kabupaten Bandung</li>
                                <li>Kabupaten Tasikmalaya</li>
                                <li>Kabupaten Cimahi</li>
                                <li>Kabupaten Sumedang</li>
                                <li>Kabupaten Subang</li>
                                <li>Kabupaten Garut</li>
                                <li>Kota Sumedang</li>
                                <li>Kota Bandung</li>
                                <li>Kota Tasikmalaya</li>
                                <li>Kota Cirebon</li>
                                <li>Kota Garut</li>
                            </ul>
                        </li>
                    </ol>
                </div>
            </section>
        </div>
    </div>

    {{--content--}}
    <div class="content has-text-centered">
        <div class="title">
            Data Mitra
        </div>
        <p class="subtitle is-size-6 fw300">Masukan data-data Mitra Anda dibawah ini secara lengkap</p>
        <!-- pribadi -->
        <div class="pribadi">
            <div class="columns is-centered mt1">
                <div class="column is-8">
                    <div class="badanusaha none">
                        <div class="columns">
                            <div class="column">
                                <div class="field mt1">
                                    <label class="label has-text-left">Nama Badan Usaha</label>
                                    <div class="control">
                                        <input class="input" type="text" placeholder="Nama Badan Usaha">
                                    </div>
                                </div>
                            </div>
                            <div class="column">
                                <div class="field mt1">
                                    <label class="label has-text-left">Nomor SIUP</label>
                                    <div class="control">
                                        <input class="input" type="text" placeholder="Nomor SIUP">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="columns">
                        <div class="column">
                            <div class="field mt1">
                                <label class="label has-text-left">Nama Penanggung Jawab</label>
                                <div class="control">
                                    <input class="input inp_name" type="text" placeholder="Nama Penanggung Jawab">
                                </div>
                            </div>
                            <div class="field mt1">
                                <label class="label has-text-left">Alamat Mitra</label>
                                <div class="control">
                                    <textarea class="textarea inp_alamatp" placeholder="Alamat Peternakan"></textarea>
                                </div>
                            </div>
                            <div class="list-ask mt1">
                                <button class="accordion">Area Penjualan</button>
                                <div class="panels areaPenjualan">
                                    <p class="has-text-left">
                            <span class="field block">
                              <input class="is-checkradio is-info inp_animal" id="Jabodetabek" type="checkbox" name="area" value="Jabodetabek" checked="checked">
                              <label for="Jabodetabek" class="no-margin">Jabodetabek <span class="is-text-blue">(Rp 300.000)</span></label>
                            </span>
                                        <span class="field block">
                              <input class="is-checkradio is-info inp_animal" id="Banten" type="checkbox" name="area" value="Banten">
                              <label for="Banten" class="no-margin">Banten <span class="is-text-blue">(Rp 400.000)</span></label>
                            </span>
                                        <span class="field block">
                              <input class="is-checkradio is-info inp_animan" id="JawaBarat1" type="checkbox" name="area" value="JawaBarat1">
                              <label for="JawaBarat1" class="no-margin">Jawa Barat 1 <span class="is-text-blue">(Rp 500.000)</span></label>
                            </span>
                                        <span class="field block">
                              <input class="is-checkradio is-info inp_animal" id="JawaBarat2" type="checkbox" name="area" value="JawaBarat2">
                              <label for="JawaBarat2" class="no-margin">Jawa Barat 2 <span class="is-text-blue">(Rp 500.000)</span></label>
                            </span>
                                        <span class="field block">
                              <input class="is-checkradio is-info inp_animal" id="JawaTengah1" type="checkbox" name="area" value="SAPI">
                              <label for="JawaTengah1" class="no-margin">Jawa Tengah 1 <span class="is-text-blue">(Rp 600.000)</span></label>
                            </span>
                                        <span class="field block">
                              <input class="is-checkradio is-info inp_animal" id="JawaTImur1" type="checkbox" name="area" value="JawaTImur1">
                              <label for="JawaTImur1" class="no-margin">Jawa Timur 1 &amp; Madura <span class="is-text-blue">(Rp 600.000)</span></label>
                            </span>
                                        <span class="field block">
                              <input class="is-checkradio is-info inp_animal" id="Yogyakarta" type="checkbox" name="area" value="Yogyakarta">
                              <label for="Yogyakarta" class="no-margin">DI. Yogyakarta <span class="is-text-blue">(Rp 600.000)</span></label>
                            </span>
                                    </p>
                                </div>
                                <div class="is-text-blue field mt0h has-text-right pointer" onclick="keterangan()">Lihat Keterangan</div>
                            </div>
                            <div class="field mt1">
                                <label class="label has-text-left">Pin Alamat (opsional)</label>
                                <div class="pin-point relative">
                                    <a href="#" class="button btn-map">Ubah Lokasi</a>
                                </div>
                                <form>
                                    <input type="hidden" name="formatted_address" />
                                    <input type="hidden" name="lat" />
                                    <input type="hidden" name="lng" />
                                    <label class="label location-label-lat mb0 lat-lang">Latitude - Longitude</label>
                                    <label class="label location-label-add formatted_address">Alamat Lengkap</label>
                                </form>
                            </div>
                        </div>
                        <div class="column">
                            <div class="field mt1">
                                <label class="label has-text-left">Nama Mitra</label>
                                <div class="control">
                                    <input class="input inp_namep" type="text" placeholder="Nama Peternakan">
                                </div>
                            </div>
                            <div class="field has-text-left mt1">
                                <label class="label">Komoditas Hewan</label>
                                <input class="is-checkradio is-info inp_animal" id="sapi" type="checkbox" name="sapi" value="sapi" checked="checked">
                                <label for="sapi" class="no-margin">Sapi</label>
                                <input class="is-checkradio is-info inp_animal" id="kambing" type="checkbox" name="kambing" value="kambing">
                                <label for="kambing" class="no-margin">Kambing</label>
                                <input class="is-checkradio is-info inp_animal" id="domba" type="checkbox" value="domba" name="domba">
                                <label for="domba" class="no-margin">Domba</label>
                            </div>
                            <div class="field has-text-left mt1">
                                <label class="label">Jenis Program</label>
                                <input class="is-checkradio is-info inp_program" id="kurban" type="checkbox" name="kurban" value="kurban" checked="checked">
                                <label for="kurban" class="no-margin">Qurban</label>
                                <input class="is-checkradio is-info inp_program" id="akikah" type="checkbox" name="akikah" value="akikah">
                                <label for="akikah" class="no-margin">Aqiqah</label>
                            </div>
                            <div class="field has-text-left mt1">
                                <label class="label">Pengiriman </label>
                                <input class="is-checkradio is-info" id="kir_akadbaiq" type="radio" name="pengiriman" checked="checked">
                                <label for="kir_akadbaiq" class="ml0">Jasa AkadBaiQ</label>
                                <input class="is-checkradio is-info" id=kir_pribadi type="radio" name="pengiriman">
                                <label for=kir_pribadi>Jasa Pribadi</label>
                            </div>
                            <div class="field mt2h">
                                <label class="label has-text-left mb-75">Upload Foto Mitra</label>
                                <input type="file" class="inputfile inputfile-2" id="file" >
                                <label for="file">
                                    <div class="pic-ternak pointer">
                                        <div>
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75 67.5" width="50">
                                                <defs>
                                                    <style>
                                                        .pic-ternak {
                                                            fill: #a7a7a7;
                                                        }
                                                    </style>
                                                </defs>
                                                <g id="camera" transform="translate(0 -25.5)">
                                                    <g id="camera-alt" transform="translate(0 25.5)">
                                                        <circle id="Ellipse_71" data-name="Ellipse 71" class="pic-ternak" cx="12" cy="12" r="12" transform="translate(25.5 25.5)"/>
                                                        <path id="Path_31604" data-name="Path 31604" class="pic-ternak" d="M26.25,25.5,19.5,33H7.5A7.522,7.522,0,0,0,0,40.5v45A7.522,7.522,0,0,0,7.5,93h60A7.522,7.522,0,0,0,75,85.5v-45A7.522,7.522,0,0,0,67.5,33h-12l-6.75-7.5ZM37.5,81.75A18.75,18.75,0,1,1,56.25,63,18.568,18.568,0,0,1,37.5,81.75Z" transform="translate(0 -25.5)"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                </label>
                                <div id="filename" class="mt0h">File format: jpg atau png</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="field mt2">
            <a href="mitra-pengaturan-peternakan.html" class="button btn-akadQ w-250 btn_submit">Simpan</a>
        </div>
    </div>
@endsection

@section('script')
    <script src='http://maps.google.com/maps/api/js?key=AIzaSyC58CZr-iYkzHQRS5d2JRmSRQ0-mzKD5-4&sensor=false&libraries=places'></script>
    <script src='{{ asset('assets/js/jquery.geocomplete.js') }}'></script>
    <script>
        const GlobalJS = new Global()

        var geodecoder =  new google.maps.Geocoder();
        var lats;
        var lngs;
        var maps;
        var coord = {lat:  -6.1762117, lng: 106.8259329};

        function initMap() {
            maps = new google.maps.Map(document.getElementById('map_canvas'), {
                center: coord,
                zoom: 10
            });
        }

        /* remove array function */
        Array.prototype.remove = function() {
            var what, a = arguments, L = a.length, ax;
            while (L && this.length) {
                what = a[--L];
                while ((ax = this.indexOf(what)) !== -1) {
                    this.splice(ax, 1);
                }
            }
            return this;
        };

        /* MAP */
        $("#geocomplete").geocomplete({
            map: ".map_canvas",
            details: "form ",
            markerOptions: {
                draggable: true
            }
        });
        $("#geocomplete").val("Jakarta");
        $("#geocomplete").bind("geocode:dragged", function(event, latLng){
            lats = latLng.lat();
            lngs = latLng.lng();
            $(".lat-lang").html(lats+" - "+lngs);
            $(".lat-lang").html(lats+" - "+lngs);
            $("#reset").show();
            geodecoder.geocode({
                'latLng': new google.maps.LatLng({lat: latLng.lat(), lng: latLng.lng()})
            }, function (results, status) {
                if (status ==
                    google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        alert("Ambil alamat ini ("+results[0].formatted_address+") ?");
                        //alert(results[0].formatted_address);
                        $(".formatted_address").html(results[0].formatted_address);
                        $("#geocomplete").val(results[0].formatted_address);

                    } else {
                        alert('No results found');
                    }
                } else {
                    alert('Geocoder failed due to: ' + status);
                }
            });
        });

        $("#reset").click(function(){
            $("#geocomplete").geocomplete("resetMarker");
            $("#reset").hide();
            return false;
        });

        $("#find").click(function(){
            $("#geocomplete").trigger("geocode");
        }).click();

        $(".inp_program").click(function(){
            if($(this).is(':checked')){
                programType.push($(this).val());
            }else{
                programType.remove($(this).val())
            }
        })

        function pilih(param) {
            if(param == "pribadi") {
                $('.badanusaha').addClass('none').removeClass('block')
                requisitionType = "PERSONAL"
            } else {
                $('.badanusaha').addClass('block').removeClass('none')
                requisitionType = "CORPORATE"
            }
        }

        $('.btn-map').click(function(){
            $('.point-map').addClass('is-active');
            $("#geocomplete").trigger("geocode");
        })

        $('.delete, .btn-pick-address').click(function(){
            $(this).parents('.modal').removeClass('is-active')
        })

        function keterangan(){
            $('.keterangan').addClass('is-active')
        }

        function showMenu(){
            if($('.sub-menus').hasClass('none')) {
                $('.sub-menus').addClass('block').removeClass('none');
                $('.menu-add-hewan svg').css('transform', 'rotate(90deg)')
            } else {
                $('.sub-menus').addClass('none').removeClass('block')
                $('.menu-add-hewan svg').css('transform', 'rotate(0deg)')
            }
        }

        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight){
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }

        document.getElementById("file").onchange = function(event) {
            var file = document.getElementById("file");
            if(file.files.length > 0)
            {

                var pathFile = file.value
                var nameFile = file.files[0].name
                var typeFile = file.files[0].type

                if(typeFile != 'image/jpg' && typeFile != 'image/jpeg' && typeFile != 'image/png'){
                    alert('format file tidak didukung')
                    $('.pic-ternak svg').addClass('block').removeClass('none')
                    $('.pic-ternak').css('background-image', 'url()')
                    $('#filename').text("File format: jpg atau png");
                    return false
                }

                var reader = new FileReader();
                reader.readAsDataURL(event.srcElement.files[0]);
                var me = this;
                reader.onload = function () {
                    var fileContent = reader.result;
                    console.log(reader);
                    $('#filename').text(file.files[0].name);
                    $('.pic-ternak svg').addClass('none');
                    $('.pic-ternak').css('background-image', 'url('+fileContent+')');
                    $('.pic-ternak').css('background-size', 'contain');
                    $('.pic-ternak').css('background-repeat', 'no-repeat');
                    $('.pic-ternak').css('background-position', 'center');
                }
            }
        }
    </script>
@endsection