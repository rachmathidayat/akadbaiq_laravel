@extends('dashboard.buyer.layout.index')

@section('content')
    @include('dashboard.buyer.layout.modal_detail_transaksi_pembelian')

    <div class="content has-text-left">
        <div class="title">
            Riwayat Pembayaran
        </div>
        <div class="columns col-netral">
            <div class="column is-7">
                <p class="subtitle is-size-6 fw300">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </div>
    </div>
    <!-- table -->
    <div class="table-responsive">
        <table class="table table-riwayat is-striped is-fullwidth">
            <thead>
            <tr>
                <th>Detail</th>
                <th>Mitra</th>
                <th>Komoditas</th>
                <th>Tanggal</th>
                <th>Bayar</th>
                <th>Harga</th>
                <th>Invoice</th>
            </tr>
            </thead>
            <tbody class="tbodyRiwayatPembayaran">
            {{--<tr>--}}
                {{--<td>--}}
                    {{--<div class="find-detail is-pulled-left">--}}
                        {{--<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>--}}
                    {{--</div>--}}
                {{--</td>--}}
                {{--<td>UD BAHARUDIN MS</td>--}}
                {{--<td>Sapi Limosin</td>--}}
                {{--<td>12/05/2018</td>--}}
                {{--<td>1/6</td>--}}
                {{--<td>Rp 5.000.000</td>--}}
                {{--<td class="approval">--}}
                    {{--<a href="#" class="button btn-akadQ w-100">Print</a>--}}
                {{--</td>--}}
            {{--</tr>--}}
            </tbody>
        </table>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            getOrderBuyerList()
        })

        function getOrderBuyerList(){
            var methodRequest = "GET"
            var urlRequest = "{{ env('HOST_API') }}/api/order/buyer/list?"
            var headerRequest = {
                Authorization: "{{ Session::get('accessToken') }}",
                'Accept-Version': '1.0'
            }
            var dataRequest = ""

            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseOrderBuyerList => {
//                console.log(responseOrderBuyerList)
                if(responseOrderBuyerList.data.length > 0){
                    var tbodyRiwayatPembayaran = $('.tbodyRiwayatPembayaran')
                    responseOrderBuyerList.data.map((item, index) => {

                        var transactionType = ''
                        if(item.transactionType == 'Tabungan'){
                            transactionType = item.paymentTerms.confirmed+'/'+item.paymentTerms.target
                        }else{
                            transactionType = 'Tunai'
                        }

                        tbodyRiwayatPembayaran.append(
                            `<tr>
                                <input type="hidden" id="orderUUid_`+index+`" value="`+item.uuid+`" />
                                <td>
                                    <div class="find-detail is-pulled-left" onclick\=findDetail(`+index+`)\>
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>
                                    </div>
                                </td>
                                <td>`+item.mitra.name+`</td>
                                <td>`+item.animal.name+`</td>
                                <td>`+item.dateCreated.substr(0, 11).split("-").join(" ")+`</td>
                                <td>`+transactionType+`</td>
                                <td>`+GlobalJS.convertToRupiah(Math.floor(item.subTotal))+`</td>
                                <td class="approval" onclick\=print(`+index+`)\>
                                    <a href="#" class="button btn-akadQ w-100">Print</a>
                                </td>
                            </tr>`
                        )
                    })
                }
            })
        }

        function findDetail(index){
//            alert($('#orderUUid_'+index).val())
            getOrderByUuid($('#orderUUid_'+index).val())
        }

        function getOrderByUuid(orderUuid){
            var methodRequest = "GET"
            var urlRequest = "{{ env('HOST_API') }}/api/order/get/"+orderUuid
            var headerRequest = {
                Authorization: "{{ Session::get('accessToken') }}",
                'Accept-Version': '1.0'
            }
            var dataRequest = ""

            GlobalJS.requestAjaxSync(methodRequest, urlRequest, headerRequest, dataRequest).then(responseOrderBuyerByUuid => {
                console.log(responseOrderBuyerByUuid)
                $('.dateDetailOrder').html(': '+responseOrderBuyerByUuid.data.dateCreated.substr(0, 11).split("-").join(" "))
                $('.orderNoDetailOrder').html(': '+responseOrderBuyerByUuid.data.orderNo)
                $('.animalNameDetailOrder').html(': '+responseOrderBuyerByUuid.data.animal.name)
                $('.mitraNameDetailOrder').html(': '+responseOrderBuyerByUuid.data.mitra.name)
                $('.alamatMitraDetailOrder').html(': '+responseOrderBuyerByUuid.data.address.address)
                $('.transactionTypeDetailOrder').html(responseOrderBuyerByUuid.data.transactionType == 'SAVING' ? responseOrderBuyerByUuid.data.paymentTerms.confirmed+'/'+responseOrderBuyerByUuid.data.paymentTerms.target+' X '+GlobalJS.convertToRupiah(Math.floor(responseOrderBuyerByUuid.data.subTotal / responseOrderBuyerByUuid.data.paymentTerms.target)) : ': Tunai')
                $('.subTotalDetailOrder').html(': '+GlobalJS.convertToRupiah(Math.floor(responseOrderBuyerByUuid.data.subTotal)))
                $('.detail-pembeli').addClass('is-active');
            })
        }

        function print(index){
            var methodRequest = "GET"
            var urlRequest = "{{ env('HOST_API') }}/api/order/get/"+$('#orderUUid_'+index).val()
            var headerRequest = {
                Authorization: "{{ Session::get('accessToken') }}",
                'Accept-Version': '1.0'
            }
            var dataRequest = ""

            GlobalJS.requestAjaxSync(methodRequest, urlRequest, headerRequest, dataRequest).then(responseOrderBuyerByUuid => {
                $('.dateDetailOrder').html(': '+responseOrderBuyerByUuid.data.dateCreated.substr(0, 11).split("-").join(" "))
                $('.orderNoDetailOrder').html(': '+responseOrderBuyerByUuid.data.orderNo)
                $('.animalNameDetailOrder').html(': '+responseOrderBuyerByUuid.data.animal.name)
                $('.mitraNameDetailOrder').html(': '+responseOrderBuyerByUuid.data.mitra.name)
                $('.alamatMitraDetailOrder').html(': '+responseOrderBuyerByUuid.data.address.address)
                $('.transactionTypeDetailOrder').html(responseOrderBuyerByUuid.data.transactionType == 'SAVING' ? ': Tabungan' : ': Tunai')
                $('.subTotalDetailOrder').html(': '+GlobalJS.convertToRupiah(Math.floor(responseOrderBuyerByUuid.data.subTotal)))

                var divToPrint=document.getElementById("printTable");
                newWin= window.open("");
                newWin.document.write(divToPrint.outerHTML);
                newWin.print();
                newWin.close();
            })
        }

        $('.find-detail').click(function(){
            $('.detail-pembeli').addClass('is-active');
        });
        $('.modal .delete').click(function(){
            $(this).parents('.modal').removeClass('is-active');
            $('html').removeClass('hide-overflow');
        });

        $('.approval a').click(function(){
            var divToPrint=document.getElementById("printTable");
            newWin= window.open("");
            newWin.document.write(divToPrint.outerHTML);
            newWin.print();
            newWin.close();
        });
    </script>
@endsection