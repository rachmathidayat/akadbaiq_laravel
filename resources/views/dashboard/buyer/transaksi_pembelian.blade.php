@extends('dashboard.buyer.layout.index')

@section('content')
    <div class="field">
        <label class="label"></label>
        <div class="control">
            <div class="select">
                <select name="filterTransaksi" onchange="fileterTransaksi(this.value)">
                    <option value="all">Semua Transaksi</option>
                    <option value="NEED_SELLER_CONFIRMATION">Meunggu Persetujuan</option>
                    <option value="NEED_SELLER_CONFIRMATION">Sudah Bayar</option>
                    <option value="NEED_SELLER_CONFIRMATION">Menabung</option>
                    <option value="NEED_SELLER_CONFIRMATION">Dibatalkan</option>
                </select>
            </div>
        </div>
    </div>

    <div id="orderBuyerList">

    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            getOrderBuyerList()
        })

        function getOrderBuyerList(){
            var methodRequest = "GET"
            var urlRequest = "{{ env('HOST_API') }}/api/order/buyer/list?"
            var headerRequest = {
                Authorization: "{{ Session::get('accessToken') }}",
                'Accept-Version': '1.0'
            }
            var dataRequest = ""

            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseOrderBuyerList => {
                console.log(responseOrderBuyerList)
//                nomer invoice = INV/20180605/XVIII/VI/168854789

                var orderBuyerList = $('#orderBuyerList')
                orderBuyerList.html('')
                responseOrderBuyerList.data.map((item, index) => {
                    var disabledButton = ''
                    if(item.orderStatus != 'BUYER_CASH_SUCCESS' && item.orderStatus != 'BUYER_SAVING_SUCCESS'){
                        disabledButton = 'disabled'
                    }
                    orderBuyerList.append(
                        `<div class="box-transaksi shadow-box">
                            <input type="hidden" id="transaksi_`+index+`" value="`+item.uuid+`"/>
                            <div class="info-transaksi">
                                <div class="level">
                                    <div class="level-left">
                                        <div class="level-item tgl-transaksi">
                                            `+item.dateCreated.substr(0, 11).split("-").join(" ")+`
                                        </div>
                                    </div>
                                    <div class="level-right">
                                        <div class="level-item invoice">
                                            `+item.orderNo+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="columns is-desktop align-items detail-transaksi mb0 pb0 is-multiline">
                                <div class="column is-4 is-12-mobile">
                                    <div class="columns is-mobile align-items">
                                        <div class="column">
                                            <div class="img-transaksi">
                                                <img src="{{ asset('assets/img/sapi-1-min.jpg') }}">
                                            </div>
                                        </div>
                                        <div class="column">
                                            <div class="content">
                                                <div class="fw500">`+item.animal.name+`</div>
                                                <div class="small">`+GlobalJS.convertToRupiah(item.orderPrice)+`</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="column is-6-mobile">
                                    <div class="content">
                                        <div class="fw500">Bayar</div>
                                        <div class="small">`+item.transactionType+`</div>
                                    </div>
                                </div>
                                <div class="column is-6-mobile">
                                    <div class="content">
                                        <div class="fw500">Nama Mitra</div>
                                        <div class="small">`+item.mitra.name+`</div>
                                    </div>
                                </div>
                                <div class="column is-6-mobile">
                                    <div class="content">
                                        <div class="fw500">Proses</div>
                                        <div class="small is-text-red">`+item.orderStatus.split("_").join(" ")+`</div>
                                    </div>
                                </div>
                                <div class="column is-6-mobile">
                                    <div class="field">
                                        <a class="button is-fullwidth btn-akadQ findDetail `+disabledButton+`" onclick\=findDetail(`+index+`)\>Detail Mitra</a>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="more-detail">
                                <div class="level">
                                    <div class="level-left">
                                        <div class="level-item">
                                            <table class="table">
                                                <tr>
                                                    <td>
                                                        Pengiriman
                                                    </td>
                                                    <td>
                                                       `+GlobalJS.convertToRupiah(Math.floor(item.shippingPrice))+`
                                                    </td>
                                                </tr>
                                                <tr class="fw600">
                                                    <td>
                                                        Total Harga
                                                    </td>
                                                    <td>
                                                        `+GlobalJS.convertToRupiah(Math.floor(item.subTotal))+`
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="level-right">
                                        <a class="level-item cancel-order" onclick\=\cancelOrder(`+index+`)\>
                                            <svg class="mr0h" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2C6.47,2 2,6.47 2,12C2,17.53 6.47,22 12,22C17.53,22 22,17.53 22,12C22,6.47 17.53,2 12,2M14.59,8L12,10.59L9.41,8L8,9.41L10.59,12L8,14.59L9.41,16L12,13.41L14.59,16L16,14.59L13.41,12L16,9.41L14.59,8Z" fill="#707070"/></svg>
                                            <span>Batalkan Pesanan</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>`
                    )
                })
            })
        }

        function findDetail(transaksiuuid){
            alert($('#transaksi_'+transaksiuuid).val())
            $('.detail-farm').addClass('is-active');
            $('html').addClass('hide-overflow');
        }

        function chooseReasonCancelOrder(param){
            var methodRequest = "POST"
            var urlRequest = "{{ env('HOST_API') }}/api/order/get/"+$('#transaksiUUID').val()+'/reject'
            var headerRequest = {
                Authorization: "{{ Session::get('accessToken') }}",
                'Accept-Version': '1.0'
            }
            var dataRequest = {
                message: param
            }

            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseCancelOrder => {
//                console.log(responseCancelOrder)
                if(responseCancelOrder.success){
                    location.reload()
                }else{
                    alert(responseCancelOrder.message)
                    $('.box-reason').removeClass('is-active');
                    $('html').removeClass('hide-overflow');
                }
            })
        }

        function fileterTransaksi(value){
            var methodRequest = "GET"
            var urlRequest = "{{ env('HOST_API') }}/api/order/buyer/list?"
            var headerRequest = {
                Authorization: "{{ Session::get('accessToken') }}",
                'Accept-Version': '1.0'
            }
            var dataRequest = ""

            GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseOrderBuyerList => {
//                console.log(responseOrderBuyerList)
                if(responseOrderBuyerList.data.length > 0){
                    let filterOrder = responseOrderBuyerList.data.filter((item, index) => {
                        if(value == 'all'){
                            return item
                        }else{
                            return item.orderStatus == value
                        }
                    })

                    var orderBuyerList = $('#orderBuyerList')
                    orderBuyerList.html('')
                    filterOrder.map((item, index) => {
                        var disabledButton = ''
                        if(item.orderStatus != 'BUYER_CASH_SUCCESS' && item.orderStatus != 'BUYER_SAVING_SUCCESS'){
                            disabledButton = 'disabled'
                        }
                        orderBuyerList.append(
                            `<div class="box-transaksi shadow-box">
                                    <input type="hidden" id="transaksi_`+index+`" value="`+item.uuid+`"/>
                                    <div class="info-transaksi">
                                        <div class="level">
                                            <div class="level-left">
                                                <div class="level-item tgl-transaksi">
                                                    `+item.dateCreated.substr(0, 11).split("-").join(" ")+`
                                                </div>
                                            </div>
                                            <div class="level-right">
                                                <div class="level-item invoice">
                                                    `+item.orderNo+`
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="columns is-desktop align-items detail-transaksi mb0 pb0 is-multiline">
                                        <div class="column is-4 is-12-mobile">
                                            <div class="columns is-mobile align-items">
                                                <div class="column">
                                                    <div class="img-transaksi">
                                                        <img src="{{ asset('assets/img/sapi-1-min.jpg') }}">
                                                    </div>
                                                </div>
                                                <div class="column">
                                                    <div class="content">
                                                        <div class="fw500">`+item.animal.name+`</div>
                                                        <div class="small">`+GlobalJS.convertToRupiah(item.orderPrice)+`</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column is-6-mobile">
                                            <div class="content">
                                                <div class="fw500">Bayar</div>
                                                <div class="small">`+item.transactionType+`</div>
                                            </div>
                                        </div>
                                        <div class="column is-6-mobile">
                                            <div class="content">
                                                <div class="fw500">Nama Mitra</div>
                                                <div class="small">`+item.mitra.name+`</div>
                                            </div>
                                        </div>
                                        <div class="column is-6-mobile">
                                            <div class="content">
                                                <div class="fw500">Proses</div>
                                                <div class="small is-text-red">`+item.orderStatus.split("_").join(" ")+`</div>
                                            </div>
                                        </div>
                                        <div class="column is-6-mobile">
                                            <div class="field">
                                                <a class="button is-fullwidth btn-akadQ findDetail `+disabledButton+`" onclick\=findDetail(`+index+`)\>Detail Mitra</a>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="more-detail">
                                        <div class="level">
                                            <div class="level-left">
                                                <div class="level-item">
                                                    <table class="table">
                                                        <tr>
                                                            <td>
                                                                Pengiriman
                                                            </td>
                                                            <td>
                                                               `+GlobalJS.convertToRupiah(Math.floor(item.shippingPrice))+`
                                                            </td>
                                                        </tr>
                                                        <tr class="fw600">
                                                            <td>
                                                                Total Harga
                                                            </td>
                                                            <td>
                                                                `+GlobalJS.convertToRupiah(Math.floor(item.subTotal))+`
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="level-right">
                                                <a class="level-item cancel-order" onclick\=\cancelOrder(`+index+`)\>
                                                    <svg class="mr0h" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2C6.47,2 2,6.47 2,12C2,17.53 6.47,22 12,22C17.53,22 22,17.53 22,12C22,6.47 17.53,2 12,2M14.59,8L12,10.59L9.41,8L8,9.41L10.59,12L8,14.59L9.41,16L12,13.41L14.59,16L16,14.59L13.41,12L16,9.41L14.59,8Z" fill="#707070"/></svg>
                                                    <span>Batalkan Pesanan</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>`
                        )
                    })
                }
            })
        }

        function cancelOrder(transaksiuuid){
            $('#transaksiUUID').val($('#transaksi_'+transaksiuuid).val())
            $('.box-reason').addClass('is-active');
            $('html').addClass('hide-overflow');
        }

        $('.findDetail').click(function(){
            $('.detail-farm').addClass('is-active');
            $('html').addClass('hide-overflow');
        });
        $('.modal .delete').click(function(){
            $('#transaksiUUID').val('')
            $(this).parents('.modal').removeClass('is-active');
            $('html').removeClass('hide-overflow');
        });
        $('.cancel-order').click(function(){
            $('.box-reason').addClass('is-active');
            $('html').addClass('hide-overflow');
        });
    </script>
@endsection