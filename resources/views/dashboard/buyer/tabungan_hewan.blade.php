@extends('dashboard.buyer.layout.index')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/swiper.css') }}">
@endsection

@section('content')
@include('dashboard.buyer.layout.modal_detail_informasi')

<div class="content has-text-left">
    <div class="title">
        Tabungan Hewan
    </div>
    <div class="columns is-multiline is-desktop">
        <div class="column is-7">
            <p class="subtitle is-size-6 fw300">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
    </div>
</div>
<!-- table -->
<div class="columns is-desktop is-multiline listTabunganHewan">

</div>
@endsection

@section('script')
<script src="{{ asset('assets/js/swiper.min.js') }}"></script>
<script>
    $(document).ready(function(){
        getTabunganHewan()
    })

    function getTabunganHewan(){
        var methodRequest = "GET"
        var urlRequest = "{{ env('HOST_API') }}/api/order/buyer/list?"
        var headerRequest = {
            Authorization: "{{ Session::get('accessToken') }}",
            'Accept-Version': '1.0'
        }
        var dataRequest = ""

        GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseOrderBuyerList => {
            let statusSavingSuccess = responseOrderBuyerList.data.filter((item, index) => {
                if(item.orderStatus == 'BUYER_SAVING_SUCCESS'){
                    return item
                }
            })

//            console.log(statusSavingSuccess)
            var listTabunganHewan = $('.listTabunganHewan')
            statusSavingSuccess.map((item, index) => {
                listTabunganHewan.append(
                    `<div class="column is-6 is-12-mobile">
                        <div class="box-transaksi shadow-box">
                            <div class="info-transaksi">
                                <div class="level">
                                    <div class="level-left">
                                        <div class="level-item tgl-transaksi">
                                            `+item.dateCreated.substr(0, 11).split("-").join(" ")+`
                                        </div>
                                    </div>
                                    <div class="level-right">
                                        <div class="level-item invoice">
                                            `+item.orderNo+`
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="columns is-multiline is-mobile align-items detail-transaksi mb0 pb0">
                                <div class="column is-7">
                                    <div class="columns is-multiline is-mobile align-items">
                                        <div class="column">
                                            <div class="img-transaksi">
                                                <img src="{{ asset('assets/img/sapi-1-min.jpg') }}">
                                            </div>
                                        </div>
                                        <div class="column">
                                            <div class="content">
                                                <div class="fw500">`+item.animal.name+`</div>
                                                <div class="small">`+item.mitra.name+`</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="column">
                                    <div class="content">
                                        <div class="fw500">Menabung</div>
                                        <div class="small">`+item.paymentTerms.confirmed+`/`+item.paymentTerms.target+` X `+GlobalJS.convertToRupiah(Math.floor(item.subTotal / item.paymentTerms.target))+`</div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="more-detail">
                                <div class="columns is-multiline is-mobile">
                                    <div class="column">
                                        <a class="button is-fullwidth btn-akadQ informasiClick" onclick\=\detailHewan(`+index+`)\>Detail Hewan</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`
                )
            })
        })
    }

    function detailHewan(index){
        $('.detail-informasi').addClass('is-active')
        $('html').addClass('hide-overflow')
        var swiper3 = new Swiper('.swiper3', {
            slidesPerView: 1,
            loop: true,
            autoplay: true,
            pagination: {
                el: '.swiper-pagination.pag3',
                clickable: true,
            },
        });
    }

    $('.modal .delete').click(function(){
        $(this).parents('.modal').removeClass('is-active')
        $('html').removeClass('hide-overflow')
    })

    $('ul.tabbing li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.tabbing li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    });

    $('.informasiClick').click(function(){
        $('.detail-informasi').addClass('is-active')
        $('html').addClass('hide-overflow')
        var swiper3 = new Swiper('.swiper3', {
            slidesPerView: 1,
            loop: true,
            autoplay: true,
            pagination: {
                el: '.swiper-pagination.pag3',
                clickable: true,
            },
        });
    });
</script>
@endsection