@extends('dashboard.buyer.layout.index')

@section('content')
    <div class="modal modal-left boxDelivered">
        <div class="modal-background"></div>
        <div class="modal-card">
            <header class="modal-card-head">
                <p class="modal-card-title is-size-6 fw600">Yakin hewan Anda sudah sampai?</p>
                <button class="delete del-reason" aria-label="close"></button>
            </header>
            <section class="modal-card-body">
                <ul class="reason">
                    <li id="yesDel">Ya</li>
                    <li id="noDel">Tidak</li>
                </ul>
            </section>
        </div>
    </div>

    <div class="tabs tabsku is-boxed mb0">
        <ul>
            <li class="is-active" id="1">
                <a>
                    <span>Status Pembayaran</span>
                </a>
            </li>
            <li id="2">
                <a>
                    <span>Status Pemesanan</span>
                </a>
            </li>
            <li id="3">
                <a>
                    <span>Konfirmasi Penerimaan</span>
                </a>
            </li>
        </ul>
    </div>
    <!-- table -->
    <div class="content-bayar">
        <div class="content-1 table-responsive">
            <table class="table table-riwayat is-striped is-fullwidth">
                <thead>
                <tr>
                    <th>Detail</th>
                    <th>Mitra</th>
                    <th>Komoditas</th>
                    <th>Transaksi</th>
                    <th>Bayar</th>
                    <th>Harga</th>
                    <th>Bayar</th>
                </tr>
                </thead>
                <tbody id="tblStatusPembayaran">
                {{--<tr>--}}
                    {{--<td>--}}
                        {{--<div class="find-detail is-pulled-left">--}}
                            {{--<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>--}}
                        {{--</div>--}}
                    {{--</td>--}}
                    {{--<td>UD BAHARUDIN MS</td>--}}
                    {{--<td>Sapi Limosin</td>--}}
                    {{--<td>12/05/2018</td>--}}
                    {{--<td>1/6</td>--}}
                    {{--<td>Rp 5.000.000</td>--}}
                    {{--<td class="approval">--}}
                        {{--<a href="{{ url('dashboard/buyer/cara-pembayaran') }}" class="button btn-akadQ w-100">Cara Bayar</a>--}}
                    {{--</td>--}}
                {{--</tr>--}}
                </tbody>
            </table>
        </div>
        <div class="content-2 table-responsive none">
            <table class="table table-riwayat is-striped is-fullwidth">
                <thead>
                <tr>
                    <th>Detail</th>
                    <th>Mitra</th>
                    <th>Komoditas</th>
                    <th>Tanggal</th>
                    <th>Bayar</th>
                    <th>Harga</th>
                    <th>Invoice</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    {{--<td>--}}
                        {{--<div class="find-detail is-pulled-left">--}}
                            {{--<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>--}}
                        {{--</div>--}}
                    {{--</td>--}}
                    {{--<td>UD BAHARUDIN MS</td>--}}
                    {{--<td>Sapi Limosin</td>--}}
                    {{--<td>13/05/2018</td>--}}
                    {{--<td>1/6</td>--}}
                    {{--<td>Rp 5.000.000</td>--}}
                    {{--<td class="approval">--}}
                        {{--<a href="#" class="button btn-akadQ w-100">Print</a>--}}
                    {{--</td>--}}
                </tr>
                </tbody>
            </table>
        </div>
        <div class="content-3 table-responsive none">
            <table class="table table-riwayat is-striped is-fullwidth">
                <thead>
                <tr>
                    <th>Detail</th>
                    <th>Mitra</th>
                    <th>Komoditas</th>
                    <th>Tanggal</th>
                    <th>Bayar</th>
                    <th>Harga</th>
                    <th>Konfirmasi</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    {{--<td>--}}
                        {{--<div class="find-detail is-pulled-left">--}}
                            {{--<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>--}}
                        {{--</div>--}}
                    {{--</td>--}}
                    {{--<td>UD BAHARUDIN MS</td>--}}
                    {{--<td>Sapi Limosin</td>--}}
                    {{--<td>12/05/2018</td>--}}
                    {{--<td>1/6</td>--}}
                    {{--<td>Rp 5.000.000</td>--}}
                    {{--<td class="approval">--}}
                        {{--<a href="#" class="button btn-akadQ w-100 isDelivered">Telah Sampai</a>--}}
                    {{--</td>--}}
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        getStatusPembayaran()
    })

    function getStatusPembayaran(){
        var methodRequest = "GET"
        var urlRequest = "{{ env('HOST_API') }}/api/order/buyer/list?"
        var headerRequest = {
            Authorization: "{{ Session::get('accessToken') }}",
            'Accept-Version': '1.0'
        }
        var dataRequest = ""

        GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseOrderBuyerList => {
            let statusPembayaran = responseOrderBuyerList.data.filter((item, index) => {
                if(item.orderStatus == 'NEED_BUYER_PAYMENT' || item.orderStatus == "NEED_SELLER_CONFIRMATION"){
                    return item
                }
            })
//            console.log(statusPembayaran)
            var tblStatusPembayaran = $('#tblStatusPembayaran')
            statusPembayaran.map((item, index) => {
                tblStatusPembayaran.append(
                    `<tr>
                        <td>
                            <div class="find-detail is-pulled-left">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="20" height="20" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"></path></svg>
                            </div>
                        </td>
                        <td>`+item.mitra.name+`</td>
                        <td>`+item.animal.name+`</td>
                        <td>`+item.dateCreated.substr(0, 11).split("-").join(" ")+`</td>
                        <td>`+item.paymentTerms.confirmed+'/'+item.paymentTerms.target+`</td>
                        <td>`+GlobalJS.convertToRupiah(Math.floor(item.subTotal/item.paymentTerms.target))+`</td>
                        <td class="approval">
                            <a href="{{ url('dashboard/buyer/cara-pembayaran') }}" class="button btn-akadQ w-100">Cara Bayar</a>
                        </td>
                    </tr>`
                )
            })
        })
    }

    $('.isDelivered').click(function(){
        $('.boxDelivered').addClass('is-active')
        $('#yesDel').click(function(){
            window.location="buyer-ulasan.html";
        })
        $('#noDel').click(function(){
            $('.boxDelivered').removeClass('is-active')
        })
    })
    $('#2').click(function(){
        $('.content-2').addClass('block').removeClass('none')
        $('.content-1').addClass('none').removeClass('block')
        $('.content-3').addClass('none').removeClass('block')
        $('#2').addClass('is-active')
        $('#1').removeClass('is-active')
        $('#3').removeClass('is-active')
    })
    $('#1').click(function(){
        $('.content-2').addClass('none').removeClass('block')
        $('.content-1').addClass('block').removeClass('none')
        $('.content-3').addClass('none').removeClass('block')
        $('#2').removeClass('is-active')
        $('#1').addClass('is-active')
        $('#3').removeClass('is-active')
    })
    $('#3').click(function(){
        $('.content-2').addClass('none').removeClass('block')
        $('.content-1').addClass('none').removeClass('block')
        $('.content-3').addClass('block').removeClass('none')
        $('#2').removeClass('is-active')
        $('#1').removeClass('is-active')
        $('#3').addClass('is-active')
    })
    $('.findDetail').click(function(){
        $('.detail-farm').addClass('is-active');
        $('html').addClass('hide-overflow');
    });
    $('.modal .delete').click(function(){
        $(this).parents('.modal').removeClass('is-active');
        $('html').removeClass('hide-overflow');
    });
    $('.cancel-order').click(function(){
        $('.box-reason').addClass('is-active');
        $('html').addClass('hide-overflow');
    });
</script>
@endsection