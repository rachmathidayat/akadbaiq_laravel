@extends('dashboard.buyer.layout.index')

@section('content')
    <div class="columns col-netral is-centered">
        <div class="column is-8">
            <div class="box-time shadow-box has-text-centered">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="100" height="100" viewBox="0 0 24 24"><path d="M12,20A7,7 0 0,1 5,13A7,7 0 0,1 12,6A7,7 0 0,1 19,13A7,7 0 0,1 12,20M19.03,7.39L20.45,5.97C20,5.46 19.55,5 19.04,4.56L17.62,6C16.07,4.74 14.12,4 12,4A9,9 0 0,0 3,13A9,9 0 0,0 12,22C17,22 21,17.97 21,13C21,10.88 20.26,8.93 19.03,7.39M11,14H13V8H11M15,1H9V3H15V1Z" fill="#DFDFDF"/></svg>
                <div class="content">
                    <div class="title"> Waktu pembayaran Anda</div>
                    <div id="countdown" class="is-text-blue is-size-3 fw400"></div>
                    <p class="subtitle is-size-6 fw300 mt1">Mohon menyelesaikan pembayaran sebelum batas waktu</p>
                </div>
            </div>
            <div class="box-bayar shadow-box mt2">
                <div class="level has-border-grey">
                    <div class="level-left">
                        <div class="level-item is-size-5 fw500">Jumlah yang harus dibayar</div>
                    </div>
                    <div class="level-right">
                        <div class="level-item is-text-blue is-size-5 fw500">Rp 30.700.000</div>
                    </div>
                </div>
                <div class="columns col-netral">
                    <div class="column">
                        <img src="{{ asset('assets/img/bca.png') }}">
                    </div>
                    <div class="column">
                        <div class="has-text-right">BCA Kode pembayaran</div>
                        <div class="fw600 is-size-5 has-text-right">863999474689</div>
                    </div>
                </div>
            </div>
            <div class="box-panduan mt2">
                <div class="is-size-5 fw500">Panduan Pembayaran</div>
                <button class="accordion">ATM</button>
                <div class="panels">
                    <ol class="showList">
                        <li>Masukkan kartu ATM BCA &amp; PIN</li>
                        <li>Pilih ‘Transaksi Lainnya</li>
                        <li>Pilih ‘Transfer’</li>
                        <li>Pilih ‘ke Rekening BCA Virtual Account’</li>
                        <li>Masukkan nomor BCA Virtual Account</li>
                        <li>Masukkan jumlah yang ingin dibayarkan</li>
                        <li>Validasi pembayaran Anda</li>
                        <li>Pembayaran selesai</li>
                    </ol>
                </div>
                <button class="accordion">Internet Banking (App &amp; Web)</button>
                <div class="panels">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
                <button class="accordion">Mobile Banking</button>
                <div class="panels">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
                <button class="accordion">Indomaret</button>
                <div class="panels">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        // Set the date we're counting down to
        var countDownDate = new Date("Jul 24, 2018 15:00:00").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now an the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Output the result in an element with id="demo"
            document.getElementById("countdown").innerHTML = days + "d " + hours + "h "
                + minutes + "m " + seconds + "s ";

            // If the count down is over, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("countdown").innerHTML = "EXPIRED";
            }
        }, 250);

        // accordion
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.maxHeight){
                    panel.style.maxHeight = null;
                } else {
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            });
        }
    </script>
@endsection