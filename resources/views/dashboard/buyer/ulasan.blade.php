@extends('dashboard.buyer.layout.index')

@section('content')
    <div class="box-transaksi shadow-box">
        <div class="info-transaksi">
            <div class="level">
                <div class="level-left">
                    <div class="level-item tgl-transaksi">
                        05 Juni 2018
                    </div>
                </div>
                <div class="level-right">
                    <div class="level-item invoice">
                        INV/20180605/XVIII/VI/168854789
                    </div>
                </div>
            </div>
        </div>
        <div class="columns align-items detail-transaksi mb0 pb0 is-desktop is-multiline">
            <div class="column is-4 is-12-mobile">
                <div class="columns align-items">
                    <div class="column">
                        <div class="img-transaksi">
                            <img src="{{ asset('assets/img/sapi-1-min.jpg') }}">
                        </div>
                    </div>
                    <div class="column">
                        <div class="content">
                            <div class="fw500">Sapi Bali Super</div>
                            <div class="small">Rp. 30.000.000</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column is-2">
                <div class="content">
                    <div class="fw500">Nama Mitra</div>
                    <div class="small">UD BAHARUDIN MS</div>
                </div>
            </div>
            <div class="column">
                <div class="field has-text-right">
                    <a class="button btn-akadQ w-250 click-Comment">Buat Ulasan</a>
                </div>
            </div>
        </div>
        <div class="ulasan none">
            <hr>
            <div>Beri Rating</div>
            <form class="rating">
                <label>
                    <input type="radio" name="stars" value="2" />
                    <span class="icon" data-rate="1">★</span>
                    <span class="icon" data-rate="2">★</span>
                    <span class="icon" data-rate="3">★</span>
                    <span class="icon" data-rate="4">★</span>
                    <span class="icon" data-rate="5">★</span>
                </label>
            </form>
            <div class="mb0h">Isi Ulasan</div>
            <!-- Wrapper Start -->
            <div class="field">
                <div class="control">
                    <textarea class="textarea is-info" type="text" placeholder="Isi ulasan Anda disini"></textarea>
                </div>
            </div>
            <div class="field has-text-right">
                <a class="button btn-akadQ w-100">Submit</a>
            </div>
            <!-- Wrapper End -->
        </div>
    </div>
@endsection

@section('script')
<script>
    $('.click-Comment').click(function(){
        $('.ulasan').slideToggle()
    })
    // rating
    $(document).on('click', '.icon', function() {
        $('input[name=stars]').val($(this).data('rate'));
        var num = 1,iconStar = $(this);
        $('.icon').each(function() {
            if (num <= iconStar.data('rate')) {
                $(this).css('color', '#FFC60B');
            }else{
                $(this).css('color', '#ccc');
            }
            num++;
        });
    });

    var num = 3,iconStar = $('input[name=stars]').val();
    $('.icon').each(function() {
        if (num <= iconStar) {
            $(this).css('color', '#FFC60B');
        }else{
            $(this).css('color', '#ccc');
        }
        num++;
    });
</script>
@endsection