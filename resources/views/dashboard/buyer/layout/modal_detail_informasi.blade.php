<div class="modal modal-left detail-informasi">
    <div class="modal-background"></div>
    <div class="modal-card">

        <section class="modal-card-body">
            <button class="delete del-informasi" aria-label="close"></button>
            <ul class="tabbing">
                <li class="tab-link current" data-tab="tab-1">Bulan 1</li>
                <li class="tab-link" data-tab="tab-2">Bulan 2</li>
                <li class="tab-link" data-tab="tab-3">Bulan 3</li>
            </ul>

            <div id="tab-1" class="tab-content current">
                <div class="columns is-multiline is-mobile">
                    <div class="column is-5">
                        <div class="swiper-container swiper3">
                            <!-- banner slider -->
                            <div class="swiper-wrapper">
                                <div class="swiper-slide"><img src="{{ asset('assets/img/sapi-1-min.jpg') }}"></div>
                                <div class="swiper-slide"><img src="{{ asset('assets/img/sapi-2-min.jpg') }}"></div>
                                <div class="swiper-slide"><img src="{{ asset('assets/img/sapi-3-min.jpg') }}"></div>
                            </div>

                        </div>
                        <!-- pagination -->
                        <div class="swiper-pagination pag3"></div>
                    </div>
                    <div class="column">
                        <div class="content desc-ternak">
                            <div class="fw500">Sapi Bali Super</div>
                            <table class="table mt1 is-fullwidth is-striped">
                                <tbody>
                                <tr>
                                    <td>Umur</td>
                                    <td>: &nbsp;5 Bulan</td>
                                </tr>
                                <tr>
                                    <td>Panjang</td>
                                    <td>: &nbsp;150cm</td>
                                </tr>
                                <tr>
                                    <td>Tinggi</td>
                                    <td>: &nbsp;110cm</td>
                                </tr>
                                <tr>
                                    <td>Lingkar Dada</td>
                                    <td>: &nbsp;260cm</td>
                                </tr>
                                <tr>
                                    <td>Berat</td>
                                    <td>: &nbsp;90kg</td>
                                </tr>
                                <tr>
                                    <td>Tenor</td>
                                    <td>: &nbsp;1/6</td>
                                </tr>
                                <tr>
                                    <td>Pembayaran</td>
                                    <td>: &nbsp;Rp 3.000.000</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="level">
                    <div class="level-left">
                        <div class="level-item">
                            <div class="last-update">
                                Update gambar : 5 Juni 2018
                            </div>
                        </div>
                    </div>
                    <div class="level-right">
                        <div class="level-item">
                            <div class="last-update">
                                Update konten : 5 Juni 2018
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab-2" class="tab-content">
                <div class="content has-text-centered">
                    <div class="title is-size-5">
                        Update hewan belum tersedia
                    </div>
                    <p class="subtitle is-size-6 fw300">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <img src="{{ asset('assets/img/icon-hewan.png') }}" width="150">
                    <!-- button -->
                    <div class="field mt1">
                        <a class="button btn-akadQ w-250 mt1">Ajukan update ke Mitra</a>
                    </div>
                </div>
            </div>
            <div id="tab-3" class="tab-content">
                <div class="content has-text-centered">
                    <div class="title is-size-5">
                        Update Hewan Tabungan
                    </div>
                    <p class="subtitle is-size-6 fw300">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <img src="{{ asset('assets/img/icon-hewan.png') }}" width="150">
                    <!-- button -->
                    <div class="field mt1">
                        <a class="button btn-akadQ w-250 mt1">Ajukan update ke Mitra</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>