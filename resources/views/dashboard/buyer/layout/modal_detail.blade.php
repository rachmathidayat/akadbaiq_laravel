<div class="modal modal-left detail-farm">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title is-size-6 fw600">Detail Mitra</p>
            <button class="delete del-detail" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <div class="columns">
                <div class="column is-5">
                    <div class="img-ternak-detail">
                        <img src="{{ asset('assets/img/banner-1.jpg') }}">
                    </div>
                </div>
                <div class="column relative">
                    <div class="columns">
                        <div class="column">
                            <div class="content desc-ternak">
                                <div class="fw500">Bonsle Farm.UD</div>
                                <div class="small">Depok, Jawa Barat</div>
                            </div>
                            <div class="content desc-ternak">
                                <div class="fw500">Penanggung Jawab</div>
                                <div class="small">Dadan Suramdan</div>
                            </div>
                        </div>
                        <div class="column">
                            <div class="content desc-ternak">
                                <div class="fw500">No Telp/HP</div>
                                <div class="small">081193337511</div>
                            </div>
                            <div class="content desc-ternak">
                                <div class="fw500">Email</div>
                                <div class="small">dadan.suramdan@gmail.com</div>
                            </div>
                        </div>
                    </div>
                    <div class="field has-addons action-button two">
                        <p class="control">
                            <a class="button is-fullwidth edit-farm">
                  <span class="icon mr0h">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="M4,4H20A2,2 0 0,1 22,6V18A2,2 0 0,1 20,20H4C2.89,20 2,19.1 2,18V6C2,4.89 2.89,4 4,4M12,11L20,6H4L12,11M4,18H20V8.37L12,13.36L4,8.37V18Z" /></svg>
                  </span>
                                <span>Kirim Pesan</span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>