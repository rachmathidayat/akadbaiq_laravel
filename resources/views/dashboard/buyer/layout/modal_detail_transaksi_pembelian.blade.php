<div class="modal modal-left detail-pembeli">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title is-size-6 fw600">Detail Transaksi Pembelian</p>
            <button class="delete del-detail" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <div class="content desc-ternak">
                <div class="columns col-netral">
                    <div class="column is-5">
                        <div class="img-ternak-detail">
                            <img src="{{ asset('assets/img/banner-1.jpg') }}">
                        </div>
                    </div>
                    <div class="column">
                        <table class="table is-fullwidth is-striped" id="printTable">
                            <tbody>
                            <tr>
                                <td>Transaksi</td>
                                <td class="fw500 dateDetailOrder">: &nbsp;12/05/18</td>
                            </tr>
                            <tr>
                                <td>Nomor Invoice</td>
                                <td class="fw500 orderNoDetailOrder">: &nbsp;INV/20180605/XVIII/VI/168854789</td>
                            </tr>
                            <tr>
                                <td>Komoditas</td>
                                <td class="fw500 animalNameDetailOrder">: &nbsp;Kambing Kacang</td>
                            </tr>
                            <tr>
                                <td>Mitra</td>
                                <td class="fw500 mitraNameDetailOrder">: &nbsp;UD BAHARUDIN MS</td>
                            </tr>
                            <tr>
                                <td>Lokasi</td>
                                <td class="fw500 alamatMitraDetailOrder">: &nbsp;Depok, Jawa Barat</td>
                            </tr>
                            <tr>
                                <td>Bayar</td>
                                <td class="fw500 transactionTypeDetailOrder">: &nbsp;1/6</td>
                            </tr>
                            <tr>
                                <td>Harga</td>
                                <td class="fw500 subTotalDetailOrder">: &nbsp;Rp 5.000.000</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>