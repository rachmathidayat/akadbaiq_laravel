<div class="column for-left is-narrow pb0 is-hidden-mobile">
    <div class="left-menu is-back-grey">
        <div class="brand-icon has-text-centered">
            <a href="{{ url('/') }}"><img src="{{ asset('assets/img/logo.png') }}" alt="Logo" width="70%"></a>
        </div>
        <aside class="menu">
            <ul class="menu-list">
                <li class="{{ Request::is('dashboard/buyer/transaksi-pembelian') ? 'is-active' : '' }}"><a href="{{ url('dashboard/buyer/transaksi-pembelian') }}">Transaksi Pembelian</a></li>
                <li class="{{ Request::is('dashboard/buyer/riwayat-pembayaran') ? 'is-active' : '' }}"><a href="{{ url('dashboard/buyer/riwayat-pembayaran') }}">Riwayat Pembayaran</a></li>
                <li class="{{ Request::is('dashboard/buyer/tabungan-hewan') ? 'is-active' : '' }}"><a href="{{ url('dashboard/buyer/tabungan-hewan') }}">Tabungan Hewan</a></li>
                <li class="{{ Request::is('dashboard/buyer/status-pembayaran') || Request::is('dashboard/buyer/cara-pembayaran') ? 'is-active' : '' }}"><a href="{{ url('dashboard/buyer/status-pembayaran') }}">Status Pembayaran</a></li>
                <li class="{{ Request::is('dashboard/buyer/ulasan') ? 'is-active' : '' }}"><a href="{{ url('dashboard/buyer/ulasan') }}">Ulasan</a></li>
            </ul>
        </aside>
    </div>
</div>