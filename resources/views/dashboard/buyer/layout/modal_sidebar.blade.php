<div class="modal side-bar">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title has-text-left">Menu</p>
            <a class="close-modal">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" fill="#fff"/></svg>
            </a>
        </header>
        <section class="modal-card-body is-paddingless list-bar">
            <div class="media flex">
                <div class="media-content align-self">
                    <div class="content is-marginless has-text-left">
                        <a href="{{ url('dashboard/buyer/transaksi-pembelian') }}"><h3 class="subtitle is-6 is-marginless is-text-blue">Transaksi Pembelian</h3></a>
                    </div>
                </div>
            </div>
            <div class="media flex">
                <div class="media-content align-self">
                    <div class="content is-marginless has-text-left">
                        <a href="{{ url('dashboard/buyer/riwayat-pembayaran') }}">
                            <h3 class="subtitle is-6 is-marginless is-text-blue">Riwayat Pembayaran</h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="media flex">
                <div class="media-content align-self">
                    <div class="content is-marginless has-text-left">
                        <a href="{{ url('dashboard/buyer/tabungan-hewan') }}">
                            <h3 class="subtitle is-6 is-marginless is-text-blue">Tabungan Hewan</h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="media flex">
                <div class="media-content align-self">
                    <div class="content is-marginless has-text-left">
                        <a href="{{ url('dashboard/buyer/status-pembayaran') }}">
                            <h3 class="subtitle is-6 is-marginless is-text-blue">Status Pembayaran</h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="media flex">
                <div class="media-content align-self">
                    <div class="content is-marginless has-text-left">
                        <a href="{{ url('dashboard/buyer/ulasan') }}">
                            <h3 class="subtitle is-6 is-marginless is-text-blue">Ulasan</h3>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>