<!DOCTYPE html>
<html>
<head>
    @include('dashboard.buyer.layout.header')
    @yield('style')
</head>
<body>

@include('dashboard.buyer.layout.modal_sidebar')

@include('dashboard.buyer.layout.modal_detail')

@include('dashboard.buyer.layout.modal_reason')

<section class="hero">
    <div class="hero-head relative shadow-bottom is-hidden-desktop">
        <nav class="navbar">
            <div class="container">
                <div class="navbar-brand">
                    <a href="{{ url('/') }}" class="navbar-item">
                        <img src="{{ asset('assets/img/logo.png') }}" alt="Logo">
                    </a>
                    <span class="navbar-burger burger">
              <span></span>
              <span></span>
              <span></span>
            </span>
                </div>
            </div>
        </nav>
    </div>
    <div class="columns is-mobile">

        @include('dashboard.buyer.layout.left_menu_desktop')

        <div class="column for-right">
            <div class="right-menu">

                @include('dashboard.buyer.layout.navbar')

                <!-- content -->
                @yield('content')

            </div>
        </div>
    </div>
</section>
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/global.js') }}"></script>
@yield('script')
<script>
    const GlobalJS = new Global()

    $(document).ready(function(){
        getMessage()
        getNotif()
    })

    function getMessage(){
        var methodRequest = "GET"
        var urlRequest = "{{ env('HOST_API') }}/api/inbox"
        var headerRequest = {
            Authorization: "{{ Session::get('accessToken') }}",
            'Accept-Version': '1.0'
        }
        var dataRequest = ""

        GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseGetMessage => {
            $('.countMessage').html(responseGetMessage.data.length)
        })
    }

    function getNotif(){
        var methodRequest = "GET"
        var urlRequest = "{{ env('HOST_API') }}/api/order/buyer/list?"
        var headerRequest = {
            Authorization: "{{ Session::get('accessToken') }}",
            'Accept-Version': '1.0'
        }
        var dataRequest = ""

        GlobalJS.requestAjax(methodRequest, urlRequest, headerRequest, dataRequest).then(responseGetNotif => {
            $('.countNotification').html(responseGetNotif.data.length)
        })
    }

    // sidebar
    function noscroll(){
        $('html').addClass('hide-overflow')
    }
    function scroll(){
        $('html').removeClass('hide-overflow')
    }

    $('.burger').click(function(){
        $('.side-bar').addClass('is-active')
        noscroll()
    })
    $(".modal-background, .close-modal").click(function(){
        $(this).parents('.modal').removeClass('is-active')
        scroll()
    })
    function profile(){
        window.location.href = "/profile/{{ Session::get('userUUID') }}"
    }
    function pembelian(){
        window.location.assign("{{ url('dashboard/buyer/transaksi-pembelian') }}")
    }
    function logout(){
        $.ajax({
            method: "POST",
            url: "{{ url('/auth/logout') }}",
            headers: "",
            data: "",
            success: function (response) {
                localStorage.removeItem('aq_login_status');
                localStorage.removeItem('aq_mitra_status');
                location.reload(true);
            }
        })
    }
    // for inbox
    $('.for-inbox').on('click', function(){
        $('.menu-inbox').toggle()
        $('.menu-notif').css('display', 'none')
        $('.menu-akun-inside').css('display', 'none')
    });
    $('.for-notif').on('click', function(){
        $('.menu-notif').toggle()
        $('.menu-inbox').css('display', 'none')
        $('.menu-akun-inside').css('display', 'none')
    });
    $('.for-akun').on('click', function(){
        $('.menu-akun-inside').toggle()
        $('.menu-notif').css('display', 'none')
        $('.menu-inbox').css('display', 'none')
    });
</script>
</body>
</html>