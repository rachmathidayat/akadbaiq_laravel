<div class="modal modal-left box-reason">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title is-size-6 fw600">Alasan kenapa dihapus?</p>
            <button class="delete del-reason" aria-label="close"></button>
        </header>
        <input type="hidden" id="transaksiUUID" />
        <section class="modal-card-body">
            <ul class="reason">
                <li onclick="chooseReasonCancelOrder('Ingin ganti hewan')">Ingin ganti hewan</li>
                <li onclick="chooseReasonCancelOrder('Lokasi mitra jauh')">Lokasi mitra jauh</li>
            </ul>
        </section>
    </div>
</div>