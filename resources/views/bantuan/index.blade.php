<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title>AkadBaiQ</title>
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bulma.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
  <link rel="shortcut icon" type="image/png" href="img/favicon.png"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
</head>
<body>
  <div class="modal side-bar">
    <div class="modal-background"></div>
    <div class="modal-card">
      <header class="modal-card-head">
        <p class="modal-card-title has-text-left">Menu</p>
        <a class="close-modal">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" fill="#fff"/></svg>
        </a>
      </header>
      <section class="modal-card-body is-paddingless list-bar">
        <div class="media flex">
          <div class="media-content align-self">
            <div class="content is-marginless has-text-left">
              <h3 class="subtitle is-6 is-marginless is-text-blue">Tentang Akadbaiq</h3>
            </div>
          </div>
        </div>
        <div class="media flex">
          <div class="media-content align-self">
            <div class="content is-marginless has-text-left">
              <h3 class="subtitle is-6 is-marginless is-text-blue">Akun</h3>
            </div>
          </div>
        </div>
        <div class="media flex">
          <div class="media-content align-self">
            <div class="content is-marginless has-text-left">
              <h3 class="subtitle is-6 is-marginless is-text-blue">Sebagai Mitra</h3>
            </div>
          </div>
        </div>
        <div class="media flex">
          <div class="media-content align-self">
            <div class="content is-marginless has-text-left">
              <h3 class="subtitle is-6 is-marginless is-text-blue">Sebagai Pembeli</h3>
            </div>
          </div>
        </div>
        <div class="media flex">
          <div class="media-content align-self">
            <div class="content is-marginless has-text-left">
              <h3 class="subtitle is-6 is-marginless is-text-blue">Aqiqah</h3>
            </div>
          </div>
        </div>
        <div class="media flex">
          <div class="media-content align-self">
            <div class="content is-marginless has-text-left">
              <h3 class="subtitle is-6 is-marginless is-text-blue">Qurban</h3>
            </div>
          </div>
        </div>
        <div class="media flex">
          <div class="media-content align-self">
            <div class="content is-marginless has-text-left">
              <h3 class="subtitle is-6 is-marginless is-text-blue">Saldo Tabungan</h3>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
  <section class="hero">
    <div class="hero-head relative shadow-bottom is-hidden-desktop">
      <nav class="navbar">
        <div class="container">
          <div class="navbar-brand">
            <a href="index.html" class="navbar-item">
              <img src="{{ asset('assets/img/logo.png') }}" alt="Logo">
            </a>
            <span class="navbar-burger burger">
              <span></span>
              <span></span>
              <span></span>
            </span>
          </div>
        </div>
      </nav>
    </div>
    <div class="columns col-netral">
      <div class="column for-left is-narrow pb0 is-hidden-mobile">
        <div class="left-menu is-back-grey">
          <div class="brand-icon has-text-centered">
            <a href="index.html"><img src="{{ asset('assets/img/logo.png') }}" alt="Logo" width="70%"></a>
          </div>
          <aside class="menu">
            <ul class="menu-list">
              <li><a href="#">Tentang Akadbaiq</a></li>
              <li class="is-active"><a href="#">Akun</a></li>
              <li><a href="#">Sebagai Mitra</a></li>
              <li><a href="#">Sebagai Pembeli</a></li>
              <li><a href="#">Aqiqah</a></li>
              <li><a href="#">Qurban</a></li>
              <li><a href="#">Saldo Tabungan </a></li>
            </ul>
          </aside>
        </div>
      </div>
      <div class="column for-right">
        <div class="right-menu">
          <!-- content -->
          <div class="content mt4">
            <div class="title">
              Bantuan Pemakaian AkadbaiQ
            </div>
            <p class="subtitle is-size-6 fw300">Untuk mempermudah pengguna aplikasi AkadbaiQ dalam mempelajari product kami.</p>
            <!-- <div class="field has-addons">
              <div class="control">
                <a class="button is-info">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" fill="#fff"/></svg>
                </a>
              </div>
              <div class="control">
                <input class="input helps w-450" type="text" placeholder="Find a repository">
              </div>
            </div> -->
            <!-- ask -->
            <div class="list-ask mt2">
              <button class="accordion">Riwayat Pembayaran</button>
              <div class="panels">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
              <button class="accordion">Verifikasi</button>
              <div class="panels">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
              <button class="accordion">Tenor pembayaran</button>
              <div class="panels">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
              <button class="accordion">Permintaan Mitra</button>
              <div class="panels">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
              <button class="accordion">Daftar Masuk</button>
              <div class="panels">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
  <script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight){
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        } 
      });
    }

    // sidebar
    function noscroll(){
      $('html').addClass('hide-overflow')
    }
    function scroll(){
      $('html').removeClass('hide-overflow')
    }
    
    $('.burger').click(function(){
      $('.side-bar').addClass('is-active')
      noscroll()
    })
    $(".modal-background, .close-modal").click(function(){
      $(this).parents('.modal').removeClass('is-active')
      scroll()
    })
  </script>
</body>
</html>