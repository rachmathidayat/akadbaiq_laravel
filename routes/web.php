<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'index_landing_page', 'uses' => 'LandingPage\LandingPageController@landing_page']);
Route::get('/pilih-program', ['middleware' => 'CheckLogin', 'as' => 'pilih_program', 'uses' => 'LandingPage\LandingPageController@pilih_program']);
Route::get('/contact-us', ['as' => 'contactus', 'uses' => 'LandingPage\LandingPageController@contactus']);
Route::get('/profile/{user_uuid}', ['middleware' => 'CheckLogin', 'as' => 'profile', 'uses' => 'LandingPage\LandingPageController@profile']);
Route::get('/profile/update/{user_uuid}', ['middleware' => 'CheckLogin', 'as' => 'profile_update', 'uses' => 'LandingPage\LandingPageController@profile_update']);
Route::get('/profile/change-password/{user_uuid}', ['middleware' => 'CheckLogin', 'as' => 'change_password', 'uses' => 'LandingPage\LandingPageController@change_password']);
Route::get('/bantuan', ['as' => 'bantuan', 'uses' => 'LandingPage\LandingPageController@bantuan']);

Route::get('/cari-ternak', ['as' => 'cari_ternak', 'uses' => 'LandingPage\CariTernakController@cari_ternak']);
Route::get('/checkout/pengiriman-ternak/{animal_uuid}', ['middleware' => 'CheckLogin', 'as' => 'pengiriman_ternak', 'uses' => 'LandingPage\LandingPageController@pengiriman_ternak']);
Route::get('/checkout/pembayaran-ternak/{animal_uuid}', ['middleware' => 'CheckLogin', 'as' => 'pembayaran_ternak', 'uses' => 'LandingPage\LandingPageController@pembayaran_ternak']);

Route::post('/save-order', ['as' => 'save_order', 'uses' => 'Order\OrderController@save_order']);

Route::get('/daftar', ['as' => 'daftar', 'uses' => 'LandingPage\LoginController@daftar']);

Route::group(['prefix' => 'dashboard'], function () {

    //mitra
    Route::get('/mitra', ['middleware' => 'CheckLogin', 'as' => 'dashboard_mitra', 'uses' => 'Dashboard\MitraController@dashboard_mitra']);
    Route::get('/mitra/pengaturan-peternakan', ['middleware' => 'CheckLogin', 'as' => 'dashboard_mitra_pengaturan_peternakan', 'uses' => 'Dashboard\MitraController@mitra_pengaturan_peternakan']);
    Route::get('/mitra/edit-peternakan', ['middleware' => 'CheckLogin', 'as' => 'dashboard_mitra_edit_peternakan', 'uses' => 'Dashboard\MitraController@mitra_edit_peternakan']);
    Route::get('/mitra/tambah-hewan', ['middleware' => 'CheckLogin', 'as' => 'dashboard_mitra_tambah_hewan', 'uses' => 'Dashboard\MitraController@mitra_tambah_hewan']);
    Route::get('/mitra/edit-hewan/{animal_uuid}', ['middleware' => 'CheckLogin', 'as' => 'dashboard_mitra_edit_hewan', 'uses' => 'Dashboard\MitraController@mitra_edit_hewan']);
    Route::get('/mitra/hewan-dijual', ['middleware' => 'CheckLogin', 'as' => 'dashboard_mitra_hewan_dijual', 'uses' => 'Dashboard\MitraController@mitra_hewan_dijual']);
    Route::get('/mitra/hewan-terjual', ['middleware' => 'CheckLogin', 'as' => 'dashboard_mitra_hewan_terjual', 'uses' => 'Dashboard\MitraController@mitra_hewan_terjual']);
    Route::get('/mitra/transaksi-anda', ['middleware' => 'CheckLogin', 'as' => 'dashboard_mitra_transaksi_anda', 'uses' => 'Dashboard\MitraController@mitra_transaksi_anda']);
    Route::get('/mitra/menunggu-persetujuan', ['middleware' => 'CheckLogin', 'as' => 'dashboard_mitra_menunggu_persetujuan', 'uses' => 'Dashboard\MitraController@mitra_menunggu_persetujuan']);
    Route::get('/mitra/menunggu-pembayaran', ['middleware' => 'CheckLogin', 'as' => 'dashboard_mitra_menunggu_pembayaran', 'uses' => 'Dashboard\MitraController@mitra_menunggu_pembayaran']);
    Route::get('/mitra/tunai-berhasil', ['middleware' => 'CheckLogin', 'as' => 'dashboard_mitra_tunai_berhasil', 'uses' => 'Dashboard\MitraController@mitra_tunai_berhasil']);
    Route::get('/mitra/tabungan-berhasil', ['middleware' => 'CheckLogin', 'as' => 'dashboard_mitra_tabungan_berhasil', 'uses' => 'Dashboard\MitraController@mitra_tabungan_berhasil']);
    Route::get('/mitra/tabungan-hewan', ['middleware' => 'CheckLogin', 'as' => 'dashboard_mitra_tabungan_hewan', 'uses' => 'Dashboard\MitraController@mitra_tabungan_hewan']);
    Route::get('/mitra/menunggu-pengiriman', ['middleware' => 'CheckLogin', 'as' => 'dashboard_mitra_menunggu_pengiriman', 'uses' => 'Dashboard\MitraController@mitra_menunggu_pengiriman']);

    //buyer
    Route::get('/buyer/transaksi-pembelian', ['middleware' => 'CheckLogin', 'as' => 'dashboard_buyer_transaksi_pembelian', 'uses' => 'Dashboard\BuyerController@buyer_transaksi_pembelian']);
    Route::get('/buyer/riwayat-pembayaran', ['middleware' => 'CheckLogin', 'as' => 'dashboard_buyer_riwayat_pembayaran', 'uses' => 'Dashboard\BuyerController@buyer_riwayat_pembayaran']);
    Route::get('/buyer/tabungan-hewan', ['middleware' => 'CheckLogin', 'as' => 'dashboard_buyer_tabungan_hewan', 'uses' => 'Dashboard\BuyerController@buyer_tabungan_hewan']);
    Route::get('/buyer/status-pembayaran', ['middleware' => 'CheckLogin', 'as' => 'dashboard_buyer_status_pembayaran', 'uses' => 'Dashboard\BuyerController@buyer_status_pembayaran']);
    Route::get('/buyer/cara-pembayaran', ['middleware' => 'CheckLogin', 'as' => 'dashboard_buyer_cara_pembayaran', 'uses' => 'Dashboard\BuyerController@buyer_cara_pembayaran']);
    Route::get('/buyer/ulasan', ['middleware' => 'CheckLogin', 'as' => 'dashboard_buyer_ulasan', 'uses' => 'Dashboard\BuyerController@buyer_ulasan']);
});

Route::post('/auth/login', ['as' => 'login', 'uses' => 'LandingPage\LoginController@login']);
Route::get('/auth/get-profile', ['as' => 'get_profile', 'uses' => 'LandingPage\LoginController@get_profile']);
Route::get('/auth/get-saldo', ['as' => 'get_saldo', 'uses' => 'LandingPage\LoginController@get_saldo']);
Route::post('/auth/logout', ['as' => 'login', 'uses' => 'LandingPage\LoginController@logout']);