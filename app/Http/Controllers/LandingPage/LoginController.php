<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07/08/18
 * Time: 9:53
 */

namespace App\Http\Controllers\LandingPage;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function login(Request $request){
        $post = (object)[
            'username' => $request->username,
            'password' => $request->password
        ];

        $ch = curl_init(env("HOST_API")."/api/auth/login");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        $response = curl_exec($ch);

        curl_close($ch);

        $responses = json_decode($response);

        Session::put('accessToken', $responses->data->accessToken);
        Session::put('userUUID', $responses->data->userAccount->uuid);

        return response()->json($responses);
    }

    public function daftar(Request $request){
        return view('landing_page/daftar');
    }

    public function get_profile(Request $request){
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, env("HOST_API")."/api/profile/me");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_HTTPHEADER, array(
            'Authorization:'.Session::get('accessToken'),
            'Accept-Version: 1.0'
        ));

        $output=curl_exec($ch);

        curl_close($ch);
        $response = json_decode($output);

        Session::put('fullName', $response->data->userAccount->userProfile->fullName);
        Session::put('phoneNumber', $response->data->userAccount->phone);
        Session::put('username', $response->data->userAccount->username);
        Session::put('address', $response->data->userAccount->userProfile->address->address);
        Session::put('mitra_numb', count($response->data->mitraList));

        return response()->json($response);
    }

    public function get_saldo(Request $request){
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, env("HOST_API")."/api/wallet/me/balance");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_HTTPHEADER, array(
            'Authorization:'.Session::get('accessToken'),
            'Accept-Version: 1.0'
        ));

        $output=curl_exec($ch);

        curl_close($ch);
        $response = json_decode($output);

        Session::put('saldoUser', $response->data->balance);

        return response()->json($response);
    }

    public function logout(){
        Session::flush();
    }
}