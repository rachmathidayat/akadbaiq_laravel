<?php

namespace App\Http\Controllers\LandingPage;

use Illuminate\Http\Request;

class CariTernakController extends Controller
{
    public function cari_ternak(Request $request){
        return view('cari_ternak/cari_ternak');
    }
}