<?php

namespace App\Http\Controllers\LandingPage;

use Illuminate\Http\Request;

class LandingPageController extends Controller
{
    public function landing_page(Request $request){
        return view('landing_page/landing_page');
    }

    public function pilih_program(Request $request){
        return view('landing_page/pilih_program');
    }

    public function contactus(Request $request){
        return view('landing_page/contactus');
    }

    public function profile(Request $request, $user_uuid){
        return view('landing_page/profile');
    }

    public function profile_update(Request $request, $user_uuid){
        return view('landing_page/profile_update');
    }

    public function change_password(Request $request, $user_uuid){
        return view('landing_page/change_password');
    }

    public function bantuan(Request $request){
        return view('bantuan/index');
    }

    public function pengiriman_ternak(Request $request, $animal_uuid){
        return view('landing_page/pengiriman_ternak', compact('animal_uuid', $animal_uuid));
    }

    public function pembayaran_ternak(Request $request, $animal_uuid){
        return view('landing_page/pembayaran_ternak', compact('animal_uuid', $animal_uuid));
    }
}    