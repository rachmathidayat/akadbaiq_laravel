<?php

namespace App\Http\Controllers\Dashboard;


use Illuminate\Http\Request;

class BuyerController extends Controller
{
    public function buyer_transaksi_pembelian(Request $request){
        return view('dashboard/buyer/transaksi_pembelian');
    }

    public function buyer_riwayat_pembayaran(Request $request){
        return view('dashboard/buyer/riwayat_pembayaran');
    }

    public function buyer_tabungan_hewan(Request $request){
        return view('dashboard/buyer/tabungan_hewan');
    }

    public function buyer_status_pembayaran(Request $request){
        return view('dashboard/buyer/status_pembayaran');
    }

    public function buyer_cara_pembayaran(Request $request){
        return view('dashboard/buyer/cara_pembayaran');
    }

    public function buyer_ulasan(Request $request){
        return view('dashboard/buyer/ulasan');
    }
}