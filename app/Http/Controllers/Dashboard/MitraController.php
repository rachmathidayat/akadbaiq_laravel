<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 08/08/18
 * Time: 11:39
 */

namespace App\Http\Controllers\Dashboard;


use Illuminate\Http\Request;

class MitraController extends Controller
{
    public function dashboard_mitra(){
        return view('dashboard/mitra/index');
    }

    public function mitra_pengaturan_peternakan(){
        return view('dashboard/mitra/pengaturan_peternakan');
    }

    public function mitra_edit_peternakan(){
        return view('dashboard/mitra/edit_peternakan');
    }

    public function mitra_tambah_hewan(){
        return view('dashboard/mitra/tambah_hewan');
    }

    public function mitra_edit_hewan(Request $request, $animal_uuid){
        return view('dashboard/mitra/edit_hewan', compact('animal_uuid'));
    }

    public function mitra_hewan_dijual(){
        return view('dashboard/mitra/hewan_dijual');
    }

    public function mitra_hewan_terjual(){
        return view('dashboard/mitra/hewan_terjual');
    }

    public function mitra_transaksi_anda(){
        return view('dashboard/mitra/transaksi_anda');
    }

    public function mitra_menunggu_persetujuan(){
        return view('dashboard/mitra/menunggu_persetujuan');
    }

    public function mitra_menunggu_pembayaran(){
        return view('dashboard/mitra/menunggu_pembayaran');
    }

    public function mitra_tunai_berhasil(){
        return view('dashboard/mitra/tunai_berhasil');
    }

    public function mitra_tabungan_berhasil(){
        return view('dashboard/mitra/tabungan_berhasil');
    }

    public function mitra_tabungan_hewan(){
        return view('dashboard/mitra/tabungan_hewan');
    }

    public function mitra_menunggu_pengiriman(){
        return view('dashboard/mitra/menunggu_pengiriman');
    }
}