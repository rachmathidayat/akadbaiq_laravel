<?php

namespace App\Http\Controllers\Dashboard;
use Illuminate\Http\Request;

/**
 * Created by PhpStorm.
 * User: root
 * Date: 06/08/18
 * Time: 11:22
 */
class DashboardController extends Controller
{
    public function get_profile(Request $request){
        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, $request->url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_HTTPHEADER, array(
              'Accept-Version: 1.0',
              'Authorization:'.$request->token
        ));

        $output=curl_exec($ch);

        curl_close($ch);
        $response = json_decode($output);

        return response()->json($response);
    }
}