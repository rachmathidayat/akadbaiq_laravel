<?php

namespace App\Http\Controllers\Order;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    function save_order(Request $request){
        $ch = curl_init(env("HOST_API")."/api/order");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept-Version: 1.0',
            'Authorization:'.Session::get('accessToken'),
            'Content-Type: application/x-www-form-urlencoded'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request->all());

        $response = curl_exec($ch);

        curl_close($ch);

        $responses = json_decode($response);
        return response()->json($responses);
    }
}    