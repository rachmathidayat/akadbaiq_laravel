<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::has('accessToken')){
            if(isset($request->user_uuid)){
                if($request->user_uuid != Session::get('userUUID')){
                    return \Response::view('errors.404',array(),404);
                }
            }

            $ch = curl_init();

            curl_setopt($ch,CURLOPT_URL, "http://103.253.107.139:8080/api/profile/me");
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch,CURLOPT_HTTPHEADER, array(
                'Accept-Version: 1.0',
                'Authorization:'.Session::get('accessToken')
            ));

            $output=curl_exec($ch);

            curl_close($ch);
            $response = json_decode($output);

            if($response->success === true){
                return $next($request);
            }else{
                return \Response::view('errors.404',array(),404);
            }
        }else{
            return redirect('/');
        }
    }
}
